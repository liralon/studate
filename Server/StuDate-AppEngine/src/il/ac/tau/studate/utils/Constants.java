package il.ac.tau.studate.utils;

import java.util.List;
import java.util.ArrayList;

public class Constants {
	
	public static final String API_KEY = "AIzaSyDbw4va3hAem7A_HoPDXgdt-BzgWIFvpyU";
	public static final String IONIC_CLIENT_ID = "932869752253-kij1dfdgom985g41uo2m77i59u8tqamb.apps.googleusercontent.com";
	public static final String WEB_CLIENT_ID = "932869752253-953sqcg86o9o000d179okrck4lm63j84.apps.googleusercontent.com";
	public static final String TEST_EMAIL = "example@example.com";
	public static final String SERVER_API_KEY = "AIzaSyCAB2meqVja8HiiCsDChhtkSkL1HFopOzA";
	
	public static final List<String> STAFF_TEST_EMAILS = new ArrayList<String>();
	public static final List<String> ADMINS_EMAILS = new ArrayList<String>();
	
	static {
		ADMINS_EMAILS.add(TEST_EMAIL);
		ADMINS_EMAILS.add("liranalon@mail.tau.ac.il");
		ADMINS_EMAILS.add("gilkaminker@mail.tau.ac.il");
		ADMINS_EMAILS.add("ophiraharoni@mail.tau.ac.il");
		ADMINS_EMAILS.add("gilblasbalg@gmail.com");
		ADMINS_EMAILS.add("miraweiss14@gmail.com");
	}
	
	static {
		STAFF_TEST_EMAILS.add("studatetester1@gmail.com");
		STAFF_TEST_EMAILS.add("studatetester2@gmail.com");
		STAFF_TEST_EMAILS.add("studatetester3@gmail.com");
	}	
}
