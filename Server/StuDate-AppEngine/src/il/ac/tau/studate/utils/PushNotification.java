package il.ac.tau.studate.utils;

import il.ac.tau.studate.entities.DevicePlatform;
import il.ac.tau.studate.entities.Student;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.json.JSONException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.PushedNotifications;
import javapns.notification.ResponsePacket;

import javax.jdo.PersistenceManager;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.appengine.api.datastore.Key;

public class PushNotification {
	
	private static final int NUMBER_OF_PUSH_RETRIES = 5;
	
	public static void sendMessageToStudent(
			PersistenceManager pm, 
			Key studentKey, 
			PushMessageTypes messageType, 
			String message, 
			Map<String, String> properties) throws IOException {
		
		Student student = pm.getObjectById(Student.class, studentKey);
		sendMessageToStudent(student, messageType, message, properties);
	}
	
	public static void sendMessageToStudent( 
			Student student, 
			PushMessageTypes messageType,
			String message, 
			Map<String, String> properties) throws IOException {
		
		// Note - In case we are debugging the user in PC, his deviceId can be null and therefore we avoid crashing here
		// Note - (Because we are debugging in PC, we just prefer to not send the push notification)
		if (student.getDeviceId() == null) {
			return;
		}
		
		properties.put("messageType", messageType.toString());
		
		if (student.getDevicePlatform() == DevicePlatform.ANDROID) {
			sendMessageAndroid(student.getDeviceId(), message, properties);
		}
		else if (student.getDevicePlatform() == DevicePlatform.IOS) {
			sendMessageIOS(student.getDeviceId(), message, properties);
		}
		
	}
	
	public static void sendMessageAndroid(String deviceRegistrationId, String message, Map<String, String> properties) throws IOException { 
	    Sender sender = new Sender(Constants.SERVER_API_KEY);
	    
	    Builder msgBuilder = new Message.Builder();
	    //msgBuilder.addData("msgcnt", "2");			// Shows message count in notification bar (Not needed. Kept here as comment for reference)
	    msgBuilder.addData("title", "Studate");
	    msgBuilder.addData("soundname", "2");
	    properties.put("message", message);
	    msgBuilder.collapseKey("demo");
	    for (Entry<String, String> property: properties.entrySet()) {
	    	msgBuilder.addData(property.getKey(), property.getValue());
	    }
	    
	    Message msg = msgBuilder.build();
	    
	    Result result = sender.send(msg, deviceRegistrationId, NUMBER_OF_PUSH_RETRIES);
    	if (result.getMessageId() != null) {
    		String canonicalRegistrationId = result.getCanonicalRegistrationId();
            if (canonicalRegistrationId != null) {
                // TODO - the registration id changed, we have to update the DB
            }
    	} else {
    		String error = result.getErrorCodeName();
            if (error.equals(com.google.android.gcm.server.Constants.ERROR_NOT_REGISTERED)) {
                // TODO - the device is no longer registered with Gcm, remove it from the DB
            }
    	}
	}
	
	public static void sendMessageIOS(String deviceRegistrationId, String message, Map<String, String> properties) throws IOException {
		PushNotificationPayload payload = PushNotificationPayload.complex();
		try {
			payload.addAlert(message);
			payload.addSound("default");

			for (Entry<String, String> property: properties.entrySet()) {
				payload.addCustomDictionary(property.getKey(), property.getValue());
			}
		// TODO - THIS "catch" BLOCK IS JUST auto-generated CODE AND SHOULD NOT BE HERE
		// TODO - WE SHOULD LET THE EXCEPTION PROPOGATE UPWARDS
		} catch (JSONException e) {
			e.printStackTrace();
			return;
		}
		
		// TODO - GET THE VALUE OF THIS FROM Constants.java
		// TODO - WHAT IS THE EFFECT OF THIS? IS IT IMPORTANT?
		boolean isProduction = false; // TODO - Should change this value on production
		try {
			// TODO - "stuDateSSLPushCert.p12" FILE IS STORED IN NON-SECURE WAY AND ANYBODY CAN JUST DOWNLOAD IT...
			// TODO - SHOULD PROBABLY CHANGE "web.xml" OR SOME OTHER SHEKER AppEngine FILE TO CONFIGURE IT UNACCESSABLE
			// TODO - IN ADDITION, PUT ALL THESE CONSTANTS in Constants.java / private static members of this class
			PushedNotifications notifications = Push.payload(payload, "stuDateSSLPushCert.p12", "studatepushkey1", isProduction, deviceRegistrationId); 
			for (PushedNotification notification : notifications.getFailedNotifications()) {
				//String invalidToken = notification.getDevice().getToken();
				// TODO - Add code here to remove invalidToken from  database  

				// Find out more about what the problem was
				Exception theProblem = notification.getException();
				theProblem.printStackTrace();	// TODO - To where does this stack trace gets printed? It is unclear...

				// If the problem was an error-response packet returned by Apple, get it
				ResponsePacket theErrorResponse = notification.getResponse();
				if (theErrorResponse != null) {
					// TODO - PUT HERE SOME ERROR HANDLING CODE?
					//System.out.println(theErrorResponse.getMessage());
				}
			} 
		// TODO - BAD HANDLING OF EXCEPTIONS:
		// TODO - SHOULD PROPOGATE EXCEPTIONS INSTEAD OF CATCHING THEM!
		} catch (KeystoreException e) {
			// critical problem occurred while trying to use your keystore  
			e.printStackTrace();
	
		} catch (CommunicationException e) {
			// A critical communication error occurred while trying to contact Apple servers  
			e.printStackTrace();
		}
	
	}
	
}
