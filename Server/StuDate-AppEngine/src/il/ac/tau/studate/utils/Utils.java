package il.ac.tau.studate.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletResponse;

import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

public class Utils {

	private static final Logger logger = Logger.getLogger("il.ac.tau.studate");

	public static void log(java.util.logging.Level level, String message) {
		logger.log(level, message);
	}

	public static void verifyUser(User userInfo) throws UnauthorizedException {	
		if (userInfo == null) {
			throw new UnauthorizedException(
					"User is not authenticated with a google account");
		}
			
		// Note - This email belongs to the local testing system...
		if (userInfo.getEmail().equals(Constants.TEST_EMAIL)) {
			return;
		}

		// Note - users with a staff test email can be verified even though they
		// don't own tau accounts
		if (Constants.STAFF_TEST_EMAILS.contains(userInfo.getEmail().toLowerCase())) {
			return;
		}

		if (!userInfo.getEmail().endsWith("mail.tau.ac.il")) {
			throw new UnauthorizedException(
					"User is a google account not related to mail.tau.ac.il");
		}
	}

	public static void verifyAdminUser(User userInfo)
			throws UnauthorizedException {
		if (userInfo == null) {
			throw new UnauthorizedException(
					"User is not authenticated with a google account");
		}

		if (!Constants.ADMINS_EMAILS.contains(userInfo.getEmail())) {
			throw new UnauthorizedException(
					"User is not one of the autherized admin users");
		}
	}

	public static User verifyUserFromServlet(HttpServletResponse response)
			throws IOException {
		final UserService userService = UserServiceFactory.getUserService();
		User userInfo = userService.getCurrentUser();

		try {
			verifyUser(userInfo);
		} catch (UnauthorizedException e) {
			response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE,
					e.getMessage());
			userInfo = null;
		}

		return userInfo;
	}

	public static Query getQueryFromCursorString(PersistenceManager pm,
			@SuppressWarnings("rawtypes") Class kind, String cursorString,
			int numberOfProfiles) {

		Query query = pm.newQuery(kind);

		if ((cursorString != null) && (!cursorString.equals(""))) {
			Cursor cursor = Cursor.fromWebSafeString(cursorString);
			HashMap<String, Object> extensionsMap = new HashMap<String, Object>();
			extensionsMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
			query.setExtensions(extensionsMap);
		}

		query.setRange(0, numberOfProfiles);

		return query;
	}

	public static byte[] getBlobDataFromBlobKey(BlobKey blobKey) {
		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();

		BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
		BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(blobKey);
		final long blobSize = blobInfo.getSize();

		byte[] blobData = blobstoreService.fetchData(blobKey, 0, blobSize);
		return blobData;
	}

	public static void deleteAllUploadBlobs(BlobstoreService blobstoreService,
			Map<String, List<BlobKey>> uploads, List<String> dontDeleteList) {

		for (Entry<String, List<BlobKey>> uploadBlobs : uploads.entrySet()) {
			if (!dontDeleteList.contains(uploadBlobs.getKey())) {
				for (BlobKey uploadBlob : uploadBlobs.getValue()) {
					blobstoreService.delete(uploadBlob);
				}
			}
		}
	}

}
