package il.ac.tau.studate.utils;

import java.io.IOException;
import java.util.List;

import il.ac.tau.studate.endpoints.AvailableDeclerationEndpoint;
import il.ac.tau.studate.entities.AvailableDecleration;
import il.ac.tau.studate.entities.Match;
import il.ac.tau.studate.entities.StudateDate;
import il.ac.tau.studate.entities.Student;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

public class MatchUtils {
	
	// Note - This function must be called inside a transaction
	public static void addMatchByEmails(PersistenceManager pm, String firstEmail, String secondEmail) throws IOException {
        Key firstStudent = KeyFactory.createKey(Student.class.getSimpleName(), firstEmail);
        Key secondStudent = KeyFactory.createKey(Student.class.getSimpleName(), secondEmail);
	    addMatch(pm, firstStudent, secondStudent);
	}
	
	// Note - This function must be called inside a transaction
	private static void addMatch(PersistenceManager pm, Key firstStudent, Key secondStudent) throws IOException {
		
		Match firstSideMatch = new Match(firstStudent, secondStudent);
		
		// The match should always not exists when we get here, but this is done for consistency check just to be sure (Can happen because of manual DB editing)
		//if (!doesMatchExists(pm, firstStudent, secondStudent)) {
		    Match secondSideMatch = new Match(secondStudent, firstStudent);
		
		    firstSideMatch.setSymetericMatch(secondSideMatch);
		    secondSideMatch.setSymetericMatch(firstSideMatch);
		    
		    pm.makePersistent(firstSideMatch);
		    pm.makePersistent(secondSideMatch);
		    
		    incrementStudentPendingMatches(pm, firstStudent);
		    incrementStudentPendingMatches(pm, secondStudent);
		//}
	    
	    // If the users are already available when the match is created, then make a date between them and send push notification!
	    if (isUserAvailable(pm, firstStudent) && isUserAvailable(pm, secondStudent)) {
	    	// TODO - CALLING CODE OF "AvailableDeclerationEndpoint" SEEMS WEIRD. SHOULD PROBABLY EXPORT THOSE FUNCTIONS TO EXTERNAL STATIC UTIL CLASS
	    	StudateDate date = AvailableDeclerationEndpoint.addDateForMatch(pm, firstSideMatch);
	    	AvailableDeclerationEndpoint.pushDateInfoToClients(date, pm);
	    }
	}
	
	// TODO - Decide if we want this code or not (Called above from code in comment)
	/*@SuppressWarnings("unchecked")
	private static boolean doesMatchExists(PersistenceManager pm, Key firstStudent, Key secondStudent) {
		Query query = pm.newQuery(Match.class);
		query.declareImports("import com.google.appengine.api.datastore.Key");
		query.setFilter("(firstStudentParam == firstStudent) && (secondStudentParam == secondStudent)");
		query.declareParameters("Key firstStudentParam, Key secondStudentParam");
		List<Match> matches = (List<Match>)query.execute(firstStudent, secondStudent);
		
		return (matches.size() > 0);
	}*/
	
	@SuppressWarnings("unchecked")
	private static boolean isUserAvailable(PersistenceManager pm, Key student) {
		// TODO - Probably there is a better way to implement this... But this is what I know for now :)
		Query query = pm.newQuery(AvailableDecleration.class);
		query.declareImports("import com.google.appengine.api.datastore.Key");
		query.setFilter("(student == me)");
		query.declareParameters("Key me");
		List<AvailableDecleration> availableList = (List<AvailableDecleration>)query.execute(student);
		return (!availableList.isEmpty());
	}
	
	private static void incrementStudentPendingMatches(PersistenceManager pm, Key studentKey) {
		Student student = (Student)pm.getObjectById(Student.class, studentKey);
		student.incrementPendingMatches();
	}
	
	private static void decrementStudentPendingMatches(PersistenceManager pm, Key studentKey) {
		Student student = (Student)pm.getObjectById(Student.class, studentKey);
		student.decrementPendingMatches();
	}
	
	public static void removeMatch(PersistenceManager pm, Match match) {
		decrementStudentPendingMatches(pm, match.getFirstStudent());
		decrementStudentPendingMatches(pm, match.getSecondStudent());
		
		Match symetericMatch = match.getSymetricMatch();
		
		pm.deletePersistent(match);
		pm.deletePersistent(symetericMatch);
	}
}
