package il.ac.tau.studate.entities;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable
public class StudateDate {
	
	public StudateDate(Match matchThatLeadToDate) {
		this.firstStudent = matchThatLeadToDate.getFirstStudent();
		this.secondStudent = matchThatLeadToDate.getSecondStudent();
		this.FirstStudentStatus = DateConfirmationStatus.NOT_ANSWERED;
		this.SecondStudentStatus = DateConfirmationStatus.NOT_ANSWERED;
		
		this.matchThatLeadToDate = matchThatLeadToDate;
		
		this.meetingPlace = "UNDEFINED";	// Note - This must be this string (it is identified uniquely in the client)
		
		this.chatMessages = new ArrayList<ChatMessage>();
	}
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long dateId;
	
	@Persistent
	@Unowned
	private Key firstStudent;

	@Persistent
	@Unowned
	private Key secondStudent;
	
	@Persistent
	private DateConfirmationStatus FirstStudentStatus;
	
	@Persistent
	private DateConfirmationStatus SecondStudentStatus;
	
	@Persistent
	private String meetingPlace;
	
	@Persistent
	@Unowned
	private Match matchThatLeadToDate;
	
	@Persistent
	private List<ChatMessage> chatMessages;
	
	public Long getDateId() 	{ return this.dateId;	}
	
	public Key getFirstStudent() 	{	return this.firstStudent;	}

	public Key getSecondStudent() { return this.secondStudent;	}
	
	public DateConfirmationStatus getFirstStudentStatus() {	return this.FirstStudentStatus;	}
	public DateConfirmationStatus getSecondStudentStatus() {	return this.SecondStudentStatus;	}
	
	public void setFirstStudentStatus(DateConfirmationStatus FirstStudentStatus) 	{	this.FirstStudentStatus = FirstStudentStatus;	}
	public void setSecondStudentStatus(DateConfirmationStatus SecondStudentStatus) 	{	this.SecondStudentStatus = SecondStudentStatus;	}
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public Match getMatchThatLeadToDate()	{ return this.matchThatLeadToDate; }
	
	public String getMeetingPlace() {	return this.meetingPlace;	}
	
	public void setMeetingPlace(String meetingPlace) 	{	this.meetingPlace = meetingPlace;	}
	
	public void addChatMessage(ChatMessage chatMessage) {
		this.chatMessages.add(chatMessage);
	}
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public List<ChatMessage> getChatMessages() 	{ return this.chatMessages; }
}
