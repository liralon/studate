package il.ac.tau.studate.entities;

public enum GenderPreferenceOptions {
	MALE,
	FEMALE,
	ALL
}
