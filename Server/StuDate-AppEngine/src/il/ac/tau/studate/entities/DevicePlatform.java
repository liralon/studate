package il.ac.tau.studate.entities;

public enum DevicePlatform {
	ANDROID,
	IOS
}
