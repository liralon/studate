package il.ac.tau.studate.entities;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class DateInfo {
	
	public DateInfo(Student peerStudent, String meetingPlace) {
		this.peerStudent = peerStudent;
		this.meetingPlace = meetingPlace;
	}
	
	@Persistent
	private Student peerStudent;
	
	@Persistent
	private String meetingPlace;

	public Student getPeerStudent() 		{ return peerStudent; }
	public String getMeetingPlace() 	{ return meetingPlace; }
	
}
