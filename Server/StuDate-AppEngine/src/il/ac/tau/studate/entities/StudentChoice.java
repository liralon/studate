package il.ac.tau.studate.entities;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class StudentChoice {
	
	public StudentChoice(String studentEmailAddress, String candidateEmailAddress, boolean doesLike) {
		this.studentEmailAddress = studentEmailAddress;
		this.candidateEmailAddress = candidateEmailAddress;
		this.doesLike = doesLike;
	}
	
	// TODO - hash of emails (Calc this in constructor)
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
	
	@Persistent
	private String studentEmailAddress;
	
	@Persistent
	private String candidateEmailAddress;
	
	@Persistent
	private boolean doesLike;
	
}
