package il.ac.tau.studate.entities;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class StringResponseEntity {

	public StringResponseEntity(String response) {
		this.response = response;
	}
	
	@Persistent
	String response;
	
	public String getResponse() { return this.response; }
}
