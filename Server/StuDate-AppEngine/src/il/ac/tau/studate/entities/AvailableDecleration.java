
package il.ac.tau.studate.entities;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable
public class AvailableDecleration {

	public AvailableDecleration(String studentEmailAddress) {
		this.studentEmailAddress = studentEmailAddress;
		this.student = KeyFactory.createKey(Student.class.getSimpleName(), studentEmailAddress);
	}
	
	@PrimaryKey
	@Persistent
	private String studentEmailAddress;
	
	@Persistent
	@Unowned
	private Key student;
	
	public String getEmailAddress()		{ return this.studentEmailAddress; }
	
}
