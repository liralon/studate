package il.ac.tau.studate.entities;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.datanucleus.annotations.Unowned;

// Note - The Match table saves mirroring entries to make availability match making faster
@PersistenceCapable
public class Match {

	public Match(Key firstStudent, Key secondStudent) {
		this.firstStudent = firstStudent;
		this.secondStudent = secondStudent;
	}

	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;

	@Persistent
	private Key firstStudent;
	
	@Persistent
	private Key secondStudent;
	
	@Persistent
	@Unowned
	private Match symetericMatch;
	
	public Key getFirstStudent() { return this.firstStudent; }
	public Key getSecondStudent() { return this.secondStudent; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public Match getSymetricMatch()		{ return this.symetericMatch; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public void setSymetericMatch(Match match)		{ this.symetericMatch = match; }
}
