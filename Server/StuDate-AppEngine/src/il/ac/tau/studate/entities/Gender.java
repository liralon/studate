package il.ac.tau.studate.entities;

public enum Gender {
	FEMALE,
	MALE
}
