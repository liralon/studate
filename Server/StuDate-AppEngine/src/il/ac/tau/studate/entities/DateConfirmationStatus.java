package il.ac.tau.studate.entities;

public enum DateConfirmationStatus {
	NOT_ANSWERED,
	CONFIRMED,
	DECLINED,
	DURING_DATE		// TODO - Seems weird this is here. Consider refactoring
}
