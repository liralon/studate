package il.ac.tau.studate.entities;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class ChatMessage {

	public ChatMessage(String emailAddress, String message) {
		this.emailAddress = emailAddress;
		this.message = message;
	}
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
	
	@Persistent
	private String emailAddress;
	
	@Persistent
	private String message;
	
	public String getEmailAddress() 	{ return this.emailAddress; }
	
	public String getMessage()			{ return this.message; }
	
}
