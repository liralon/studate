package il.ac.tau.studate.entities;

import il.ac.tau.studate.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.google.appengine.api.blobstore.BlobKey;

@PersistenceCapable
public class Student {

	public Student(
			String emailAddress,
			String name, 
			BlobKey profilePicture,
			String faculty,
			Gender gender,
			Long age,
			GenderPreferenceOptions genderPreferenceOptions) {
		this.emailAddress = emailAddress;
		this.name = name;
		this.profilePicture = profilePicture;
		this.faculty = faculty;
		this.gender = gender.toString();
		setGenderPreference(genderPreferenceOptions);
		this.pendingMatches = 0;
		this.age = age;
		
		this.dateState = DateState.NOT_IN_DATE;
		this.pendingDateId = new Long(0);
		
		this.isInvisible = false;
	}
	
	@PrimaryKey
	@Persistent
	private String emailAddress;
	
	@Persistent
	private String name;
	
	// Note - This is actually Gender but needs to be saved as string to make filtering easier
	@Persistent
	private String gender;
	
	@Persistent
	private Long age;
	
	// Note - This is actually Gender but needs to be saved as string to make filtering easier
	// Note - We save this in a list only to make searches faster (It allows us to use "contains")
	@Persistent
	private List<String> genderPreference;
	
	@Persistent
	private BlobKey profilePicture;
	
	@Persistent
	private String faculty;
	
	@Persistent
	private Double clientVersion;
	
	@Persistent
	private String deviceId;
	
	@Persistent
	private DevicePlatform devicePlatform;
	
	@Persistent
	private int pendingMatches; 
	
	@Persistent
	private DateState dateState;
	
	@Persistent
	private Long pendingDateId;
	
	@Persistent
	private boolean isInvisible;
	
	public String getEmailAddress() { return this.emailAddress; }
	
	public String getName() { return this.name; }
	
	public String getGender() { return this.gender; }
	
	public Long getAge() { return this.age; }
	
	public void setGender(Gender gender) 	{ this.gender = gender.toString(); }
	
	public void setAge(Long age) { this.age = age; } 
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public List<String> getGenderPreference() { return genderPreference; }
	
	public byte[] getProfilePicture() {
		if (this.profilePicture != null) {
			return Utils.getBlobDataFromBlobKey(this.profilePicture);
		} else {
			return (new byte[] {});
		}
	}
	
	public String getFaculty() { return this.faculty; }
	public void setFaculty(String faculty)	{ this.faculty = faculty; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public String getDeviceId() { return this.deviceId; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public DevicePlatform getDevicePlatform()		{ return this.devicePlatform; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public void setDeviceId(String deviceId, DevicePlatform devicePlatform) {
		this.deviceId = deviceId;
		this.devicePlatform = devicePlatform;
	}
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public void setClientVersion(Double clientVersion) 		{	this.clientVersion = clientVersion;		}
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public void setGenderPreference(GenderPreferenceOptions options) {
		this.genderPreference = new ArrayList<String>();
		switch (options) {
		case MALE:
			this.genderPreference.add(Gender.MALE.toString());
			break;
		
		case FEMALE:
			this.genderPreference.add(Gender.FEMALE.toString());
			break;
			
		case ALL:
			this.genderPreference.add(Gender.MALE.toString());
			this.genderPreference.add(Gender.FEMALE.toString());
			break;
		}
	}

	public GenderPreferenceOptions getGenderPreferenceOptions() {
		if (this.genderPreference.size() == 2) {
			return GenderPreferenceOptions.ALL;
		} else if (this.genderPreference.contains(Gender.MALE)) {
			return GenderPreferenceOptions.MALE;
		} else {
			return GenderPreferenceOptions.FEMALE;
		}
	}
	
	public int getPendingMatches() { return this.pendingMatches; }
	
	public DateState getDateState()		{ return this.dateState; }
	
	public Long getPendingDateId()	{ return this.pendingDateId; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public void incrementPendingMatches() { ++this.pendingMatches; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public void decrementPendingMatches() { --this.pendingMatches; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public BlobKey getProfilePictureBlobKey() { return this.profilePicture; }
	
	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public void setProfilePictureBlobKey(BlobKey profilePictureBlobKey)		{ this.profilePicture = profilePictureBlobKey; }
	
	public void setDateState(DateState dateState)	{ this.dateState = dateState; }
	
	public void setPendingDateId(Long dateId)	{ this.pendingDateId = dateId; }
	
	public void setIsInvisible(boolean isInvisible)		{ this.isInvisible = isInvisible; }
	
	public boolean getIsInvisible() 		{ return this.isInvisible; }
	
}
