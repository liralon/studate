
package il.ac.tau.studate.endpoints;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import il.ac.tau.studate.entities.ChatMessage;
import il.ac.tau.studate.entities.DateConfirmationStatus;
import il.ac.tau.studate.entities.DateInfo;
import il.ac.tau.studate.entities.DateState;
import il.ac.tau.studate.entities.StringResponseEntity;
import il.ac.tau.studate.entities.StudateDate;
import il.ac.tau.studate.entities.Student;
import il.ac.tau.studate.utils.Constants;
import il.ac.tau.studate.utils.ExclusiveLock;
import il.ac.tau.studate.utils.MatchUtils;
import il.ac.tau.studate.utils.PMF;
import il.ac.tau.studate.utils.PushMessageTypes;
import il.ac.tau.studate.utils.PushNotification;
import il.ac.tau.studate.utils.Utils;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;

import javax.inject.Named;
import javax.jdo.PersistenceManager;

import java.util.logging.Level;

@Api(
		name = "studatedateendpoint",
		namespace = @ApiNamespace(ownerDomain = "ac.il", ownerName = "ac.il", packagePath = "tau.studate"), 
		clientIds = {Constants.IONIC_CLIENT_ID, Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID}
)
public class StudateDateEndpoint {
	
	@ApiMethod(name = "setDateConfirmationResult")
	public void setDateConfirmationResult(
			@Named("dateId") Long dateId, 
			@Named ("confirmationResult") DateConfirmationStatus confirmationResult,
			User userInfo) throws UnauthorizedException, IOException {
		
		Utils.verifyUser(userInfo);
		setDateConfirmationResultLogic(dateId, confirmationResult, userInfo.getEmail());
		Utils.log(Level.INFO, "after returning");
	}
	
	@ApiMethod(name = "debugSetDateConfirmationResult")
	public void debugSetDateConfirmationResult(
			User userInfo, 
			@Named("dateId") Long dateId, 
			@Named ("confirmationResult") DateConfirmationStatus confirmationResult,
			@Named("userEmail") String userEmail) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		setDateConfirmationResultLogic(dateId, confirmationResult, userEmail);
	}
	
	private void setDateConfirmationResultLogic(
			Long dateId, 
			DateConfirmationStatus confirmationResult,
			String userEmail) throws UnauthorizedException, IOException {
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		ExclusiveLock lock = new ExclusiveLock("setDateConfirmationResultLogicLock");
		try {
			lock.lock();
			
			StudateDate date = getDateFromDateId(pm,dateId);
			
			Key userKey = KeyFactory.createKey(Student.class.getSimpleName(), userEmail);
			
			if (date.getFirstStudent().equals(userKey)) {
				date.setFirstStudentStatus(confirmationResult);
				handleStudentDateAnswer(pm, userKey, confirmationResult, date.getSecondStudent(), date.getSecondStudentStatus(), date);
				
			} else if (date.getSecondStudent().equals(userKey)) {
				date.setSecondStudentStatus(confirmationResult);
				handleStudentDateAnswer(pm, userKey, confirmationResult, date.getFirstStudent(), date.getFirstStudentStatus(), date);
			}
			
			if (isBothDateParticipantsDuringDate(date)) {
				markDateStudentsNotInDate(pm, date);
				MatchUtils.removeMatch(pm, date.getMatchThatLeadToDate());
				pm.deletePersistent(date);
			}
			
		} finally {
			lock.unlock();
	    	pm.close();
	    }
	}
	
	@ApiMethod(name = "suggestMeetingPlace")
	public void suggestMeetingPlace(
			@Named("dateId") Long dateId, 
			@Named ("meetingPlace") String meetingPlace,
			User userInfo) throws UnauthorizedException, IOException {
		Utils.verifyUser(userInfo);
		suggestMeetingPlaceLogic(dateId, meetingPlace, userInfo.getEmail());
	}
	
	@ApiMethod(name = "debugSuggestMeetingPlace")
	public void debugSuggestMeetingPlace(
			User userInfo, 
			@Named("dateId") Long dateId, 
			@Named ("meetingPlace") String meetingPlace,
			@Named("userEmail") String userEmail) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		suggestMeetingPlaceLogic(dateId, meetingPlace, userEmail);
	}
	
	private void suggestMeetingPlaceLogic(Long dateId, String meetingPlace, String userEmailAddress) throws UnauthorizedException, IOException {
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		// TODO - Transactions don't really work
		//Transaction transaction = pm.currentTransaction();
		try {
			//transaction.begin();
			
			StudateDate date = getDateFromDateId(pm, dateId);
			date.setMeetingPlace(meetingPlace);
			
			Key userKey = KeyFactory.createKey(Student.class.getSimpleName(), userEmailAddress);
			Key suggestingStudent;
			Key studentThatShouldRecvSuggestion;
			if (date.getFirstStudent().equals(userKey))
			{
				suggestingStudent = date.getFirstStudent();
				studentThatShouldRecvSuggestion = date.getSecondStudent();
			}
			else
			{
				suggestingStudent = date.getSecondStudent();
				studentThatShouldRecvSuggestion = date.getFirstStudent();
			}
			
			setStudentDateState(pm, suggestingStudent, DateState.MEETING_PLACE_SETTER);
			setStudentDateState(pm, studentThatShouldRecvSuggestion, DateState.MEETING_PLACE_WAITER);
			
			//transaction.commit();
			
			Map<String, String> properties = new HashMap<String, String>();
			properties.put("dateId", dateId.toString());
			properties.put("meetingPlace", meetingPlace);
			
			PushNotification.sendMessageToStudent(
					pm, studentThatShouldRecvSuggestion, PushMessageTypes.MEETING_PLACE_SUGGESTED, "Location suggested", properties);
			
		} finally {
			/*if (transaction.isActive()) {
				transaction.rollback();
			}*/
			
	    	pm.close();
	    }
	}
	

	@ApiMethod(name = "confirmMeetingPlace")
	public void confirmMeetingPlace(@Named("dateId") Long dateId, User userInfo) throws UnauthorizedException, IOException {
		Utils.verifyUser(userInfo);
		confirmMeetingPlaceLogic(dateId, userInfo.getEmail());
	}
	
	@ApiMethod(name = "debugConfirmMeetingPlace")
	public void debugSetMeetingPlaceConfirmationResult(
			User userInfo, 
			@Named("dateId") Long dateId, 
			@Named("userEmail") String userEmail) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		confirmMeetingPlaceLogic(dateId, userEmail);
	}
	
	private void confirmMeetingPlaceLogic(Long dateId, String userEmail) throws UnauthorizedException, IOException {
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		// TODO - Transactions don't really work
		//Transaction transaction = pm.currentTransaction();
		try {
			//transaction.begin();
			
			StudateDate date = getDateFromDateId(pm,dateId);
			
			Key userKey = KeyFactory.createKey(Student.class.getSimpleName(), userEmail);
			boolean isFirstStudentConfirmer = date.getFirstStudent().equals(userKey);
			Key studentThatShouldRecvConfirmation = (isFirstStudentConfirmer) ? (date.getSecondStudent()) : (date.getFirstStudent()); 
			
			setStudentDateState(pm, date.getFirstStudent(), DateState.MEETING_PLACE_CONFIRMED);
			setStudentDateState(pm, date.getSecondStudent(), DateState.MEETING_PLACE_CONFIRMED);
			
			//transaction.commit();
			
			Map<String, String> properties = new HashMap<String, String>();
			properties.put("dateId", dateId.toString());

			PushNotification.sendMessageToStudent(
					pm, studentThatShouldRecvConfirmation, PushMessageTypes.MEETING_PLACE_CONFIRMED, "Location confirmed", properties);
			
		} finally {
			/*if (transaction.isActive()) {
				transaction.rollback();
			}*/
			
	    	pm.close();
	    }
	}
	
	private static void setStudentDateState(PersistenceManager pm, Key studentKey, DateState dateState) {
		Student student = pm.getObjectById(Student.class, studentKey);
		student.setDateState(dateState);
	}
	
	@ApiMethod(name = "getDateInfo")
	public DateInfo getDateInfo(User userInfo, @Named("dateId") Long dateId) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		DateInfo dateInfo = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			StudateDate date = getDateFromDateId(pm, dateId);
			
			Student peerStudent = getDatePeer(pm, date, userInfo.getEmail());
			dateInfo = new DateInfo(peerStudent, date.getMeetingPlace());
			
		} finally {
			pm.close();
		}
		
		return dateInfo;
	}
	
	@ApiMethod(name = "cancelDate")
	public void cancelDate(User userInfo, @Named("dateId") Long dateId, @Named("shouldDeleteMatch") boolean shouldDeleteMatch) 
			throws UnauthorizedException, IOException {
		Utils.verifyUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		// TODO - This code should be protected by a transaction but transactions don't work yet...
		try {
			StudateDate date = getDateFromDateId(pm, dateId);
			
			Student firstStudent = pm.getObjectById(Student.class, date.getFirstStudent());
			Student secondStudent = pm.getObjectById(Student.class, date.getSecondStudent());
			firstStudent.setDateState(DateState.NOT_IN_DATE);
			secondStudent.setDateState(DateState.NOT_IN_DATE);
			
			Student peerStudent = getDatePeer(date, firstStudent, secondStudent, userInfo.getEmail());
			Map<String, String> properties = new HashMap<String, String>();
			properties.put("dateId", dateId.toString());
			PushNotification.sendMessageToStudent(peerStudent, PushMessageTypes.DATE_CANCELED, "Date Canceled... :(", properties);
			
			// Note - This is done when the user decides that he doesn't want the current peer date to appear in future dates suggestions
			if (shouldDeleteMatch) {
				MatchUtils.removeMatch(pm, date.getMatchThatLeadToDate());
			}
			
			pm.deletePersistent(date);
			
		} finally {
			pm.close();
		}
	}
	
	@ApiMethod(name = "sendChatMessageToDatePeer")
	public StringResponseEntity sendChatMessageToDatePeer(
			User userInfo, 
			@Named("dateId") Long dateId, 
			@Named("chatMessage") String chatMessage) throws UnauthorizedException, IOException {
		Utils.verifyUser(userInfo);
		return sendChatMessageToDatePeerLogic(dateId, chatMessage, userInfo.getEmail());
	}
	
	@ApiMethod(name = "debugSendChatMessageToDatePeer")
	public StringResponseEntity debugSendChatMessageToDatePeer(
			User userInfo, 
			@Named("dateId") Long dateId, 
			@Named("chatMessage") String chatMessage,  
			@Named("userEmail") String senderEmail) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		return sendChatMessageToDatePeerLogic(dateId, chatMessage, senderEmail);
	}
	
	private StringResponseEntity sendChatMessageToDatePeerLogic(Long dateId, String chatMessage, String senderEmailAddress) 
			throws UnauthorizedException, IOException {
		
		StringResponseEntity returnValue = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			StudateDate date = getDateFromDateId(pm, dateId);
		
			date.addChatMessage(new ChatMessage(senderEmailAddress, chatMessage));
			
			Key senderUserKey = KeyFactory.createKey(Student.class.getSimpleName(), senderEmailAddress);
			DateConfirmationStatus datePeerConfirmationStatus = 
					(date.getFirstStudent().equals(senderUserKey)) ? (date.getSecondStudentStatus()) : (date.getFirstStudentStatus());
			if (datePeerConfirmationStatus != DateConfirmationStatus.DURING_DATE) {
				Student peerStudent = getDatePeer(pm, date, senderEmailAddress);
				Map<String, String> properties = new HashMap<String, String>();
				properties.put("dateId", dateId.toString());
				properties.put("chatMessage", chatMessage);
				PushNotification.sendMessageToStudent(peerStudent, PushMessageTypes.DATE_CHAT_MSG, "Chat message from date peer!", properties);
				returnValue = new StringResponseEntity("SUCCESS");
			} else {
				returnValue = new StringResponseEntity("DATE_PEER_NOT_IN_DATE");
			}
		} finally {
			pm.close();
		}
		
		return returnValue;
	}
	
	@ApiMethod(name = "getChatMessages")
	public ChatMessage[] getChatMessages(User userInfo, @Named("dateId") Long dateId) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		ChatMessage[] chatMessages = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			StudateDate date = getDateFromDateId(pm, dateId);
			// Note - Converting to ArrayList is a must because it forces the GAE engine to fetch all the entries in his lazy list.
			// Note - Failing to do so will make toArray() to fail because there List's toArray implementation have a bug :P
			ArrayList<ChatMessage> allChatMessages = new ArrayList<ChatMessage>(date.getChatMessages());
			chatMessages = allChatMessages.toArray(new ChatMessage[0]);
		} finally {
			pm.close();
		}
		
		return chatMessages;
	}
	
	private Student getDatePeer(PersistenceManager pm, StudateDate date, String userEmail) {
		Key userKey = KeyFactory.createKey(Student.class.getSimpleName(), userEmail);
		Student peerStudent = (date.getFirstStudent().equals(userKey)) ? 
				(pm.getObjectById(Student.class, date.getSecondStudent())) :
				(pm.getObjectById(Student.class, date.getFirstStudent()));
		return peerStudent;
	}
	
	private Student getDatePeer(StudateDate date, Student firstStudent, Student secondStudent, String userEmail) {
		Key userKey = KeyFactory.createKey(Student.class.getSimpleName(), userEmail);
		Student peerStudent = (date.getFirstStudent().equals(userKey)) ? (secondStudent) : (firstStudent);
		return peerStudent;
	}
	
	private static void handleStudentDateAnswer(
			PersistenceManager pm, 
			Key studentThatAnswered,
			DateConfirmationStatus answer, 
			Key peerStudent, 
			DateConfirmationStatus peerAnswer,
			StudateDate date) throws IOException {
		if (answer == DateConfirmationStatus.DECLINED) {
			Utils.log(Level.INFO, "handleStudentDateAnswer: declined");
			markDateStudentsNotInDate(pm, date);
			pushDateDeclined(pm, peerStudent, date.getDateId());
			pm.deletePersistent(date);
		
		} else if (answer == DateConfirmationStatus.CONFIRMED) {
			Utils.log(Level.INFO, "handleStudentDateAnswer: confirmed");
			markStudentConfirmed(pm, studentThatAnswered);
			
			if (peerAnswer == DateConfirmationStatus.CONFIRMED) {
				Utils.log(Level.INFO, "handleStudentDateAnswer: both confirmed");
				updateStudentsToSetMeetingPlace(pm, date);
			}
		}
	}
	
	private static void markDateStudentsNotInDate(PersistenceManager pm, StudateDate date) {
		markStudentNotInDate(pm, date.getFirstStudent());
		markStudentNotInDate(pm, date.getSecondStudent());
	}
	
	private static void markStudentConfirmed(PersistenceManager pm, Key studentKey) {
		Student student = pm.getObjectById(Student.class, studentKey);
		student.setDateState(DateState.DATE_SUGGESTION_CONFIRMED);
	}
	
	private static void markStudentNotInDate(PersistenceManager pm, Key studentKey) {
		Student student = pm.getObjectById(Student.class, studentKey);
		student.setDateState(DateState.NOT_IN_DATE);
	}
	
	private static void pushDateDeclined(PersistenceManager pm, Key studentKey, Long dateId) throws IOException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("dateId", dateId.toString());
		properties.put("isConfirmed", Boolean.toString(false));
		
		PushNotification.sendMessageToStudent(pm, studentKey, PushMessageTypes.DATE_RESULT, "Date declined!", properties);
	}
	
	private static void updateStudentsToSetMeetingPlace(PersistenceManager pm, StudateDate date) throws IOException {
		setStudentMeetingPlaceInfo(pm, date.getDateId(), date.getFirstStudent(), true);
		setStudentMeetingPlaceInfo(pm, date.getDateId(), date.getSecondStudent(), false);
	}
	
	private static void setStudentMeetingPlaceInfo(PersistenceManager pm, Long dateId, Key studentKey, boolean isSetter) throws IOException {
		DateState dateState = (isSetter) ? (DateState.MEETING_PLACE_SETTER) : (DateState.MEETING_PLACE_WAITER);
		
		Student student = pm.getObjectById(Student.class, studentKey);
		student.setDateState(dateState);
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("dateId", dateId.toString());
		properties.put("isConfirmed", Boolean.toString(true));
		properties.put("isSetter", Boolean.toString(isSetter));
		
		Utils.log(Level.INFO, "setStudentMeetingPlaceInfo: send date confirmation push");
		PushNotification.sendMessageToStudent(pm, studentKey, PushMessageTypes.DATE_RESULT, "Date Confirmed!", properties);
	}
	
	private StudateDate getDateFromDateId(PersistenceManager pm,Long dateId) {
		Key dateKey = KeyFactory.createKey(StudateDate.class.getSimpleName(), dateId);
		StudateDate date = (StudateDate)pm.getObjectById(StudateDate.class, dateKey);
		return date;
	}
	
	private boolean isBothDateParticipantsDuringDate(StudateDate date) {
		return (
			(date.getFirstStudentStatus().equals(DateConfirmationStatus.DURING_DATE)) && 
			(date.getSecondStudentStatus().equals(DateConfirmationStatus.DURING_DATE))
		);
	}
	
}
