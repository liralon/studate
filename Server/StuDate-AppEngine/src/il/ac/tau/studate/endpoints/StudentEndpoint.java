package il.ac.tau.studate.endpoints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import il.ac.tau.studate.entities.Gender;
import il.ac.tau.studate.entities.GenderPreferenceOptions;
import il.ac.tau.studate.entities.DevicePlatform;
import il.ac.tau.studate.entities.StringResponseEntity;
import il.ac.tau.studate.entities.Student;
import il.ac.tau.studate.utils.Constants;
import il.ac.tau.studate.utils.PMF;
import il.ac.tau.studate.utils.Utils;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.users.User;

@Api(
		name = "studentendpoint", 
		namespace = @ApiNamespace(ownerDomain = "ac.il", ownerName = "ac.il", packagePath = "tau.studate"), 
		clientIds = {Constants.IONIC_CLIENT_ID, Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID}
)
public class StudentEndpoint {
	
	@ApiMethod(name = "signIn")
	public Student signIn(User userInfo) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Student student = null;
		try {
			student = pm.getObjectById(Student.class, userInfo.getEmail());
		} catch (JDOObjectNotFoundException e) {
			// Nothing to do here (In this case, we will just return null to the client so it will know to request profile info from the user for registration)
		} finally {
			pm.close();
		}
		
		return student;
	}
	
	@ApiMethod(name = "unregister")
	public void unregister(User userInfo) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Student student = pm.getObjectById(Student.class, userInfo.getEmail());
			pm.deletePersistent(student);
			
			BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
			blobstoreService.delete(student.getProfilePictureBlobKey());
		} finally {
			pm.close();
		}
	}
	
	// Note - The reason "age" can be null is because we added this feature later on in the development process.
	// Note - Therefore, some user profiles may have null age.
	@ApiMethod(name = "editProfile")
	public void editProfile(
			User userInfo, 
			@Named("gender") Gender gender, 
			@Nullable @Named("age") Long age,
			@Named("genderPreference") GenderPreferenceOptions genderPreferenceOptions, 
			@Named("faculty") String faculty, 
			@Named("isInvisible") boolean isInvisible) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction transaction = pm.currentTransaction();
		try {
			transaction.begin();
			
			Student student = pm.getObjectById(Student.class, userInfo.getEmail());
			
			student.setGender(gender);
			if (age != null) {
				student.setAge(age);
			}
			student.setGenderPreference(genderPreferenceOptions);
			student.setFaculty(faculty);
			student.setIsInvisible(isInvisible);
			
			transaction.commit();
		} finally {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			
			pm.close();
		}
	}
	
	@ApiMethod(name = "registerDeviceForPushNotification")
	public void registerDeviceForPushNotification(
			User userInfo, 
			@Named("registerId") String registerId, 
			@Named("devicePlatform") DevicePlatform devicePlatform, 
			@Named("clientVersion") Double clientVersion) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction transaction = pm.currentTransaction();
		try {
			transaction.begin();
			
			String emailAddress = userInfo.getEmail();
			Student student = pm.getObjectById(Student.class, emailAddress);
			student.setDeviceId(registerId, devicePlatform);
			student.setClientVersion(clientVersion);
			
			transaction.commit();
			
			deleteDeviceIdFromAllOtherStudents(pm, transaction, registerId, emailAddress);
			
		} finally {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			
			pm.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void deleteDeviceIdFromAllOtherStudents(PersistenceManager pm, Transaction transaction, String deviceId, String excludeEmail) {
		transaction = pm.currentTransaction();
		try {
			transaction.begin();
			
			Query query = pm.newQuery(Student.class);
			query.setFilter("deviceId == newDeviceId");
			query.declareParameters("String newDeviceId");
			List<Student> studentsWithSameDeviceId = (List<Student>)query.execute(deviceId);
			
			for (Student student : studentsWithSameDeviceId) {
				if (!student.getEmailAddress().equals(excludeEmail)) {
					student.setDeviceId(null, student.getDevicePlatform());
				}
			}
			
			transaction.commit();
		} finally {
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}
	
	@ApiMethod(name = "getUploadURL")
	public StringResponseEntity getUploadURL(User userInfo, @Named("uploadPage") String uploadPage) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		String uploadURL = blobstoreService.createUploadUrl("/" + uploadPage);
		
		uploadUsers.put(uploadURL, userInfo);
		return new StringResponseEntity(uploadURL);
	}
	
	public static Map<String, User> uploadUsers = new HashMap<String, User>();
}
