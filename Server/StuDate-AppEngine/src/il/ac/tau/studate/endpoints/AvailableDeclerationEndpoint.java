package il.ac.tau.studate.endpoints;

import il.ac.tau.studate.entities.AvailableDecleration;
import il.ac.tau.studate.entities.DateState;
import il.ac.tau.studate.entities.Match;
import il.ac.tau.studate.entities.StudateDate;
import il.ac.tau.studate.entities.Student;
import il.ac.tau.studate.utils.Constants;
import il.ac.tau.studate.utils.ExclusiveLock;
import il.ac.tau.studate.utils.PMF;
import il.ac.tau.studate.utils.PushMessageTypes;
import il.ac.tau.studate.utils.PushNotification;
import il.ac.tau.studate.utils.Utils;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;

import javax.inject.Named;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(
		name = "avilabledeclerationendpoint", 
		namespace = @ApiNamespace(ownerDomain = "ac.il", ownerName = "ac.il", packagePath = "tau.studate"), 
		clientIds = {Constants.IONIC_CLIENT_ID, Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID}
)
public class AvailableDeclerationEndpoint {

	@ApiMethod(name = "declareAvailable")
	public void declareAvailable(User userInfo) throws UnauthorizedException, IOException {
		Utils.verifyUser(userInfo);
		declareAvailableLogic(userInfo.getEmail());
	}
	
	@ApiMethod(name = "debugDeclareAvailable")
	public void debugDeclareAvailable(User userInfo, @Named("email") String email) throws IOException, UnauthorizedException {
		Utils.verifyAdminUser(userInfo);
		declareAvailableLogic(email);
	}
	
	private static void declareAvailableLogic(String email) throws IOException {
		Key studentKey = KeyFactory.createKey(Student.class.getSimpleName(), email);
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			availablelock.lock();
			AvailableDecleration availableDecleration = new AvailableDecleration(email);
			pm.makePersistent(availableDecleration);
			
			StudateDate date = findPendingDateForUser(pm, studentKey);
			if (date != null) {
				pushDateInfoToClients(date, pm);
			}
			
		} finally {
			pm.close();
			availablelock.unlock();
		}
	}
	
	private static StudateDate findPendingDateForUser(PersistenceManager pm, Key studentKey) {
		StudateDate date = null;
		
		// TODO - This is a concurrency bug. The transaction.begin() & transaction.commit() should be on the entire function logic
		// TODO - However, this seems to be using a XG transaction that crosses to many entity groups (maximum of 5 is supported)
		// TODO - and therefore will fail...
		//Transaction transaction = pm.currentTransaction();
		try {
			//availablelock.lock();
			//transaction.begin();
			
			List<Key> avilableStudents = getAvilableUsers(pm, studentKey);
			List<Match> avilableMatches = getAvilableUserMatches(pm, avilableStudents, studentKey);
			
			if (!avilableMatches.isEmpty()) {
				Match match = avilableMatches.get(random.nextInt(avilableMatches.size()));
				date = addDateForMatch(pm, match);
			}
			
			// transaction.commit();
			
		} finally {
			/*if (transaction.isActive()) {
				transaction.rollback();
			}*/
			//availablelock.unlock();
		}
		
		return date;
	}
	
	public static StudateDate addDateForMatch(PersistenceManager pm, Match match) {
		StudateDate date = new StudateDate(match);
		pm.makePersistent(date);
		
		markStudentsMatchInDate(pm, match, date);
		
		return date;
	}
	
	private static void markStudentsMatchInDate(PersistenceManager pm, Match match, StudateDate date) {
		markStudentInDate(pm, match.getFirstStudent(), date);
		markStudentInDate(pm, match.getSecondStudent(), date);
	}
	
	private static void markStudentInDate(PersistenceManager pm, Key studentKey, StudateDate date) {
		Student student = pm.getObjectById(Student.class, studentKey);
		student.setDateState(DateState.DATE_CONFIRMATION);
		student.setPendingDateId(date.getDateId());
		
		markStudentNotAvialable(student.getEmailAddress(), pm);
	}
	
	@ApiMethod(name = "declareNotAvailable")
	public void declareNotAvailable(User userInfo) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		// TODO - This should be here but it causes XG-transactions bug because of operating on too many entity groups
		//Transaction transaction = pm.currentTransaction();
		try {
			//transaction.begin();
			
			String userEmailAddress = userInfo.getEmail();
			markStudentNotAvialable(userEmailAddress, pm);
			
			//transaction.commit();
		} finally {
			/*if (transaction.isActive()) {
				transaction.rollback();
			}*/
			
			pm.close();
		}
	}
	
	public static void markStudentNotAvialable(String userEmailAddress, PersistenceManager pm) {
		try {
			//availablelock.lock();
			AvailableDecleration availableDecleration = pm.getObjectById(AvailableDecleration.class, userEmailAddress);
			pm.deletePersistent(availableDecleration);
		} catch (JDOObjectNotFoundException e) {
			// Nothing to do here
			Utils.log(Level.WARNING, "Tried to mark user " + userEmailAddress + " not available when he was not available to begin with");
		} finally {
			//availablelock.unlock();
		}
	}
	
	@ApiMethod(name = "getAvailableStatus")
	public AvailableDecleration getAvailableStatus(User userInfo) throws UnauthorizedException {
		Utils.verifyUser(userInfo);
		
		AvailableDecleration availableDecleration = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			availableDecleration = pm.getObjectById(AvailableDecleration.class, userInfo.getEmail());
		} catch (JDOObjectNotFoundException e) {
			// Nothing to do here
		} finally {
			pm.close();
		}
		
		return availableDecleration;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Key> getAvilableUsers(PersistenceManager pm, Key studentKey) {
		Query query = pm.newQuery("SELECT student FROM " + AvailableDecleration.class.getName());
		query.declareImports("import com.google.appengine.api.datastore.Key");
		query.setFilter("(student != me)");
		query.declareParameters("Key me");
		return (List<Key>)query.execute(studentKey);
	}

	@SuppressWarnings("unchecked")
	public static List<Match> getAvilableUserMatches(PersistenceManager pm, List<Key> avilableStudents, Key studentKey) {
		// Note - This line fixes the problem that JDOQL "contains" keyword doesn't work with empty list (Broken java stuff...)
		if (avilableStudents.isEmpty()) {
			return new ArrayList<Match>();
		}
		
		// TODO - PERFORMANCE - We couldn't filter on contains & also filter on firstStudent
		// TODO - PERFORMANCE - Therefore, we do this manually :)
		
		Query query = pm.newQuery(Match.class);
		query.declareImports("import com.google.appengine.api.datastore.Key");
		query.setFilter("(firstStudent == student)");
		query.declareParameters("Key student");
		List<Match> matches = new ArrayList<Match>((List<Match>)query.execute(studentKey));
		
		Iterator<Match> it = matches.iterator();
		while (it.hasNext()) {
			Match match = it.next();
			if (!avilableStudents.contains(match.getSecondStudent())) {
				it.remove();
			}
		}
		
		return matches;
	}
	
	@ApiMethod(name = "debugPushDateInfoToClients")
	public void debugPushDateInfoToClients(User userInfo, @Named("dateId") Long dateId) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			StudateDate date = pm.getObjectById(StudateDate.class, dateId);
			
			sendDateIntoToClient(pm, date.getFirstStudent(), date.getDateId());
			sendDateIntoToClient(pm, date.getSecondStudent(), date.getDateId());
		} finally {
			pm.close();
		}
	}
	
	public static void pushDateInfoToClients(StudateDate date, PersistenceManager pm) throws IOException {
		sendDateIntoToClient(pm, date.getFirstStudent(), date.getDateId());
		sendDateIntoToClient(pm, date.getSecondStudent(), date.getDateId());
	}
	
	private static void sendDateIntoToClient(PersistenceManager pm, Key studentToSendTo, Long dateId) throws IOException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("dateId", dateId.toString());
		
		PushNotification.sendMessageToStudent(pm, studentToSendTo, PushMessageTypes.DATE_ALERT, "You have a date!", properties);
	}
	
	private static final Random random = new Random();
	private static final ExclusiveLock availablelock = new ExclusiveLock("AvailableLock");
	
}
