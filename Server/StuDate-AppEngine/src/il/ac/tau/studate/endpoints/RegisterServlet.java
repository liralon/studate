package il.ac.tau.studate.endpoints;

import il.ac.tau.studate.entities.Gender;
import il.ac.tau.studate.entities.GenderPreferenceOptions;
import il.ac.tau.studate.entities.Student;
import il.ac.tau.studate.utils.PMF;
import il.ac.tau.studate.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.users.User;

@SuppressWarnings("serial")
public class RegisterServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Map<String, List<BlobKey>> uploads = blobstoreService.getUploads(request);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		// TODO - transactions don't really work...
		//Transaction transaction = pm.currentTransaction();
		List<String> blobsNotToDelete = new ArrayList<String>();
		try {
			String uploadURL = request.getParameter("uploadURL");
			final User userInfo = StudentEndpoint.uploadUsers.get(uploadURL);
			StudentEndpoint.uploadUsers.remove(uploadURL);
			/*final User userInfo = Utils.verifyUserFromServlet(response); 
			if (userInfo == null) {
				Utils.deleteAllUploadBlobs(blobstoreService, uploads);
				return;
			}*/
			
			final String studentName = request.getParameter("name");
			final String faculty = request.getParameter("faculty");
			final Long age = Long.parseLong(request.getParameter("age"));
			final Gender gender = Gender.valueOf(request.getParameter("gender"));
			final GenderPreferenceOptions genderPreferenceOptions = GenderPreferenceOptions.valueOf(request.getParameter("genderPreferences"));
			
			
			List<BlobKey> profilePictures = blobstoreService.getUploads(request).get("profilePicture");
			if (profilePictures == null) {
				response.getWriter().println("Failed to receive profilePicture");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			
			BlobKey profilePicture = profilePictures.get(0);
			//transaction.begin();
			Student student = new Student(
					userInfo.getEmail(), 
					studentName, 
					profilePicture, 
					faculty,
					gender,
					age,
					genderPreferenceOptions);
			pm.makePersistent(student);
			blobsNotToDelete.add("profilePicture");
			//transaction.commit();
			
			// Note - This is a hack used to indicate to the client that the profilePicture the server got actually was empty.
			// Note - This is caused on some old Android phones (4.2 and below).
			// Note - When the client will receive this, it will immediately call editProfilePicture API after the registration.
			long profilePictureSize = blobstoreService.getBlobInfos(request).get("profilePicture").get(0).getSize();
			response.getWriter().print(profilePictureSize);
			
			response.setStatus(HttpServletResponse.SC_OK);
		
		} finally {
			/*if (transaction.isActive()) {
				transaction.rollback();
			}*/
			
			pm.close();
			Utils.deleteAllUploadBlobs(blobstoreService, uploads, blobsNotToDelete);
		}
	}
}
