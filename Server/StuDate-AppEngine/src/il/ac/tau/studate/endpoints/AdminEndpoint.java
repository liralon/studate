package il.ac.tau.studate.endpoints;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import il.ac.tau.studate.entities.DateState;
import il.ac.tau.studate.entities.Gender;
import il.ac.tau.studate.entities.GenderPreferenceOptions;
import il.ac.tau.studate.entities.Student;
import il.ac.tau.studate.utils.PMF;
import il.ac.tau.studate.utils.PushMessageTypes;
import il.ac.tau.studate.utils.PushNotification;
import il.ac.tau.studate.utils.Utils;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;

@Api(name = "adminendpoint", namespace = @ApiNamespace(ownerDomain = "ac.il", ownerName = "ac.il", packagePath = "tau.studate.entities"))
public class AdminEndpoint {
	
	@SuppressWarnings("unchecked")
	@ApiMethod(name = "broadcastMessage")
	public void broadcastMessage(
			User userInfo, 
			@Named("pushNotificationText") String pushNotificationText, 
			@Named("messageText") String messageText) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Map<String, String> properties = new HashMap<String, String>();
			properties.put("messageText",  messageText);
			
			Query query = pm.newQuery(Student.class);
			List<Student> students = (List<Student>)query.execute();
			for (Student student : students) {
				PushNotification.sendMessageToStudent(student, PushMessageTypes.BROADCAST_MESSAGE, pushNotificationText, properties);
			}
		} finally {
			pm.close();
		}
	}
	
	@ApiMethod(name = "debugSetStudentDateState")
	public void setStudentDateState(User userInfo, @Named("email") String email, @Named("dateState") DateState dateState) throws UnauthorizedException {
		Utils.verifyAdminUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction transaction = pm.currentTransaction();
		try {
			transaction.begin();
			
			Student student = pm.getObjectById(Student.class, email);
			student.setDateState(dateState);
			
			transaction.commit();
		} finally {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			
			pm.close();
		}
	}
	
	@ApiMethod(name = "debugRegisterUser")
	public void registerUserDebug(
			User userInfo, 
			@Named("email") String email, 
			@Named("name") String name,
            @Named("age") Long age,
			@Named("faculty") String faculty, 
			@Named("gender") Gender gender,
			@Named("genderPreferences") GenderPreferenceOptions genderPreferenceOptions) throws UnauthorizedException {
		Utils.verifyAdminUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction transaction = pm.currentTransaction();
		try {
			transaction.begin();
			
			Student student = new Student(
					email, 
					name, 
					null, 
					faculty,
					gender,
                    age,
					genderPreferenceOptions);
			pm.makePersistent(student);
			
			transaction.commit();
		} finally {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			
			pm.close();
		}
	}
	
	@ApiMethod(name = "debugPushNotifyAndroid")
	public void debugPushNotifyAndroid(
			User userInfo, 
			@Named("registrationId") String registrationId,
			@Named("messageType") PushMessageTypes messageType, 
			@Named("messsage") String message) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("messageType", messageType.toString());
		
		PushNotification.sendMessageAndroid(registrationId, message, properties);
	}
	
	@ApiMethod(name = "debugPushNotifyIOS")
	public void debugPushNotifyIOS(
			User userInfo, 
			@Named("registrationId") String registrationId,
			@Named("messageType") PushMessageTypes messageType, 
			@Named("messsage") String message) throws UnauthorizedException, IOException {
		Utils.verifyAdminUser(userInfo);
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("messageType", messageType.toString());
		
		PushNotification.sendMessageIOS(registrationId, message, properties);
	}
	
	@ApiMethod(name = "debugSetIsInvisible")
	public void setIsInvisible(User userInfo, @Named("email") String email, @Named("isInvisible") boolean isInvisible) throws UnauthorizedException {
		Utils.verifyAdminUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction transaction = pm.currentTransaction();
		try {
			transaction.begin();
			
			Student student = pm.getObjectById(Student.class, email);
			student.setIsInvisible(isInvisible);
			
			transaction.commit();
		} finally {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			
			pm.close();
		}
	}

}
