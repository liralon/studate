package il.ac.tau.studate.endpoints;

import il.ac.tau.studate.entities.Student;
import il.ac.tau.studate.utils.PMF;
import il.ac.tau.studate.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.users.User;

@SuppressWarnings("serial")
public class EditProfilePictureServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Map<String, List<BlobKey>> uploads = blobstoreService.getUploads(request);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		// TODO - Transactions don't really work
		//Transaction transaction = pm.currentTransaction();
		List<String> blobsNotToDelete = new ArrayList<String>();
		try {
			String uploadURL = request.getHeader("uploadURL");
			final User userInfo = StudentEndpoint.uploadUsers.get(uploadURL);
			StudentEndpoint.uploadUsers.remove(uploadURL);
			/*final User userInfo = Utils.verifyUserFromServlet(response); 
			if (userInfo == null) {
				Utils.deleteAllUploadBlobs(blobstoreService, uploads);
				return;
			}*/
			
			List<BlobKey> profilePictures = blobstoreService.getUploads(request).get("profilePicture");
			if (profilePictures == null) {
				response.getWriter().println("Failed to receive profilePicture");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			BlobKey profilePicture = profilePictures.get(0);
			
			//transaction.begin();
			
			Student student = pm.getObjectById(Student.class, userInfo.getEmail());
			BlobKey previousProfilePicture = student.getProfilePictureBlobKey();
			student.setProfilePictureBlobKey(profilePicture);
			blobsNotToDelete.add("profilePicture");
			blobstoreService.delete(previousProfilePicture);
			
			//transaction.commit();
			
			response.setStatus(HttpServletResponse.SC_OK);
			
		} finally {
			/*if (transaction.isActive()) {
				transaction.rollback();
			}*/
			
			pm.close();
			Utils.deleteAllUploadBlobs(blobstoreService, uploads, blobsNotToDelete);
		}
	}
}
