package il.ac.tau.studate.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.users.User;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import il.ac.tau.studate.entities.Student;
import il.ac.tau.studate.entities.StudentChoice;
import il.ac.tau.studate.utils.Constants;
import il.ac.tau.studate.utils.MatchUtils;
import il.ac.tau.studate.utils.PMF;
import il.ac.tau.studate.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(
		name = "studentchoiceendpoint", 
		namespace = @ApiNamespace(ownerDomain = "ac.il", ownerName = "ac.il", packagePath = "tau.studate"), 
		clientIds = {Constants.IONIC_CLIENT_ID, Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID}
)
public class StudentChoiceEndpoint { 
	
	@ApiMethod(name = "getNextProfilesOfUsersThatLikeMe", path = "collectionresponse_getNextProfilesOfUsersThatLikeMe")
	public CollectionResponse<Student> getNextProfilesOfUsersThatLikeMe(
			@Named("numberOfProfiles") int numberOfProfiles, 
			User userInfo) throws UnauthorizedException {
		
		Utils.verifyUser(userInfo);
		
		if (0 == numberOfProfiles) {
			return CollectionResponse.<Student> builder().build();
		}
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		List<Student> students = null;
		try {
			students = new ArrayList<Student>(getUsersThatLikeMe(pm, userInfo));
			filterStudents(pm, userInfo, students);
			
			students.subList(0, numberOfProfiles);
		} finally {
			pm.close();
		}
		
		return CollectionResponse.<Student> builder().setItems(students).build();
	}
	
	@SuppressWarnings("unchecked")
	@ApiMethod(name = "getNextProfiles", path = "collectionresponse_getNextProfiles")
	public CollectionResponse<Student> getNextProfiles(
			@Nullable @Named("cursor") String cursorString, 
			@Named("numberOfProfiles") int numberOfProfiles, 
			User userInfo) throws UnauthorizedException {
		
		Utils.verifyUser(userInfo);
		
		if (0 == numberOfProfiles) {
			return CollectionResponse.<Student> builder().build();
		}
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		List<Student> returnedStudents = new ArrayList<Student>();
		try {
			// Note - This loop is necessary because even though we indeed query only "numberOfProfiles" rows, we later filter them manually
			// Note - This can cause the number of returned profiles to actually be smaller than the number of profiles requested
			while (returnedStudents.size() < numberOfProfiles) {
				Query query = Utils.getQueryFromCursorString(pm, Student.class, cursorString, numberOfProfiles);
				List<Student> students = (List<Student>)query.execute();
				
				Cursor cursor = JDOCursorHelper.getCursor(students);
				cursorString = cursor.toWebSafeString();
				
				students = new ArrayList<Student>(students);
				if (students.isEmpty()) {
					break;
				}
				filterStudents(pm, userInfo, students);
				returnedStudents.addAll(students);
			}
		} finally {
			pm.close();
		}
		
		return CollectionResponse.<Student> builder().setItems(returnedStudents).setNextPageToken(cursorString).build();
	}
	
	@ApiMethod(name = "setProfileAnswers", path = "setProfileAnswers")
	public CollectionResponse<Student> setProfileAnswers(
			@Named("emails") String[] emails, 
			@Named("results") boolean[] results, 
			@Nullable @Named("cursor") String cursorString, 
			@Named("numberOfProfiles") int numberOfProfiles, 
			@Named("shouldReturnOnlyUsersThatLikeMe") boolean shouldReturnOnlyUsersThatLikeMe, 
			User userInfo) throws UnauthorizedException, IOException, EntityNotFoundException {
		Utils.verifyUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try {
			writeUserChoicesAndUpdateMatches(pm, userInfo.getEmail(), emails, results);	
		} finally {
			if (pm.currentTransaction().isActive()) {
				pm.currentTransaction().rollback();
			}
			pm.close();
		}
		
		return (shouldReturnOnlyUsersThatLikeMe) ? 
				(this.getNextProfilesOfUsersThatLikeMe(numberOfProfiles, userInfo)) :
				(this.getNextProfiles(cursorString, numberOfProfiles, userInfo));
	}
	
	@ApiMethod(name = "addStudentChoice", path = "addStudentChoicePath")
	public void addStudentChoice(
			User userInfo, 
			@Named("choosingStudentEmail") String choosingStudentEmail, 
			@Named("emails") String[] emails, 
			@Named("results") boolean[] results) throws IOException, EntityNotFoundException, UnauthorizedException {
		Utils.verifyAdminUser(userInfo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try {
			writeUserChoicesAndUpdateMatches(pm, choosingStudentEmail, emails, results);	
		} finally {
			pm.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<Student> getUsersThatLikeMe(PersistenceManager pm, User userInfo) {
		List<String> usersThatLikeMeEmailAddresses = getUsersThatLikeMeEmailAddresses(pm, userInfo.getEmail());
		// Note - This line fixes the problem that JDOQL "contains" keyword doesn't work with empty list (Broken java stuff...)
		if (usersThatLikeMeEmailAddresses.isEmpty()) {
			return new ArrayList<Student>();
		}
		
		Student student = pm.getObjectById(Student.class, userInfo.getEmail());
		Query query = pm.newQuery(Student.class);
		query.setFilter("(:p1.contains(emailAddress)) && (:p2.contains(gender))");
		// TODO - Think - Is it a problem we don't limit the query here? I don't think so...
		List<Student> studentsThatLikeMe = (List<Student>)query.execute(usersThatLikeMeEmailAddresses, student.getGenderPreference());
		return studentsThatLikeMe;
	}	
	
	@SuppressWarnings("unchecked")
	private List<String> getUsersThatLikeMeEmailAddresses(PersistenceManager pm, String email) {
		Query query = pm.newQuery("SELECT studentEmailAddress FROM " + StudentChoice.class.getName());
		query.setFilter("(candidateEmailAddress == emailAddress) && (doesLike == true)");
		query.declareParameters("String emailAddress");
		List<String> studentsEmailAddresses = (List<String>)query.execute(email);
		return studentsEmailAddresses;
	}
	
	private void filterStudents(PersistenceManager pm, User userInfo, List<Student> students) {
		Student currentStudent = pm.getObjectById(Student.class, userInfo.getEmail());
		List<String> genderPreference = currentStudent.getGenderPreference();
		
		List<String> usersIHaveAnsweredEmails = getUsersThatIAnsweredEmailAddresses(pm, userInfo);
		
		Iterator<Student> it = students.iterator();
		while (it.hasNext()) {
			Student student = it.next();
			
			// Filter out invisible students
			if (student.getIsInvisible()) {
				it.remove();
				continue;
			}
			
			// Filter out students from wrong gender
			if (!genderPreference.contains(student.getGender())) {
				it.remove();
				continue;
			}
			
			// Filter out student that don't like my gender
			if (!student.getGenderPreference().contains(currentStudent.getGender())) {
				it.remove();
				continue;
			}
	
			// Filter out students that I have already answered
			if (usersIHaveAnsweredEmails.contains(student.getEmailAddress())) {
				it.remove();
				continue;
			}
			
			// Filter out myself :)
			if (student.getEmailAddress().equals(userInfo.getEmail())) {
				it.remove();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<String> getUsersThatIAnsweredEmailAddresses(PersistenceManager pm, User userInfo) {
		Query query = pm.newQuery("SELECT candidateEmailAddress FROM " + StudentChoice.class.getName());
		query.setFilter("studentEmailAddress == emailAddress");
		query.declareParameters("String emailAddress");
		List<String> studentsEmailAddresses = (List<String>)query.execute(userInfo.getEmail());
		return studentsEmailAddresses;
	}
	
	private void writeUserChoicesAndUpdateMatches(PersistenceManager pm, String choosingStudentEmail, String[] emails, boolean[] results) throws IOException, EntityNotFoundException {
		List<String> usersThatLikedMeEmails = new ArrayList<String>(getUsersThatLikeMeEmailAddresses(pm, choosingStudentEmail));
		
		// TODO - XG-transactions don't really work...
		//Transaction transaction = pm.currentTransaction();
		try {
			//transaction.begin();
			
			for (int i = 0 ; i < emails.length ; ++i) {
				// Should never happen but insure consistency when playing with DB manually
				//deleteUserChoiceIfExists(pm, choosingStudentEmail, emails[i]);
				
				StudentChoice choice = new StudentChoice(choosingStudentEmail, emails[i], results[i]);
				pm.makePersistent(choice);
			}
			
			for (int i = 0 ; i < emails.length ; ++i) {
				if ((results[i]) && usersThatLikedMeEmails.contains(emails[i])) {
					MatchUtils.addMatchByEmails(pm, choosingStudentEmail, emails[i]);
				}
			}
	
			//transaction.commit();
			
		} finally {
			/*if (transaction.isActive()) {
				transaction.rollback();
			}*/
		}
	}
	
	// TODO - Decide if we want this code or not (Called in comment above)
	/*@SuppressWarnings("unchecked")
	private void deleteUserChoiceIfExists(PersistenceManager pm, String choosingStudentEmail, String candidateEmailAddress) {
		Query query = pm.newQuery(StudentChoice.class);
		query.setFilter("(choosingStudentEmail == studentEmailAddress) && (candidateEmailAddressParam == candidateEmailAddress)");
		query.declareParameters("String choosingStudentEmail, String candidateEmailAddressParam");
		List<StudentChoice> choices = (List<StudentChoice>)query.execute(choosingStudentEmail, candidateEmailAddress);
		
		pm.deletePersistentAll(choices);
	}*/
	
}
