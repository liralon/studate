﻿
Installing on windows:
(1) Install Node.js
(2) Open the "Git bash"
(3) Type in it: "npm install -g cordova"
(4) Then, type in it: "npm install -g ionic"

Running the project:
(1) Navigate in the "Git bash" to the project directory
(2) Run "ionic serve [port number]"
(3) A browser will open with our project :)

Local Server Debugging Note:
Open chrome with "chrome --disable-web-security" (To prevent COSR violations...)

Installing the project for mobile:
(1) Run "cordova plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-inappbrowser.git"
(2) Run “cordova plugin add org.apache.cordova.camera”
(3) Run “cordova plugin add org.apache.cordova.splashscreen”
(4) Run “cordova plugin add org.apache.cordova.device”
(5) Run “cordova plugin add https://github.com/phonegap-build/PushPlugin.git”
(6) Run "cordova plugin add https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin.git"
(7) Run "cordova plugin add org.apache.cordova.network-information"
(8) Run "cordova plugin add com.ionic.keyboard"
(9) Run "cordova plugin add https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git"
	(9.1) If it doesn't work Run "cordova plugin add nl.x-services.plugins.socialsharing"
(10) Run "cordova plugin add https://github.com/bez4pieci/Phonegap-Cookies-Plugin.git"
(11) Run "ionic platform add android" / "ionic platform add ios"

Debugging on mobile:
(0) "adb logcat -c"
(1) "adb logcat -v time CordovaLog:D \*:S"
(2) Useful link to check compatability: http://caniuse.com/#search=border-radius

***Note: if you have an existing platform, and you install plugins, you must remove the platform and add it again using ionic platform add android/ ionic platform add ios*****

Running the project on mobile:
(1) Run "ionic emulate android" / "ionic emulate ios"

Developing Tips:
(*) Awesome example that you can learn almost everything we need from it:
	https://code.angularjs.org/1.2.26/docs/tutorial
(*) Very detailed toturial on everything you need to know on AngularJS: 
	https://thinkster.io/angulartutorial/a-better-way-to-learn-angularjs/
(*) AngularJS API Documentation:
	https://docs.angularjs.org/api
(*) All the Ionic UI Elements & CSS Attributes with examples 
	(Basicly, everything you need to know to design the UI):
		http://ionicframework.com/docs/
(*) Apache Cordova documentation:
	http://cordova.apache.org/docs/en/4.0.0/


Publishing (android):
(*) change the version - edit the config.xml file to increment the android:versionCode value. (e.g. 0.0.1)
(*) change the version in config.js (e.g 0.1)
(*) build release - cordova build android --release