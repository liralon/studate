
// Note - Code taken from: https://gist.github.com/mircobabini/689955216a036544d594

angular.module('HardwareBackButtonManagerModule', []).service('HardwareBackButtonManager', function($ionicPlatform) {
    this.deregister = undefined;

    this.disable = function()
    {
        this.deregister = $ionicPlatform.registerBackButtonAction(function(e)
        {
            e.preventDefault();
            return false;
        }, 101);
    };

    this.enable = function()
    {
        if (this.deregister !== undefined)
        {
            this.deregister();
            this.deregister = undefined;
        }
    };

    return this;
});
