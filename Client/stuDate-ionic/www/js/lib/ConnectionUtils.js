
angular.module('ConnectionUtilsModule', []).factory('$connectionUtils', function() {

    function ConnectionUtils()
    {
        // Nothing to do here
    }

    // Note - This must be called after "deviceReady" event was triggered
    ConnectionUtils.prototype.init = function()
    {
        this.networkConnection = navigator.connection;
        if (!navigator.connection)
        {
            console.log('this.networkConnection.type not defined (Probably means "network information" ionic plugin not installed');
            return;
        }

        console.log('connectionType: ' + this.getConnectionType());
    };

    // This returns one of the following:
    // UNKNOWN: "unknown", "ethernet", "wifi",
    // "2g", "3g", "4g", "cellular", "none"
    ConnectionUtils.prototype.getConnectionType = function()
    {
        // Note - This "if" is a workaround a bug of cordova 2.2.0 (For more info: https://issues.apache.org/jira/browse/CB-1807)
        return ((navigator.connection.type) ? (navigator.connection.type) : (navigator.network.connection.type));
    };

    ConnectionUtils.prototype.isConnectedToInternet = function()
    {
        // Note - Make code still work properly on PC
        if (!this.networkConnection)
        {
            return true;
        }

        return (this.getConnectionType() != 'none');
    };

    ConnectionUtils.prototype.registerConnectionStatusHandlers = function(offlineHandler, onlineHandler)
    {
        document.addEventListener('offline', offlineHandler, false);
        document.addEventListener('online', onlineHandler, false);
    };

    return (new ConnectionUtils());
});
