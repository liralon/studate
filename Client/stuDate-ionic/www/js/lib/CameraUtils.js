
angular.module('CameraModule', []).factory('$cameraUtils', function($cordovaCamera, $jrCrop){
  
function CameraUtils()
{
    this.pictureOptions = {
        quality : 100,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : false,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 400,
        targetHeight: 534,
        //popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true,
        cameraDirection: 1
    };
}

CameraUtils.prototype.takePictureFromSource = function(sourceType, finishedCallback)
{
    this.pictureOptions.sourceType = sourceType;

    $cordovaCamera.getPicture(this.pictureOptions).then((function(imageData)
    {
        this.cropImg(imageData, finishedCallback);
    }).bind(this), function(errorInfo)
    {
        finishedCallback(null);
    });
};

CameraUtils.prototype.takePicture = function(finishedCallback)
{
    this.takePictureFromSource(Camera.PictureSourceType.CAMERA, finishedCallback);
};

CameraUtils.prototype.takeFromGallery = function(finishedCallback) {
    this.takePictureFromSource(Camera.PictureSourceType.PHOTOLIBRARY, finishedCallback);
};

CameraUtils.prototype.cropImg = function(imageData, finishedCallback) { 
    $jrCrop.crop({
        url: "data:image/jpeg;base64," + imageData,
        width: 350,
        height: 350,
        title: 'Select image area'
    }).then(function(canvas) 
    {
        var newImageData = canvas.toDataURL('image/jpeg');
        if (newImageData.indexOf('image/jpeg') == (-1))
        {
            var encoder = new JPEGEncoder();
            newImageData = encoder.encode(canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height), 100);
        }
        newImageData = newImageData.slice("data:image/jpeg;base64,".length);
        finishedCallback(newImageData);
    }, function() {
        finishedCallback(null);
    });
};

return (window.Camera) ? (new CameraUtils()) : (null);

});
