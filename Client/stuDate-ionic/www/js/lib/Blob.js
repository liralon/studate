
// Note - copied from:
// https://books.google.co.il/books?id=mKqkAwAAQBAJ&pg=PT286&lpg=PT286&dq=cordova+plugin+add+polyfill-blob-constructor&source=bl&ots=yqpPz7q_v6&sig=2X-ug4GfyRl5sK8uYfu7_dbI13k&hl=en&sa=X&ei=VwSYVKafHsHmaoe_gtAD&ved=0CCkQ6AEwAg#v=onepage&q=cordova%20plugin%20add%20polyfill-blob-constructor&f=false
// https://github.com/MobileChromeApps/mobile-chrome-apps/blob/master/chrome-cordova/plugins/polyfill-blob-constructor/Blob.js

// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

try {
    console.log('Blob.js: ' + typeof Blob);
    new Blob([]);
} catch (e) {
    var nativeBlob = window.Blob;
    var newBlob = function(parts, options) {
        console.log('Blob constructor called!');
        if (window.WebKitBlobBuilder) {
            console.log('Blob constructor going to success!');
            var bb = new WebKitBlobBuilder();
            if (parts && parts.length) {
                for (var i=0; i < parts.length; i++) {
                    bb.append(parts[i]);
                }
            }
            if (options && options.type) {
                return bb.getBlob(options.type);
            }
            return bb.getBlob();
        } else {
            console.log('Blob constructor is going to fail...');
            throw 'Did not find WebKitBlobBuilder!';
        }
    };
    newBlob.prototype = nativeBlob.prototype;
    window.Blob = newBlob;
}

