
angular.module('PushNotificationModule', []).factory('$pushNotifications', function($studate, $window) {

function PushNotifications()
{
    this.MILLISECONDS_TO_WAIT_TO_BE_READY_FOR_PUSH_NOTIFICATIONS = 3000;

    this.listeners = {};
    this.isReadyToReceiveNotifications = false;     // Note - This flag is used to support "coldstart" correctly
    this.registrationId = '';
    this.canSendRegistrationIdToServer = false;
}

PushNotifications.prototype.init = function()
{
    // Note - This is done to not show error when running on desktop
    if (typeof window.plugins == 'undefined')
    {
        return;
    }

    this.pushNotificationPlugin = window.plugins.pushNotification;
    if (this.pushNotificationPlugin)
    {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    }
};

PushNotifications.prototype.isPushRegistrationSuccessful = function() 
{
    return ((this.registrationId.length > 0) || (!this.isRunningSupportedPlatform()));
}; 

PushNotifications.prototype.isRunningSupportedPlatform = function()
{
	if ((this.pushNotificationPlugin) && ($window.device))
    {
	    var devicePlatform = $window.device.platform.toUpperCase();
	    return ((devicePlatform == 'ANDROID') || (devicePlatform == 'IOS'));
	}
	else
    {
		return false;
	}
};

PushNotifications.prototype.onDeviceReady = function()
{
    console.log('onDeviceReady called!');

    this.devicePlatform = $window.device.platform.toUpperCase();
    console.log('device: ' + JSON.stringify($window.device));
    console.log('devicePlatform = ' + this.devicePlatform);

    if (this.devicePlatform == 'ANDROID')
    {
        // Register with google GCM server
        this.pushNotificationPlugin.register(
            this.onPushServerRegistrationSuccess.bind(this),
            this.onPushServerRegistrationError.bind(this),
            {
                'senderID': $studate.projectId,
                'ecb': 'onNotificationGCM'
            });
    }
    else if (this.devicePlatform == 'IOS')
    {
        // Register with apple APN server
        this.pushNotificationPlugin.register(
            this.onPushServerRegistrationSuccess.bind(this),
            this.onPushServerRegistrationError.bind(this),
            {
                "badge": "true",
                "sound": "true",
                "alert": "true",
                "ecb": "onNotificationAPN"
            });
    }
};

PushNotifications.prototype.unregister = function()
{
    if (this.pushNotificationPlugin)
    {
        this.pushNotificationPlugin.unregister(function()
        {
            console.log('Finished unregistering from push notifications');
        });
    }
};

PushNotifications.prototype.onPushServerRegistrationSuccess = function(result)
{
    console.log('Successfully registered against Result = '+ result);

    if (this.devicePlatform == 'IOS')
    {
        // result is the deviceRegistrationId in the iOS case
        this.setRegistrationId(result);
    }
    else
    {
        // Nothing to do here (We receive the registration ID later on android...)
    }
};

PushNotifications.prototype.onPushServerRegistrationError = function(result)
{
    consoler.log('Failed to register against Result = '+ result);
};

PushNotifications.prototype.registerListener = function(messageType, listener)
{
    this.listeners[messageType] = listener;
};

PushNotifications.prototype.unregisterAllListeners = function()
{
    this.listeners = {};
};

PushNotifications.prototype.unregisterListener = function(messageType)
{
    delete this.listeners[messageType];
};

PushNotifications.prototype.setReadyToReceiveNotifications = function()
{
    this.isReadyToReceiveNotifications = true;
};

PushNotifications.prototype.sendToListener = function(message, isColdStart)
{
    if (this.isReadyToReceiveNotifications)
    {
        this.listeners[message.messageType](message, isColdStart);
    }
    else
    {
        setTimeout(this.sendToListener.bind(this, message), this.MILLISECONDS_TO_WAIT_TO_BE_READY_FOR_PUSH_NOTIFICATIONS);
    }
};

PushNotifications.prototype.setUserSignedIn = function()
{
    if (this.isRunningSupportedPlatform())
    {
        if (this.registrationId.length > 0)
        {
            this.sendRegistrationIdNoCallback();
        }
        else
        {
            console.log('Registration Id not received yet after signIn. Will be sent to server upon received.');
            this.canSendRegistrationIdToServer = true;
        }
    }
};

PushNotifications.prototype.setRegistrationId = function(registrationId)
{
    this.registrationId = registrationId;
    console.log('Registration ID: ' + this.registrationId);

    if (this.canSendRegistrationIdToServer)
    {
        this.sendRegistrationIdNoCallback();
    }
};

PushNotifications.prototype.sendRegistrationIdNoCallback = function()
{
    console.log('Sending registration id!');
    this.sendRegistrationId(function(isSuccess)
    {
        console.log('sentRegistrationId result: ' + isSuccess);
        // Do nothing (The user has nothing to do in case this fails so we better not tell him it happened)
    });
};

PushNotifications.prototype.sendRegistrationId = function(finishedCallback)
{
    var params = {
        registerId: this.registrationId,
        devicePlatform: this.devicePlatform,
        clientVersion: Config.studateAppVersion
    };
    gapi.client.studentendpoint.registerDeviceForPushNotification(params).execute(function(response)
    {
        var didRegisterSucceed = $studate.didAPISucceed(response);
        finishedCallback(didRegisterSucceed);
    });
};

PushNotifications.prototype.onNotificationGCM = function(e)
{
    console.log('onNotificationGCM: ' + JSON.stringify(e));

    switch (e.event)
    {
        case 'registered':
            this.setRegistrationId(e.regid);
            break;

        case 'message':
            if (e.foreground)
            {
                // Note - We got a push notification while the user was in the app
                // Note - In this case, there is no sound and no notification in the notification tray

                // if the notification contains a soundname, play it.
                //var my_media = new Media("/android_asset/www/" +e.payload.soundname);
                //my_media.play();
                console.log('FOREGROUND PUSH NOTIFICATION!');
            }
            else
            {
                if (e.coldstart)
                {
                    // Note - This means that the application was launched (While it was closed!)
                    // Note - because the user touched the notification in the notification tray
                    console.log('COLDSTART PUSH NOTIFICATION!');
                }
                else
                {
                    // Note - The applicaiton was running in the background.
                    // Note - We got back to the app because the user clicked the notification in the notification tray
                    console.log('BACKGROUND PUSH NOTIFICATION!');
                }
            }

            // Note - e.payload.message contains the actual message
            // Note - e.payload.soundname contains sound file name (wav file path)
            this.sendToListener(e.payload, e.coldstart);
            break;

        case 'error':
            console.error('Got error from push notifictaion: ' + e.msg);
            break;

        default:
            console.error('Got an Unknown push notification event');
            break;
    }
};

PushNotifications.prototype.onNotificationAPN = function(e)
{
    console.log('onNotificationAPN: ' + JSON.stringify(e));
    if (e.alert)
    {
        //navigator.notification.alert(e.alert);
        console.log('onNotificationAPN: inside e.alert, sending to listener');
        this.sendToListener(e, 0);
    }

    if (e.sound)
    {
        var snd = new Media(e.sound);
        snd.play();
    }

    if (e.badge)
    {
        // TODO - HANDLE THIS
        //pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
    }
};

var pushNotificationsInstance = new PushNotifications();

// Handle Android push notifications
$window.onNotificationGCM = function(e)
{
    return pushNotificationsInstance.onNotificationGCM(e);
};

// Handle iOS push notifications
$window.onNotificationAPN = function(e)
{
    return pushNotificationsInstance.onNotificationAPN(e);
};

// Note - If we are running from the browser
// Note - Then, we will want a "Push Notification Simulator" that we can activate from the console (for debugging)
if (Config.runningFromBrowser)
{
    $window.pushMessage = function(message, isColdStart)
    {
        pushNotificationsInstance.sendToListener(message, isColdStart);
    };
}

return pushNotificationsInstance;

});
