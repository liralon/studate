angular.module('StudateActionSheets', []).factory('$studateActionSheets', function($ionicActionSheet, $timeout, $ionicPopup) {

	function StudateActionSheets()
    {
        // Nothing to do here
    }

    StudateActionSheets.prototype.showGetOutOfDateSheet = function(getOutHandler, cancelHandler) {
    	// Show the action sheet
        var hideSheet = $ionicActionSheet.show({
            buttons: [{ text: 'Leave Date' }],
            destructiveText: 'Delete Date <br><sub>Don\'t show me this person anymore</sub>',
            cancelText: 'Cancel',
            cancel: cancelHandler,
            destructiveButtonClicked: function() {
                var alertPopup = $ionicPopup.confirm({
                    title: 'Are you sure?',
                    template: 'After confirm you will not be <br> able to date this person anymore',
                    cancelText: 'No',
                    cancelType: 'button-' + Config.appMood,
                    okText: 'Yes',
                    okType: 'button-' + Config.appMood
                });
                alertPopup.then(function (res) {
                    if (res) {
                        console.log('DeleteDate: User selected to delete date');
                        getOutHandler(true);
                    } else {
                        console.log('DeleteDate: User selected to cancel');
                    }
                });
                return true;    // Returning true causes the action sheet to be closed
            },
            buttonClicked: function(index) {
                getOutHandler(false);
            }
        });

        // Hide the action sheet after 4 seconds (To prevent mistakes)
        $timeout(function() { hideSheet(); }, 4000);
    };

    StudateActionSheets.prototype.showEditProfilePicSheet = function(cameraPicCallback, galleryPicCallback){

    	var hideSheet = $ionicActionSheet.show({
            buttons: [
                { text: '<a class="ion-camera">Camera<a>' },
                { text: '<a class="icon-left ion-image">Gallery</a>' }
            ],
            titleText: '<b>Set profile picture</b>',
            cancelText: 'Cancel',
            cancel: function() {},
            buttonClicked: function(index)
            {
                switch(index) {
                    case 0:
                        cameraPicCallback();
                        break;
                    case 1:
                        galleryPicCallback();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        // Hide the action sheet after 4 seconds (To prevent mistakes)
        $timeout(function() { hideSheet(); }, 4000);
    };

    return (new StudateActionSheets());
});