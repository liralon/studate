
angular.module('studateAppEngine').factory('$users', function($window, $http, $studate, $oauthLogin, $connectionUtils)
{

function Users()
{
    this.OAUTH_LOGIN_FAILURE = 0;
    this.COMMUNICATION_FAILURE = 1;
    this.NO_STUDATE_USER_FAILURE = 2;
    this.AUTHENTICATION_FAILURE = 3;
}

Users.prototype.signIn = function(finishedSignInCallback)
{
    $oauthLogin.signIn((function(signInResult)
    {
        if (!signInResult)
        {
            var errorReason =
                ($connectionUtils.isConnectedToInternet()) ? (this.OAUTH_LOGIN_FAILURE) : (this.COMMUNICATION_FAILURE);
            finishedSignInCallback(errorReason);
        }
        else
        {
            console.log('Calling signIn endpoint...');
            gapi.client.studentendpoint.signIn().execute((function(response)
            {
                if (($studate.didAPISucceed(response)) && (typeof response.emailAddress != 'undefined'))
                {
                    finishedSignInCallback(response);
                }
                else if ($studate.didHadCommunicationFailure(response))
                {
                    finishedSignInCallback(this.COMMUNICATION_FAILURE);
                }
                else if ($studate.didHasAuthenticationError(response))
                {
                    finishedSignInCallback(this.AUTHENTICATION_FAILURE);
                }
                else
                {
                    finishedSignInCallback(this.NO_STUDATE_USER_FAILURE);
                }
            }).bind(this));
        }
    }).bind(this));
};

Users.prototype.signOut = function(finishedCallback)
{
    $oauthLogin.signOut(finishedCallback);
};

Users.prototype.getUserInfo = function(gotUserInfoCallback)
{
    $oauthLogin.getUserInfo(gotUserInfoCallback);
};

Users.prototype.register = function(fullName, age, faculty, gender, genderPreferences, profilePictureBase64, finishedRegisteredCallback)
{
    var profilePictureBinaryString = atob(profilePictureBase64);

    var parameters = [
        {name: 'name', value: fullName},
        {name: 'age', value: age},
        {name: 'faculty', value: faculty},
        {name: 'gender', value: gender},
        {name: 'genderPreferences', value: genderPreferences},
        {name: 'profilePicture', value: profilePictureBinaryString, isBinary: true, mimeType: 'image/jpeg'}
    ];

    this.sendUploadPost('registerUser', parameters, (function(isSuccess, response)
    {
        try
        {
            var profilePictureSize = parseInt(response);
            if (profilePictureSize == 0)
            {
                // Note - This is a special handling for Android 4.2 and below
                // (There is a bug that they send empty Blobs instead of their content...)
                // (For more info see: "https://ghinda.net/article/jpeg-blob-ajax-android/")
                return this.sendBufferUploadPost(
                    'editProfilePicture',
                    profilePictureBinaryString,
                    'profilePicture',
                    'image/jpeg',
                    finishedRegisteredCallback);
            }
        }
        catch (e)
        {
            isSuccess = false;  // Something went wrong...
        }
        finishedRegisteredCallback(isSuccess);
    }).bind(this));
};

Users.prototype.unregister = function(finishedUnregisterCallback)
{
    gapi.client.studentendpoint.unregister().execute(function(response)
    {
        var didRegisterSucceed = $studate.didAPISucceed(response);
        finishedUnregisterCallback(didRegisterSucceed);
    });
};

/**
 * Note - All parameters are required besides profilePicture which is optional!
 * @param finishedEditProfileCallback
 * @param gender
 * @param genderPreference
 * @param faculty
 * @param isInvisible
 * @param profilePictureBase64 (Optional) - It is very important to not pass this if not needed!
 */
Users.prototype.editProfile = function(finishedEditProfileCallback, age, gender, genderPreference, faculty, isInvisible, profilePictureBase64)
{
    var editProfileParams = {
        gender: gender,
        genderPreference : genderPreference,
        faculty: faculty,
        age: age,
        isInvisible: isInvisible ? (true) : (false)     // Note - This is done because sometimes isInvisible is undefined because it wasn't defined to all users (The feature was added later)
    };
    gapi.client.studentendpoint.editProfile(editProfileParams).execute((function(response)
    {
        if ($studate.didAPISucceed(response))
        {
            if (profilePictureBase64)
            {
                this.sendBufferUploadPost(
                    'editProfilePicture',
                    atob(profilePictureBase64),
                    'profilePicture',
                    'image/jpeg',
                    finishedEditProfileCallback);
            }
            else
            {
                finishedEditProfileCallback(true);
            }
        }
        else
        {
            finishedEditProfileCallback(false);
        }
    }).bind(this));
};

Users.prototype.sendUploadPost = function(uploadPage, parameters, finishedUploadCallback)
{
    var formData = this.getFormData(parameters);

    var getUploadURLParams = {
        uploadPage: uploadPage
    };
    gapi.client.studentendpoint.getUploadURL(getUploadURLParams).execute((function(response)
    {
        if ($studate.didAPISucceed(response))
        {
            var uploadURL = response.response;
            console.log('uploadURL: ' + uploadURL);

            formData.append('uploadURL', uploadURL);
            this.getUploadURLCallback(uploadURL, formData, finishedUploadCallback);
        }
        else
        {
            finishedUploadCallback(false);
        }
    }).bind(this));
};

Users.prototype.getFormData = function(parameters)
{
    var formData = new FormData();
    for (var i = 0 ; i < parameters.length ; ++i)
    {
        var parameter = parameters[i];
        if (parameter.isBinary)
        {
            // TODO - HACK - Note - It seems that the buffer is uploaded weird when using the Blob constructor...
            // TODO - HACK - Note - The solution for now is to send an empty buffer such that the server will return
            // TODO - HACK - Note - to us that he got an empty buffer and therefore we will call editProfile
            // TODO - HACK - Note - which always work correctly...
            console.log('parameter.value: ' + btoa(parameter.value));
            //var parameterBlob = new Blob([parameter.value], {type: parameter.mimeType});
            var parameterBlob = new Blob([''], {type: parameter.mimeType});
            console.log('parameterBlob.size: ' + parameterBlob.size);
            formData.append(parameter.name, parameterBlob);
        }
        else
        {
            formData.append(parameter.name, parameter.value);
        }
    }

    return formData;
};

// Note - requestHeaders is an optional parameter
Users.prototype.getUploadURLCallback = function(uploadURL, formDataOrBuffer, finishedUploadCallback, requestHeaders)
{
    // TODO - Convert below requests to use $http of AngularJS
    var uploadRequest = new XMLHttpRequest();
    uploadRequest.open('POST', uploadURL, true);
    if (requestHeaders)
    {
        for (var i = 0 ; i < requestHeaders.length ; ++i)
        {
            var header = requestHeaders[i];
            uploadRequest.setRequestHeader(header.name, header.value);
        }
    }
    uploadRequest.onload = function(e)
    {
        if (uploadRequest.readyState == 4)
        {
            var uploadResponse = uploadRequest.responseText;
            var isSuccess = (uploadRequest.status == 200);
            finishedUploadCallback(isSuccess, uploadResponse);
        }
    };
    uploadRequest.onerror = function(e)
    {
        finishedUploadCallback(false);
    };
    uploadRequest.send(formDataOrBuffer);
};

Users.prototype.sendBufferUploadPost = function(uploadPage, buffer, fileName, contentType, finishedUploadCallback)
{
    var getUploadURLParams = {
        uploadPage: uploadPage
    };
    gapi.client.studentendpoint.getUploadURL(getUploadURLParams).execute((function(response)
    {
        if ($studate.didAPISucceed(response))
        {
            var uploadURL = response.response;
            console.log('uploadURL: ' + uploadURL);

            var requestHeaders = [
                {name: 'uploadURL', value: uploadURL}
            ];
            this.getUploadURLBinaryStringCallback(uploadURL, buffer, contentType, fileName, finishedUploadCallback, requestHeaders);
        }
        else if ($studate.didHasAuthenticationError(response))
        {
            $oauthLogin.refreshToken((function(isSuccess)
            {
                if (isSuccess)
                {
                    this.sendBufferUploadPost(uploadPage, buffer, fileName, contentType, finishedUploadCallback);
                }
                else
                {
                    finishedCallback(false);
                }
            }).bind(this));
        }
        else
        {
            finishedUploadCallback(false);
        }
    }).bind(this));
};

// TODO - THIS MAY BE USEFUL IN THE FUTURE. IF NOT, DELETE IT
/*Users.prototype.getArrayBufferFromBinaryString = function(binaryString)
{
    var buffer = new ArrayBuffer(binaryString.length * 2);  // 2 bytes for each char
    var bufferView = new Uint16Array(buffer);
    var binaryStringLength = binaryString.length;
    for (var i = 0 ; i < binaryStringLength ; ++i)
    {
        bufferView[i] = binaryString.charCodeAt(i);
    }
    return buffer;
};*/

// SERIOUS HACK - This logic is done in order to send an ArrayBuffer to AppEngine
// SERIOUS HACK - But because AppEngine Blobstore service upload handler have many bugs, this is the only way to do it! WTF!
// (When not done, We get OutOfMemory exception! Holy shit!)
// Implementation is based on: "http://blog.cail.cn/2011/04/post-to-appengine-blobstore-using-ajax-firefox-and-chrome/"
// Note - requestHeaders is an optional parameter
Users.prototype.getUploadURLBinaryStringCallback = function(
    uploadURL, binaryString, bufferContentType, fileName, finishedUploadCallback, requestHeaders)
{
    var boundary = 'AJAX------------------------AJAX';
    var contentType = "multipart/form-data; boundary=" + boundary;
    var postHead = '--' + boundary + '\r\n' +
        'Content-Disposition: form-data; name="' + fileName + '"; filename="blob"\r\n' +
        'Content-Type: ' + bufferContentType + '\r\n\r\n';
    var postTail = '\r\n--' + boundary + '--';

    var dataToSend = postHead + binaryString + postTail;
    if (typeof XMLHttpRequest.prototype.sendAsBinary !== 'function')
    {
        XMLHttpRequest.prototype.sendAsBinary = function(datastr)
        {
            function byteValue(x)
            {
                return (x.charCodeAt(0) & 0xff);
            }
            var ords = Array.prototype.map.call(datastr, byteValue);
            var ui8a = new Uint8Array(ords);
            this.send(ui8a.buffer);
        };
    }

    // TODO - Convert below requests to use $http of AngularJS
    var uploadRequest = new XMLHttpRequest();
    uploadRequest.open('POST', uploadURL, true);

    if (requestHeaders)
    {
        for (var i = 0 ; i < requestHeaders.length ; ++i)
        {
            var header = requestHeaders[i];
            uploadRequest.setRequestHeader(header.name, header.value);
        }
    }
    uploadRequest.setRequestHeader("Content-Type", contentType);

    uploadRequest.onload = function(e)
    {
        if (uploadRequest.readyState == 4)
        {
            var uploadResponse = uploadRequest.responseText;
            var isSuccess = (uploadRequest.status == 200);
            finishedUploadCallback(isSuccess, uploadResponse);
        }
    };
    uploadRequest.onerror = function(e)
    {
        finishedUploadCallback(false);
    };
    uploadRequest.sendAsBinary(dataToSend);
};

return new Users();

});
