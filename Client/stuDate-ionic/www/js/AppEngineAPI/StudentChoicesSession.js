angular.module('studateAppEngine').factory('$studentChoicesSession', function($studate, $ionicLoading, 
                                                                              $cordovaToast, $oauthLogin)
{

function StudentChoicesSession()
{
    this.maxNumProfiles = 10;
    this.nextProfilesCursorKey = 'nextProfilesCursor';
    this.profileEmailsKey = 'profileEmails';
    this.profilesByEmailKey = 'profilesByEmail';
    this.pendingResponsesKey = 'pendingResponses';

    var localCursor = localStorage.getItem(this.nextProfilesCursorKey);
    this.cursor = ((localCursor == 'null') || (localCursor === null) ? (null) : (localCursor));

    var localProfileEmails = localStorage.getItem(this.profileEmailsKey);
    this.profileEmails = ((localProfileEmails == 'null') || (localProfileEmails === null) ? ([]) : (JSON.parse(localProfileEmails)));

    var localProfilesByEmail = localStorage.getItem(this.profilesByEmailKey);
    this.profilesByEmail = ((localProfilesByEmail == 'null') || (localProfilesByEmail === null) ? ({}) : (JSON.parse(localProfilesByEmail)));

    // Note - This is done for safety check to avoid having bugs at clients (It should never occur)
    if (Object.keys(this.profilesByEmail).length != this.profileEmails.length)
    {
        console.log('This should never occur! profileByEmails.length != profileEmails.length');
        this.profileEmails = [];
        this.profilesByEmail = {};
        this.saveProfilesToLocalStore();
    }

    var localPendingResponses = localStorage.getItem(this.pendingResponsesKey);
    this.pendingResponses = ((localPendingResponses == 'null') || (localPendingResponses === null) ? ([]) : (JSON.parse(localPendingResponses))); 

    if (this.pendingResponses.length > 0)
    {
        this.setProfileAnswers(this.pendingResponses, this.maxNumProfiles, false, (function(isSuccess) {
            if (isSuccess)
            {
                this.clearPendingResponses();
            }
        }).bind(this));
    }  

    // TODO - can store current shown card index - not so important
}

StudentChoicesSession.prototype.getMaxNumProfiles = function()
{
    return this.maxNumProfiles;
};

StudentChoicesSession.prototype.getPendingResponses = function()
{
    return this.pendingResponses;
};

StudentChoicesSession.prototype.clearPendingResponses = function()
{
    this.pendingResponses = [];
    localStorage.setItem(this.pendingResponsesKey,JSON.stringify([]));
};

StudentChoicesSession.prototype.addPendingResponses = function(responses)
{
    
    this.pendingResponses = this.pendingResponses.concat(responses);
    localStorage.setItem(this.pendingResponsesKey, JSON.stringify(this.pendingResponses));
};

StudentChoicesSession.prototype.saveProfilesToLocalStore = function()
{
    localStorage.setItem(this.profileEmailsKey, JSON.stringify(this.profileEmails));
    localStorage.setItem(this.profilesByEmailKey, JSON.stringify(this.profilesByEmail));
};

StudentChoicesSession.prototype.getNumberOfAvailableProfiles = function()
{
    return this.profileEmails.length;
};

StudentChoicesSession.prototype.getAvailableProfiles = function()
{
    var profiles = this.profileEmails.map((function(email)
    {
        return this.profilesByEmail[email];
    }).bind(this));

    return profiles;
};

StudentChoicesSession.prototype.resetAvailableProfiles = function() 
{
    this.cursor = null;
    localStorage.setItem(this.nextProfilesCursorKey, null);

    this.profileEmails = [];
    this.profilesByEmail = {};
    this.saveProfilesToLocalStore();
};

// returns the number of new profiles received
StudentChoicesSession.prototype.handleProfilesFromServer = function(response) 
{
    console.log('got back cursor' + response.nextPageToken);
    if (this.cursor == response.nextPageToken)
    {
        this.cursor = null;
        console.log("Reached end of profile list in server - resetting cursor");
    }
    else
    {
        this.cursor = response.nextPageToken;
    }

    localStorage.setItem(this.nextProfilesCursorKey, this.cursor);
    return this.insertNewAvailableProfiles(response.items || []);
};

StudentChoicesSession.prototype.getNextProfiles = function(maxNumberOfProfiles, finishedCallback)
{
    var numberOfProfiles = maxNumberOfProfiles - this.getNumberOfAvailableProfiles();

    var getNextProfilesParams = {
        cursor: this.cursor,
        numberOfProfiles: numberOfProfiles
    };

    console.log('getting next profiles with cursor:' + this.cursor);

    $ionicLoading.show({
        template : '<h4>Retrieving profiles...</h4><i class="icon ion-loading-c icon_test"></i>'
    });

	gapi.client.studentchoiceendpoint.getNextProfiles(getNextProfilesParams).execute(
        (function(response)
        {
            $ionicLoading.hide();
            if ($studate.didAPISucceed(response))
            {
                var numberOfNewProfiles = this.handleProfilesFromServer(response);
                this.showNumberOfProfilesRetrieved(numberOfNewProfiles, true);
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.getNextProfiles(maxNumberOfProfiles, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
               finishedCallback(false);
            }
        }).bind(this)
    );
};

StudentChoicesSession.prototype.setProfileAnswers = function(
    profileAnswers,
    maxNumberOfProfiles,
    shouldReturnOnlyUsersThatLikeMe,
    finishedCallback)
{
    var numberOfProfilesToFetch = maxNumberOfProfiles - this.getNumberOfAvailableProfiles();

	var setProfileAnswersParams = {
        emails: profileAnswers.map(function(profileAnswer) { return profileAnswer.email; }),
        results: profileAnswers.map(function(profileAnswer) { return profileAnswer.result; }),
        cursor: this.cursor,
        numberOfProfiles: numberOfProfilesToFetch,
        shouldReturnOnlyUsersThatLikeMe: shouldReturnOnlyUsersThatLikeMe
    };
    gapi.client.studentchoiceendpoint.setProfileAnswers(setProfileAnswersParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {   
                var numberOfNewProfiles = this.handleProfilesFromServer(response);
                this.showNumberOfProfilesRetrieved(numberOfNewProfiles,false);
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.setProfileAnswers(profileAnswers, maxNumberOfProfiles, shouldReturnOnlyUsersThatLikeMe, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

StudentChoicesSession.prototype.deleteProfilesByEmail = function(emails)
{
    console.log('this.profileEmails: ' + JSON.stringify(this.profileEmails));

    for (var i = 0 ; i < emails.length ; ++i)
    {
        var email = emails[i];
        console.log('Deleting email: ' + email);

        var index = this.profileEmails.indexOf(email);
        this.profileEmails.splice(index, 1);

        delete this.profilesByEmail[email];
    }

    this.saveProfilesToLocalStore();
};

StudentChoicesSession.prototype.showNumberOfProfilesRetrieved = function(numberOfNewProfiles, isShowIfNone)
{
    var msg;
    if (numberOfNewProfiles == 0)
    {
        msg = "No new profiles were retrieved.";
    }
    else if (numberOfNewProfiles == 1)
    {
        msg = numberOfNewProfiles + " new profile was retrieved.";
    }
    else
    {
        msg = numberOfNewProfiles + " new profiles were retrieved.";
    }

    if (isShowIfNone || (numberOfNewProfiles > 0))
    {
        $cordovaToast.show(msg, 'short', 'bottom');
    }
};

// NOTE - expected to be called after retrieving new profiles from server
// returns the number of new profiles received
StudentChoicesSession.prototype.insertNewAvailableProfiles = function(newProfiles)
{
    var numberOfNewProfiles = 0 ;

    // This makes sure that profiles we already know about gets only updated
    // and profiles we don't know about gets added
    for (var i = 0 ; i < newProfiles.length ; ++i)
    {
        var newProfile = newProfiles[i];
        var newEmailAddress = newProfile.emailAddress;
        var doesHaveProfile = this.profilesByEmail[newEmailAddress];
        if (doesHaveProfile == undefined)
        {
            this.profileEmails.unshift(newEmailAddress);
            numberOfNewProfiles++;
        }
        this.profilesByEmail[newEmailAddress] = newProfile;
    }

    this.saveProfilesToLocalStore();
    return numberOfNewProfiles;
};

return new StudentChoicesSession();

});
