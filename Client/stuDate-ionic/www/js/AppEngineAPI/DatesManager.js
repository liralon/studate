
angular.module('studateAppEngine').factory('$datesManager', function($window, $http, $studate, $oauthLogin)
{

function DatesManager()
{
    // Nothing to do
}

DatesManager.prototype.getDateInfo =  function(dateId, finishedCallback)
{
    var getDateInfoParams = {
        dateId: dateId
    };
    gapi.client.studatedateendpoint.getDateInfo(getDateInfoParams).execute((function(response)
    {
        if ($studate.didAPISucceed(response))
        {
            finishedCallback(response);
        }
        else if ($studate.didHasAuthenticationError(response))
        {
            $oauthLogin.refreshToken((function(isSuccess)
            {
                if (isSuccess)
                {
                    this.getDateInfo(dateId, finishedCallback);
                }
                else
                {
                    finishedCallback(false);
                }
            }).bind(this));
        }
        else
        {
            finishedCallback(null);
        }
    }).bind(this));
};

DatesManager.prototype.setDateConfirmationResult = function(dateId, confirmationResult, finishedCallback)
{
    var setDateConfirmationResultParams = {
        dateId: dateId,
        confirmationResult: confirmationResult ? ("CONFIRMED") : ("DECLINED")
    };
    gapi.client.studatedateendpoint.setDateConfirmationResult(setDateConfirmationResultParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.setDateConfirmationResult(dateId, confirmationResult, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

StudateDate.prototype.setWeMet = function(finishedCallback)
{
    var setDateConfirmationResultParams = {
        dateId: this.dateId,
        confirmationResult: 'DURING_DATE'
    };
    gapi.client.studatedateendpoint.setDateConfirmationResult(setDateConfirmationResultParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.setWeMet(finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

DatesManager.prototype.suggestMeetingPlace = function(dateId, meetingPlace, finishedCallback)
{
    var suggestMeetingPlaceParams = {
        dateId: dateId,
        meetingPlace: meetingPlace
    };
    gapi.client.studatedateendpoint.suggestMeetingPlace(suggestMeetingPlaceParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.suggestMeetingPlace(dateId, meetingPlace, textMessage, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

DatesManager.prototype.confirmMeetingPlace = function(dateId, finishedCallback)
{
    var setMeetingPlaceConfirmationResultParams = {
        dateId: dateId
    };
    gapi.client.studatedateendpoint.confirmMeetingPlace(setMeetingPlaceConfirmationResultParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.setMeetingPlaceConfirmationResult(dateId, isConfirmed, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

DatesManager.prototype.respondLocationSuggestion = function(dateId, userResponse, finishedCallback)
{
    var meetingPlaceResponseParams = {
        dateId: dateId,
        userResponse: userResponse
    };
    gapi.client.studatedateendpoint.respondLocationSuggestion(meetingPlaceResponseParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.respondLocationSuggestion(dateId, userResponse, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

DatesManager.prototype.cancelDate = function(dateId, shouldDeleteMatch, finishedCallback)
{
    var cancelDateParams = {
        dateId: dateId,
        shouldDeleteMatch: shouldDeleteMatch
    };
    gapi.client.studatedateendpoint.cancelDate(cancelDateParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.cancelDate(dateId, shouldDeleteMatch, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

DatesManager.prototype.sendChatMessageToDatePeer = function(dateId, chatMessage, index, finishedCallback)
{
    var sendChatMessageToDatePeerParams = {
        dateId: dateId,
        chatMessage: chatMessage
    };
    gapi.client.studatedateendpoint.sendChatMessageToDatePeer(sendChatMessageToDatePeerParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true, index, chatMessage, response.response);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.sendChatMessageToDatePeer(dateId, chatMessage, index, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false, index, chatMessage);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false, index, chatMessage);
            }
        }).bind(this)
    );
};

DatesManager.prototype.getChatMessages = function(dateId, finishedCallback)
{
    var getChatMessagesParams = {
        dateId: dateId
    };
    gapi.client.studatedateendpoint.getChatMessages(getChatMessagesParams).execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(response.items);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.getChatMessages(dateId, finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

return new DatesManager();

});
