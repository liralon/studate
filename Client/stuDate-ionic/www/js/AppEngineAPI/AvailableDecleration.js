
angular.module('studateAppEngine').factory('$availableDecleration', function($window, $http, $studate, $oauthLogin)
{

function AvailableDecleration()
{
    // Nothing to do here
}

AvailableDecleration.prototype.declareAvailable = function(finishedCallback)
{
    gapi.client.avilabledeclerationendpoint.declareAvailable().execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.declareAvailable(finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

AvailableDecleration.prototype.declareNotAvailable = function(finishedCallback)
{
    gapi.client.avilabledeclerationendpoint.declareNotAvailable().execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                finishedCallback(true);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.declareNotAvailable(finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(false);
            }
        }).bind(this)
    );
};

AvailableDecleration.prototype.getAvailableStatus = function(finishedCallback)
{
    gapi.client.avilabledeclerationendpoint.getAvailableStatus().execute(
        (function(response)
        {
            if ($studate.didAPISucceed(response))
            {
                var isAvailable = (typeof response.emailAddress != 'undefined');
                finishedCallback(isAvailable);
            }
            else if ($studate.didHasAuthenticationError(response))
            {
                $oauthLogin.refreshToken((function(isSuccess)
                {
                    if (isSuccess)
                    {
                        this.getAvailableStatus(finishedCallback);
                    }
                    else
                    {
                        finishedCallback(false);
                    }
                }).bind(this));
            }
            else
            {
                finishedCallback(null);
            }
        }).bind(this)
    );
};

return new AvailableDecleration();

});
