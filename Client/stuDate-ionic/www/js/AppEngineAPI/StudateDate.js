
function StudateDate(datesManager, dateId)
{
    this.datesManager = datesManager;
    this.dateId = dateId;
    this.datePeerProfile = null;
}

StudateDate.prototype.getDateInfo = function(finishedCallback)
{
    this.datesManager.getDateInfo(this.dateId, (function(response)
    {
        if (response)
        {
            this.datePeerProfile = response.peerStudent;
            this.meetingPlace = response.meetingPlace;
        }

        finishedCallback(response);
    }).bind(this));
};

StudateDate.prototype.confirm = function(finishedCallback)
{
    this.datesManager.setDateConfirmationResult(this.dateId, true, finishedCallback);
};

StudateDate.prototype.decline = function(finishedCallback)
{
    this.datesManager.setDateConfirmationResult(this.dateId, false, finishedCallback);
};

StudateDate.prototype.suggestMeetingPlace = function(meetingPlace, finishedCallback)
{
    this.datesManager.suggestMeetingPlace(this.dateId, meetingPlace, finishedCallback);
};

StudateDate.prototype.confirmMeetingPlace = function(finishedCallback)
{
    this.datesManager.confirmMeetingPlace(this.dateId, finishedCallback);
};

StudateDate.prototype.setWeMet = function(finishedCallback)
{
    this.datesManager.setWeMet(this.dateId, finishedCallback);
};

StudateDate.prototype.cancelDate = function(shouldDeleteMatch, finishedCallback)
{
    this.datesManager.cancelDate(this.dateId, shouldDeleteMatch, finishedCallback);
};

StudateDate.prototype.sendChatMessageToDatePeer = function(chatMessage, index, finishedCallback)
{
    this.datesManager.sendChatMessageToDatePeer(this.dateId, chatMessage, index, finishedCallback);
};

StudateDate.prototype.getChatMessages = function(finishedCallback)
{
    this.datesManager.getChatMessages(this.dateId, finishedCallback);
};
