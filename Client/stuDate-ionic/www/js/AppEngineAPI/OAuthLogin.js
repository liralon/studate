
angular.module('studateAppEngine').factory('$oauthLogin', function($window, $http, $studate)
{

function MobileGoogleLogin()
{
    this.fakeRedirectUrl = 'http://localhost/callback';
    this.authUrl = this.getAuthUrl();
}

MobileGoogleLogin.prototype.getAuthUrl = function()
{
    var authUrl = 'https://accounts.google.com/o/oauth2/auth?' +
	    'client_id=' + $studate.clientId + '&' +
	    'redirect_uri=' + this.fakeRedirectUrl + '&' +
	    'scope=' + $studate.scopes + '&' +
	    'response_type=code' + '&' +
	    'access_type=offline' + '&';

    // Note - staff test accounts are directed to a regular google sign in
    if (!Config.staffAccount)
    {
        authUrl += ('hd=' + $studate.googleAppDomain);
    }

    return authUrl;
};

MobileGoogleLogin.prototype.signIn = function(finishedSignInCallback)
{
    // Check if already signed-in
    var currentToken = gapi.auth.getToken();
    if (currentToken != null)
    {
        console.log('Detected that user is already signed-in!');
        setTimeout(function() { finishedSignInCallback(true); }, 0);
        return;
    }

    this.trySignInFromLocalStorage((function(isSuccess)
    {
        if (isSuccess)
        {
            finishedSignInCallback(true);
        }
        else
        {
            this.finishedSignInCallback = finishedSignInCallback;
            this.authWindow = this.openAuthWindow(this.authUrl, false);
        }
    }).bind(this));
};

MobileGoogleLogin.prototype.refreshToken = function(finishedCallback)
{
    this.trySignInFromLocalStorage(finishedCallback);
};

MobileGoogleLogin.prototype.openAuthWindow = function(authUrl, isWindowHidden)
{
    if (isWindowHidden)
    {
        console.log('Opening a hidden login browser...');
        var authWindow = $window.open(authUrl, '_blank', 'hidden=yes,clearsessioncache=yes');
    }
    else
    {
        var authWindow = $window.open(authUrl, '_blank', 'location=no,toolbar=no,clearsessioncache=yes');
    }

    // Note - This must be saved in members so that we can later remove them
    this.fakeRedirectLoadCallbackBound = this.fakeRedirectLoadCallback.bind(this);
    this.loadPageErrorBound = this.loadPageError.bind(this);
    this.exitBrowserHandlerBound = this.exitBrowserHandler.bind(this);
    authWindow.addEventListener('loadstart', this.fakeRedirectLoadCallbackBound);
    authWindow.addEventListener('loaderror',this.loadPageErrorBound);
    authWindow.addEventListener('exit', this.exitBrowserHandlerBound);

    return authWindow;
};

MobileGoogleLogin.prototype.closeAuthWindowWithoutExitHandler = function(authWindow)
{
    authWindow.removeEventListener('loadstart', this.fakeRedirectLoadCallbackBound);
    authWindow.removeEventListener('loaderror', this.loadPageErrorBound);
    authWindow.removeEventListener('exit', this.exitBrowserHandlerBound);
    authWindow.close();
};

MobileGoogleLogin.prototype.loadPageError = function(event)
{
    // Our fake page doesn't count as a real error. It is just a trick we use to steal the access token from the url
    var url = event.url;
    if (url.indexOf(this.fakeRedirectUrl) != 0)
    {
        console.log('Failed to load page in InAppBrowser (event: ' + JSON.stringify(event) + ')');
        this.closeAuthWindowWithoutExitHandler(this.authWindow);
        this.finishedSignInCallback(false);
    }
};

MobileGoogleLogin.prototype.exitBrowserHandler = function(event)
{
    console.log('Exit browser handler called');
    this.finishedSignInCallback(false);
};

MobileGoogleLogin.prototype.fakeRedirectLoadCallback = function(event)
{
    var url = event.url;
    if (url.indexOf(this.fakeRedirectUrl) == 0)
    {
        console.log('redirectLoadCallback called with fake URL!');

        var requestToken = this.getRequestTokenFromURL(url);
        if (!requestToken)
        {
            console.log('Failed to get request token from URL!');
            setTimeout((function() { this.finishedSignInCallback(false); }).bind(this), 0);
            this.closeAuthWindowWithoutExitHandler(this.authWindow);
            return;
        }

        var getAccessTokenData = this.getRequestAccessTokenData(requestToken);

        // Note - This is a hack in order to workaround a bug of OAuth2 that returns both success and failure even though
        // Note - we haven't failed...
        this.didAjaxSucceed = false;
        this.TIME_TO_WAIT_FOR_AJAX_SUCCESS_IN_MILLISECONDS = 2000;

        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        $http({
            method: 'POST',
            url: 'https://accounts.google.com/o/oauth2/token',
            data: getAccessTokenData
        })
        .success((function(data)
        {
            console.log('Got TAU token successfully!');
            this.didAjaxSucceed = true;
            this.token = data;
            this.setToken(this.token);
            this.saveTokenInfoToLocalStorage(this.token);
            this.finishedSignInCallback(true);
        }).bind(this))
        .error((function(data)
        {
            console.log('GetAccessToken failed: ' + JSON.stringify(data));
            // Note - This is done because we want to wait for a while that maybe OAuth2 may change his mind about success
            setTimeout((function()
            {
                if (!this.didAjaxSucceed)
                {
                    this.finishedSignInCallback(false);
                }
            }).bind(this), this.TIME_TO_WAIT_FOR_AJAX_SUCCESS_IN_MILLISECONDS);
        }).bind(this));

        this.closeAuthWindowWithoutExitHandler(this.authWindow);
    }
};

MobileGoogleLogin.prototype.getRequestTokenFromURL = function(url)
{
    if (url.indexOf('code=') == (-1))
    {
        return null;
    }
    else
    {
        return (url.split('code=')[1]);
    }
};

MobileGoogleLogin.prototype.getRequestAccessTokenData = function(requestToken)
{
    var getAccessTokenUrl =
		'client_id=' + $studate.clientId + '&' +
		'client_secret=' + $studate.clientSecret + '&' +
		'redirect_uri=' + this.fakeRedirectUrl + '&' +
		'grant_type=authorization_code' + '&' +
		'code=' + requestToken;
    return getAccessTokenUrl;
};

MobileGoogleLogin.prototype.getRequestRefreshTokenData = function(refreshToken)
{
    var getRefreshTokenUrl =
        'refresh_token=' + refreshToken + '&' +
		'client_id=' + $studate.clientId + '&' +
		'client_secret=' + $studate.clientSecret + '&' +
		'grant_type=refresh_token';
    return getRefreshTokenUrl;
};

MobileGoogleLogin.prototype.setToken = function(token)
{
    $window.gapi.auth.setToken(token);
    var acceptedToken = gapi.auth.getToken();
    console.log('acceptedToken: ' + JSON.stringify(acceptedToken));

    $http.defaults.headers.common.Authorization = token.token_type + ' ' + token.access_token;
};

MobileGoogleLogin.prototype.unsetToken = function(finishedCallback)
{
    $window.gapi.auth.setToken(null);
    this.deleteTokenInfoFromLocalStorage();

    $window.cookies.clear(finishedCallback);
};

MobileGoogleLogin.prototype.saveTokenInfoToLocalStorage = function(token)
{
    $window.localStorage.token_object = JSON.stringify(token);

    $window.localStorage.access_token = token.access_token;

    // We need to get a refresh token only once. Once we have it, we don't need new ones
    if (($window.localStorage.getItem('refresh_token') == null) && (token.refresh_token))
    {
        $window.localStorage.refresh_token = token.refresh_token;
    }

    var expires_at = new Date().getTime() + parseInt(token.expires_in, 10) * 1000 - 60000;
    $window.localStorage.expires_at = expires_at;
};

MobileGoogleLogin.prototype.deleteTokenInfoFromLocalStorage = function()
{
    console.log('deleteTokenInfoFromLocalStorage');
    $window.localStorage.removeItem('token_object');
    $window.localStorage.removeItem('access_token');
    $window.localStorage.removeItem('refresh_token');
    $window.localStorage.removeItem('expires_at');
};

MobileGoogleLogin.prototype.didLoginInPast = function()
{
    return ($window.localStorage.getItem('refresh_token') != null)
};

MobileGoogleLogin.prototype.trySignInFromLocalStorage = function(finishedCallback)
{
    if (!this.didLoginInPast())
    {
        console.log('Access token not found on local storage');
        return finishedCallback(false);
    }

    var currentTime = new Date().getTime();
    console.log('Access token expires at: ' + $window.localStorage.expires_at);
    if (currentTime < $window.localStorage.expires_at)
    {
        console.log('access token has not yet expired!');
        this.token = JSON.parse($window.localStorage.token_object);
        this.setToken(this.token);
        return finishedCallback(true);
    }
    else if ($window.localStorage.getItem('refresh_token') == null)
    {
        console.log('access token has expired but dont have refresh token from some reason (Should not happen)');
        return finishedCallback(false);
    }
    else
    {
        console.log('access token has expired. try to refresh it!');
        console.log('refresh token: ' + $window.localStorage.refresh_token);

        var getRefreshTokenData = this.getRequestRefreshTokenData($window.localStorage.refresh_token);

        // Note - This is a hack in order to workaround a bug of OAuth2 that returns both success and failure even though
        // Note - we haven't failed...
        this.didAjaxSucceed = false;
        this.TIME_TO_WAIT_FOR_AJAX_SUCCESS_IN_MILLISECONDS = 2000;

        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        $http({
            method: 'POST',
            url: 'https://accounts.google.com/o/oauth2/token',
            data: getRefreshTokenData
        })
        .success((function(data)
        {
            console.log('Got TAU token successfully!');
            this.didAjaxSucceed = true;
            this.token = data;
            this.setToken(this.token);
            this.saveTokenInfoToLocalStorage(this.token);
            finishedCallback(true);
        }).bind(this))
        .error((function(data)
        {
            console.log('Got error from refreshing token! ' + JSON.stringify(data));
            // Note - This is done because we want to wait for a while that maybe OAuth2 may change his mind about success
            setTimeout((function()
            {
                if (!this.didAjaxSucceed)
                {
                    finishedCallback(false);
                }
            }).bind(this), this.TIME_TO_WAIT_FOR_AJAX_SUCCESS_IN_MILLISECONDS);
        }).bind(this));
    }
};

function PCOAuthLogin()
{
    // Nothing to do here
}

// TODO - Note - Because of a bug in gapi.auth.authorize of not calling the callback in case we
// TODO - Note - need to popup to user to sign in, this method needs to be called twice
// TODO - Note - (The second time only after user confirmed he finished signing-in google)
// TODO - Note - in order to promise that the user has indeed sign in. Sorry... :\
PCOAuthLogin.prototype.signIn = function(signInResultCallback)
{
    this.trySignInWithCurrentOAuth2Token((function(didSignInSucceeded)
    {
        if (didSignInSucceeded)
        {
            signInResultCallback(true);
        }
        else
        {

            this.popupUserToSignInToGetOAuth2Token((function(didSignInSucceeded)
            {
                if (didSignInSucceeded)
                {
                    signInResultCallback(true);
                }
                else
                {
                    this.trySignInWithCurrentOAuth2Token(signInResultCallback);
                }
            }).bind(this));
        }
    }).bind(this));
};

PCOAuthLogin.prototype.trySignInWithCurrentOAuth2Token = function(signInResultCallback)
{
    var authParams = this.getAuthParams();
    authParams.immediate = true;
    gapi.auth.authorize(authParams, this.authorizeResultCallback.bind(this, signInResultCallback));
};
PCOAuthLogin.prototype.trySignIn = PCOAuthLogin.prototype.trySignInWithCurrentOAuth2Token;

PCOAuthLogin.prototype.popupUserToSignInToGetOAuth2Token = function(signInResultCallback)
{
    var authParams = this.getAuthParams();
    authParams.immediate = false;
    gapi.auth.authorize(authParams, this.authorizeResultCallback.bind(this, signInResultCallback));
};

PCOAuthLogin.prototype.getAuthParams = function()
{
    var authParams = {
        client_id: $studate.clientId,
        scope: $studate.scopes,
        response_type: 'token'
    };
    // Note - staff test accounts are directed to a regular google sign in
    if (!Config.staffAccount)
    {
        authParams.hd = $studate.googleAppDomain;
    }
    return authParams;
};

PCOAuthLogin.prototype.authorizeResultCallback = function(signInResultCallback, authResult)
{
    signInResultCallback(this.didSignInSucceeded(authResult));
};

PCOAuthLogin.prototype.didSignInSucceeded = function(authResult)
{
    return (authResult && !authResult.error);
};

PCOAuthLogin.prototype.setToken = function(token)
{
    gapi.auth.setToken(token);
};

PCOAuthLogin.prototype.unsetToken = function()
{
    gapi.auth.setToken(null);
};

PCOAuthLogin.prototype.refreshToken = function(finishedCallback)
{
    // Nothing to do here (Not implemented refresh logic for PC because it is unnecessary for debugging)
};

PCOAuthLogin.prototype.didLoginInPast = function()
{
    // Note - In PC we can always assume we are already logged-in...
    return true;
};

// Implementation of common methods

function signOut(finishedCallback)
{
    var accessToken = gapi.auth.getToken().access_token;

    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'https://accounts.google.com/o/oauth2/revoke?token=' + encodeURIComponent(accessToken), false);
    httpRequest.setRequestHeader("Content-Type", 'application/json');
    httpRequest.send();

    this.unsetToken(finishedCallback);
}
MobileGoogleLogin.prototype.signOut = signOut;
PCOAuthLogin.prototype.signOut = signOut;

// Note - gotUserInfoCallback receives as a parmeter an object with the following properties:
// (*) id
// (*) family_name
// (*) given_name
// (*) hd (For example: mail.tau.ac.il)
// (*) email
// (*) name (Full name. Includes both family_name and given_name)
// (*) locale
// (*) picture (URL to google profile picture)
// (*) verified_email
// Note - If the function fails, the returned object will have a "code" property which indicates Error Code
function getUserInfo(gotUserInfoCallback)
{
    gapi.client.oauth2.userinfo.get().execute(gotUserInfoCallback);
}
MobileGoogleLogin.prototype.getUserInfo = getUserInfo;
PCOAuthLogin.prototype.getUserInfo = getUserInfo;

return (Config.runningFromBrowser) ? (new PCOAuthLogin()) : (new MobileGoogleLogin());

});
