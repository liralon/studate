
angular.module('studateAppEngine', []).factory('$studate', function($window, $connectionUtils)
{

function Studate()
{
    this.TIME_TO_WAIT_FOR_APP_ENGINE_TO_INIT_INTEVALS_IN_MILLISECONDS = 100;

    this.projectId = '932869752253';

	this.studateURL = Config.studateURL;
	this.studateAPIURL = this.studateURL + '_ah/api';
    this.googleAppDomain = 'mail.tau.ac.il';

    // Note - APIKey is only necessary if we want some of the APIs to be accessed without any autherization
    this.APIKey = 'AIzaSyDbw4va3hAem7A_HoPDXgdt-BzgWIFvpyU';

    this.initClientInfo();

    // Note - Specifies that we want user permissions to being able to read profile & email address of user
    this.scopes = 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';

    this.isAPILoaded = false;
}

Studate.prototype.initClientInfo = function()
{
    this.clientId = '932869752253-953sqcg86o9o000d179okrck4lm63j84.apps.googleusercontent.com';
    this.clientSecret = 't2VhcvodnYsCFSZOpcuNXM29';
};

Studate.prototype.waitForAPIToLoad = function(finishedCallback)
{
    if (this.isAPILoaded)
    {
        finishedCallback(true);
        return;
    }

    if (!$connectionUtils.isConnectedToInternet())
    {
        finishedCallback(false);
        return;
    }

    if ($window.AppEngineAPILoaded)
    {
        this.getGoogleAPIClient().setApiKey(this.APIKey);
        var apisToLoad = [];
        apisToLoad.push( {name: 'oauth2', version: 'v2', useGoogleAPI: true});
        apisToLoad.push( {name: 'studentendpoint', version: 'v1', useGoogleAPI: false});
        apisToLoad.push( {name: 'studentchoiceendpoint', version: 'v1', useGoogleAPI: false});
        apisToLoad.push( {name: 'avilabledeclerationendpoint', version: 'v1', useGoogleAPI: false});
        apisToLoad.push( {name: 'studatedateendpoint', version: 'v1', useGoogleAPI: false});

        this.loadAllAPIs(apisToLoad, (function(isSuccess)
        {
            console.log('Finished loading AppEngine APIs result: ' + isSuccess);
            this.isAPILoaded = isSuccess;
            finishedCallback(isSuccess);
        }).bind(this));
    }
    else
    {
        setTimeout(
            this.waitForAPIToLoad.bind(this, finishedCallback),
            this.TIME_TO_WAIT_FOR_APP_ENGINE_TO_INIT_INTEVALS_IN_MILLISECONDS);
    }
};

Studate.prototype.loadAllAPIs = function(apisToLoad, finishedCallback)
{
    if (apisToLoad.length == 0)
    {
        // Note - It may be possible that we disconnected while loading the last API...
        finishedCallback($connectionUtils.isConnectedToInternet());
    }
    else if (!$connectionUtils.isConnectedToInternet())
    {
        finishedCallback(false);
    }
    else
    {
        var apiToLoad = apisToLoad.pop();
        var loadAPIFunction = (apiToLoad.useGoogleAPI) ? (this.loadGoogleAPI.bind(this)) : (this.loadAPI.bind(this));
        loadAPIFunction(apiToLoad.name, apiToLoad.version, (function()
        {
            this.loadAllAPIs(apisToLoad, finishedCallback);
        }).bind(this));
    }
};

Studate.prototype.getGoogleAPIClient = function()
{
    return $window.gapi.client;
};

Studate.prototype.loadAPI = function(apiName, apiVersion, finishedLoadingCallback)
{
	this.getGoogleAPIClient().load(apiName, apiVersion, finishedLoadingCallback, this.studateAPIURL);
};

Studate.prototype.loadGoogleAPI = function(apiName, apiVersion, finishedLoadingCallback)
{
    this.getGoogleAPIClient().load(apiName, apiVersion, finishedLoadingCallback);
};

Studate.prototype.didAPISucceed = function(response)
{
    return (!response.code);
};

Studate.prototype.didHasAuthenticationError = function(response)
{
    return (response.code == 401);
};

Studate.prototype.didHadCommunicationFailure = function(response)
{
    return ((typeof response.message == 'string') && (response.message.indexOf('A network error occurred') != (-1)));
};

Studate.prototype.convertDateToAppEngineParam = function(date)
{
    return JSON.stringify(date).split('"')[1];
};

return new Studate();

});

// Note - This is the function that should be passed as parameter to the gclient.js loading init function
function InitAppEngineAPI()
{
    // TODO - The GUI currently don't use it. I can't make this to work because I don't understand AngularJS enough
    // TODO - We should somehow use this with ng-show attribute of an HTML element
    // Note - This is used to now show the App GUI before Google AppEngine API is loaded
    window.AppEngineAPILoaded = true;
}
