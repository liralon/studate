// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

angular.module('starter', ['ionic', 'starter.controllers', 'PushNotificationModule', 'ConnectionUtilsModule',
                            'CameraModule', 'HardwareBackButtonManagerModule', 'StudateActionSheets', 'ngPlacesAutocomplete'])

  .directive('croppedImage', function () {
      return {
          restrict: "E",
          replace: true,
          template: "<div class='center-cropped'></div>",
          link: function(scope, element, attrs) {
              scope.$watch('imagesrc', function () {


              var width = Math.floor(attrs.heightpercentage * screen.height / 100);
              var height = width;
              var backgroundImageSrc = "data:image/jpeg;base64," + scope.$eval(attrs.imagesrc);

              var image = new Image();
              image.src = backgroundImageSrc;
              image.onload = function () {
                var newImageHeight;
                var newImageWidth;
                if (image.width < image.height)
                {
                  newImageWidth = (width);
                  newImageHeight = Math.floor(image.height * width / image.width);
                }
                else
                {
                  newImageHeight = (height);
                  newImageWidth = Math.floor(image.width * height / image.height);
                }

                element.css('width', width + "px");
                element.css('height', height + "px");
                element.css('marginTop', '5px');
                element.css('backgroundPosition', 'center center');
                element.css('backgroundRepeat', 'no-repeat');
                element.css('backgroundImage', "url('" + backgroundImageSrc + "')");
                element.css('backgroundSize', newImageWidth + 'px ' + newImageHeight + 'px' );
                element.css('border-radius', '9999px');
                element.css('-webkit-border-radius', '9999px');
                element.css('border-top-left-radius', '9999px');
                element.css('border-top-right-radius', '9999px');
                element.css('border-bottom-left-radius', '9999px');
                element.css('border-bottom-right-radius', '9999px');
                element.css('-webkit-border-top-left-radius', '9999px');
                element.css('-webkit-border-top-right-radius', '9999px');
                element.css('-webkit-border-bottom-left-radius', '9999px');
                element.css('-webkit-border-bottom-right-radius', '9999px');
                element.css('box-shadow', '0 0 8px rgba(0, 0, 0, .8)');
              };
            });
          }
      };
  })

.run(function($ionicPlatform, $cordovaToast) {
    $ionicPlatform.ready(function() {
        //Turn on keyboard accessorybar in order to enable "done" button in selects
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        }
        if(window.StatusBar) {
            // Set the statusbar to use the default style, tweak this to
            // remove the status bar on iOS or change it to use white instead of dark colors.
            StatusBar.styleDefault();
        }

        // In case we are running from browser (debugging), define another function for showing toast messages
        if (Config.runningFromBrowser)
        {
            $cordovaToast.show = function(msg) { console.log('Toast message: ' + msg); };
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // Each page has its own nav history stack:
    //////////////
    //New code
    //////////////

  .state('init', {
    url: "/init?parameters",
    views: {
      '': {
        templateUrl: "templates/initView.html",
        controller: 'initCtrl'
      }
    }
  })

  .state('introduction', {
    url: "/introduction",
    views: {
      '': {
        templateUrl: "templates/tutorialView.html",
        controller: 'introductionCtrl'
      }
    }
  })

  .state('tauAuthenticaionPage', {
      url: '/tauAuthenticaion',
      views: {
          '': {
              templateUrl: 'templates/tauAuthenticaionView.html',
              controller: 'initCtrl'
          }
      }
  })


  .state('signUp', {
      url: '/signUp',
      views: {
          '': {
              templateUrl: 'templates/signUpView.html',
              controller: 'signUpCtrl'
          }
      }
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.findPeoplePage', {
    url: "/findPeople",
    views: {
      '': {
        templateUrl: "templates/findPeopleView.html",
        controller: 'FindPeopleCtrl'
      }
    }
  })

  .state('app.findPeopleIntroPage', {
    url: "/findPeopleIntro",
    views: {
      '': {
        templateUrl: "templates/findPeopleIntroView.html",
        controller: 'FindPeopleIntroCtrl'
      }
    }
  })

  .state('editProfilePage', {
      url: '/editProfile',
      views: {
          '': {
              templateUrl: 'templates/editProfileView.html',
              controller: 'editProfileCtrl'
          }
      }
  })

  .state('preferencesPage', {
      url: '/preferences',
      views: {
          '': {
              templateUrl: 'templates/preferencesView.html',
              controller: 'preferencesCtrl'
          }
      }
  })

  .state('meetingPlacePage', {
      url: '/meetingPlace?parameters',
      views: {
          '': {
              templateUrl: 'templates/meetingPlaceView.html',
              controller: 'meetingPlaceCtrl'
          }
      }
  })

  .state('actualDatePage', {
      url: '/actualDate',
      views: {
          '': {
              templateUrl: 'templates/actualDateView.html',
              controller: 'actualDateCtrl'
          }
      }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/init');

});
