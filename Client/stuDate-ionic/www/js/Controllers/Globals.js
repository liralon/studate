
window.dateStates = {
    NOT_IN_DATE: 'NOT_IN_DATE',
    DATE_CONFIRMATION: 'DATE_CONFIRMATION',
    DATE_SUGGESTION_CONFIRMED: 'DATE_SUGGESTION_CONFIRMED',
	MEETING_PLACE_SETTER: 'MEETING_PLACE_SETTER',
	MEETING_PLACE_WAITER: 'MEETING_PLACE_WAITER',
    MEETING_PLACE_CONFIRMED: 'MEETING_PLACE_CONFIRMED'
};

window.PushMessageTypes = {
    DATE_ALERT: 'DATE_ALERT',
    DATE_RESULT: 'DATE_RESULT',
	MEETING_PLACE_SUGGESTED: 'MEETING_PLACE_SUGGESTED',
	MEETING_PLACE_CONFIRMED: 'MEETING_PLACE_CONFIRMED',
    DATE_CANCELED: 'DATE_CANCELED',
    DATE_CHAT_MSG: 'DATE_CHAT_MSG',
    BROADCAST_MESSAGE: 'BROADCAST_MESSAGE'
};

window.Globals = {};
window.Globals.userProfile = null;
window.Globals.userAvailable = false;

window.Globals.faculties = [
    'Arts',
    'Engineering',
    'Exact Sciences',
    'Humanities',
    'Law',
    'Life Sciences',
    'Management',
    'Medicine',
    'Social Sciences'
];

window.Globals.yesNoOptions = ['YES', 'NO'];
window.Globals.genders = ['FEMALE', 'MALE'];
window.Globals.genderOptions = ['ALL', 'FEMALE', 'MALE'];

window.Globals.meetingPlaces = [
    'Aroma',
    'Gilman Grass',
    'Hahoog Hatzfoni',
    'King George',
	'McDonalds',
    'Other'
];
window.undefinedMeetingPlace = 'UNDEFINED';

// Used to pass data between controllers
window.Globals.currentDate = null;

window.Globals.dateAlertHandler = null;
window.Globals.dateResultHandler = null;
window.Globals.meetingPlaceSuggestedHandler = null;
window.Globals.meetingPlaceConfirmedHandler = null;
window.Globals.dateChatMessageHandler = null;

window.Globals.registerPushNotificationHandlers = null;

window.setStudentNotInDate = function()
{
    window.Globals.currentDate = null;
    window.Globals.userProfile.dateState = window.dateStates.NOT_IN_DATE;
};
