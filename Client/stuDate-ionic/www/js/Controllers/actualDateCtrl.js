
angular.module('starter.controllers').controller('actualDateCtrl', function($scope, $state, $stateParams, $window,
                                                                            $ionicPopup, $ionicViewService, $cordovaToast,
                                                                            $studateActionSheets,$ionicScrollDelegate) {

    $scope.date = {name: '', gender: '', faculty: '', profilePicture: '', meetingPlace: ''};

    $scope.chatParams = {
        lastMessage : "",
        fetchingPrevMsgs : false
    };

    $scope.userPic = $window.Globals.userProfile.profilePicture;

    $scope.messages = [];

    $scope.initPage = function()
    {
        console.log('actualDateCtrl.initPage');

        $ionicViewService.nextViewOptions({
            disableAnimate: true,
            disableBack: true
        });

        // Initialize date properties
        $scope.date.name = $window.Globals.currentDate.datePeerProfile.name;
        $scope.date.firstName = $scope.date.name.split(" ")[0];
        $scope.date.gender = $window.Globals.currentDate.datePeerProfile.gender;
        $scope.date.faculty = $window.Globals.currentDate.datePeerProfile.faculty;
        $scope.date.profilePicture = "data:image/jpeg;base64," + $window.Globals.currentDate.datePeerProfile.profilePicture;
        $scope.date.meetingPlace = $window.Globals.currentDate.meetingPlace;

        if ((typeof(cordova) != 'undefined') && (cordova.plugins.Keyboard))
        {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }

        $scope.parsePrevMsgs();
    };

    $scope.parsePrevMsgs = function() {
        $scope.chatParams.fetchingPrevMsgs = true;
        $window.Globals.currentDate.getChatMessages(function(chatMessages)
        {
            if (chatMessages)
            {
                var userEmail = $window.Globals.userProfile.emailAddress;
                for (var i = 0; i < chatMessages.length; ++i)
                {
                    var chatMessage = chatMessages[i];
                    if (chatMessage.emailAddress == userEmail)
                    {
                        $scope.addChatMessage("Me", chatMessage.message);
                    }
                    else
                    {
                        $scope.addChatMessage($scope.date.name, chatMessage.message);
                    }
                }
            }
            else
            {
                // Note - We deliberately fail silently becuase the chat is still funcitonal
                console.log('Failed to get chat messages on startup.');
            }
            
            $scope.chatParams.fetchingPrevMsgs = false;
            $scope.$apply();
            $ionicScrollDelegate.scrollBottom(true);
        });
    };

    $scope.addChatMessage = function(userName, message) {
        var messageStruct = {
            userName: userName,
            text: message
        };
        $scope.messages.push(messageStruct);
        $ionicScrollDelegate.scrollBottom(true);
    };

    $scope.sendChatMessage = function() {
        console.log('send message ' + $scope.chatParams.lastMessage);
        var curMessage = $scope.chatParams.lastMessage;
        var curIndex = $scope.messages.length;

        $scope.addChatMessage("Me", curMessage);
        $scope.chatParams.lastMessage = "";     

        $window.Globals.currentDate.sendChatMessageToDatePeer(curMessage, curIndex, function(isSuccess, index, curMessage, returnValue)
        {
            if (isSuccess)
            {
                console.log('send chat success');
                if (returnValue != 'SUCCESS')
                {
                    var alertPopup = $ionicPopup.confirm({
                        title: "Haven't you met?",
                        template: "Your peer said you have already met. <br> Press OK to return to main screen",
                        okType: 'button-' + Config.appMood
                    });
                    alertPopup.then(function(res)
                    {
                        $scope.sendServerWeHaveMet();
                    });
                }
            }
            else
            {
                console.log('send chat failed');
                $scope.messages.splice(index,1);
                $cordovaToast.show("Failed to communicate with server. Please try again.", "short", "bottom");
                $scope.chatParams.lastMessage = curMessage;
            }

            $scope.$apply();
        });  
    };

    $window.Globals.dateChatMessageHandler = function(message)
    {
        var dateId = JSON.parse(message.dateId);
        if (($window.Globals.currentDate) && ($window.Globals.currentDate.dateId == dateId))
        {
            console.log('Received chat message from date peer: ' + message.chatMessage);
            $scope.addChatMessage($scope.date.name, message.chatMessage);
            $scope.$apply();
        }
        else
        {
            console.log('Probably got duplicate push notification: ' + JSON.stringify(message));
            console.log('I think currentDate is: '  + JSON.stringify($window.Globals.currentDate));
        }
    };

    $scope.confirmMeeting = function()
    {
        var alertPopup = $ionicPopup.confirm({
            title: 'Warning!',
            template: "Are you sure you met your date? <br> " +
                "After meeting confirmation, stuDate won't schedule a date with " +
                $window.Globals.currentDate.datePeerProfile.name + " in the future!",
            okType: 'button-' + Config.appMood
        });

        alertPopup.then(function(res)
        {
            if (res)
            {
                $scope.sendServerWeHaveMet();
            }
            else
            {
                console.log('User canceled meeting confirmation');
            }
        });
    };

    $scope.sendServerWeHaveMet = function()
    {
        $window.Globals.currentDate.setWeMet(function(isSuccess)
        {
            if (isSuccess)
            {
                console.log('User has confirmed the meeting!');
                goToFindPeoplePage();
            }
            else
            {
                $cordovaToast.show(
                    'Failed to communicate with server. Please try again.',
                    'long', 'bottom');
            }
        });
    };

    $scope.getOutOfDate = function()
    {
        var getOutHandler = function(shouldDeleteMatch)
        {
            $window.Globals.currentDate.cancelDate(shouldDeleteMatch,
                function(isSuccess)
                {
                    // Nothing to do here
                    // (We don't have any smart thing to do here in case we fail...)
                });
            $window.setStudentNotInDate();
            goToFindPeoplePage();
        };
        var cancelHandler = function () {};
        $studateActionSheets.showGetOutOfDateSheet(getOutHandler, cancelHandler);
    };

    var goToFindPeoplePage = function() { 
        if ((typeof(cordova) != 'undefined') && (cordova.plugins.Keyboard))
        {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(false);
        }
        console.log('Going to findPeoplePage...');
        $state.go('app.findPeoplePage');
    };

});
