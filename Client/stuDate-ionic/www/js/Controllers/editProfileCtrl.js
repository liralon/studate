
angular.module('starter.controllers').controller('editProfileCtrl', function($scope, $window, $users, $state,
                                                                             $ionicLoading, $ionicPopup, $cameraUtils,
                                                                             $studateActionSheets) {

    $scope.profileDetails = null;
    $scope.newProfilePicture = false;

    $scope.genders = $window.Globals.genders;
    $scope.faculties = $window.Globals.faculties;

    $scope.initializePage = function()
    {
        $scope.userDetails = {};
        $scope.userDetails.gender = $window.Globals.userProfile.gender;
        $scope.userDetails.faculty = $window.Globals.userProfile.faculty;
        $scope.userDetails.age = $window.Globals.userProfile.age;
        $scope.userDetails.genderPreference = $window.Globals.userProfile.genderPreferenceOptions;
        $scope.profilePicture = $window.Globals.userProfile.profilePicture;
    };

    $scope.takePicture = function()
    {
        $cameraUtils.takePicture(function(imageData)
        {
            if (imageData)
            {
                $scope.profilePicture = imageData;
                $scope.newProfilePicture = true;
            }
        });
    };

    $scope.getPictureFromGallery = function()
    {
        $cameraUtils.takeFromGallery(function(imageData)
        {
            if (imageData)
            {
                $scope.profilePicture = imageData;
                $scope.newProfilePicture = true;
            }
        });
    };

    $scope.submitChanges = function()
    {
        $scope.confirmationPopup('Submit changes', 'Are you sure?', function(res)
        {
            if (res)
            {
                $scope.sendChangesToServer();
            }
            else
            {
                console.log('submitChanges: cancelled. display edit screen.');
            }
        });
    };

    $scope.sendChangesToServer = function()
    {
        // Show loading animation while we are doing logic in the background
        $ionicLoading.show({
            template : '<h4>Submitting Changes...</h4><i class="icon ion-loading-c icon_test"></i>'
        });
        console.log('submitChanges: Submitting changes');
        // A new profile picture was taken. Submit details with picture.
        $users.editProfile(
            $scope.serverProfileEditCallback,
            $scope.userDetails.age,
            $scope.userDetails.gender,
            $scope.userDetails.genderPreference,
            $scope.userDetails.faculty,
            $scope.userDetails.isInvisible,
            ($scope.newProfilePicture) ? ($scope.profilePicture) : (undefined));
    };

    $scope.discardChanges = function()
    {
        $scope.confirmationPopup('Discarding changes', 'Changes will be lost. Continue?', function(res) {
            if (res)
            {
                console.log('discardChanges: Discarding. going back to main.');
                $state.go('app.findPeoplePage');
            }
            else
            {
                console.log('discardChanges: cancelled. display edit screen.');
            }
        });
    };

    $scope.confirmationPopup = function(titleText, templateText, actionFunction)
    {
        var alertPopup = $ionicPopup.confirm(
            {
                title: titleText,
                template: templateText,
                okType: 'button-' + Config.appMood
            });
        alertPopup.then(actionFunction);
    };

    $scope.serverProfileEditCallback = function(result)
    {
        $ionicLoading.hide();
        if (result)
        {
            console.log('editProfileCtrl.serverProfileEditCallback(): server changed user details successfully.');

            //Save changes for local session
            $window.Globals.userProfile.gender = $scope.userDetails.gender;
            $window.Globals.userProfile.age = $scope.userDetails.age;
            $window.Globals.userProfile.faculty = $scope.userDetails.faculty;
            $window.Globals.userProfile.profilePicture = $scope.profilePicture;
            $state.go('app.findPeoplePage');
        }
        else
        {
            console.log('editProfileCtrl.serverProfileEditCallback(): server failed. Try again.');
            $scope.editFailurePopup();
        }
    };

    // On server's failure user will be given with the option to try again or go back to main screen
    $scope.editFailurePopup = function() {
        var alertPopup = $ionicPopup.confirm({
            title: 'Profile edit failure',
            template: '',
            cancelText: 'Cancel',
            cancelType: 'button-' + Config.appMood,
            okText: 'Try again',
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(res) {
            if (res) {
                console.log('editFailurePopup: User selected to try again');
                $scope.sendChangesToServer();
            } else {
                console.log('editFailurePopup: User selected to cancel');
            }
        });
    };

    $scope.onTapProfilePicture = function() {
        $studateActionSheets.showEditProfilePicSheet($scope.takePicture, $scope.getPictureFromGallery);
    };

});
