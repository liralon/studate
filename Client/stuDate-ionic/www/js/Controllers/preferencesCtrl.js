
angular.module('starter.controllers').controller('preferencesCtrl', function($scope, $window, $state, $ionicPopup,
                                                                             $ionicLoading, $users, $studentChoicesSession) {

    $scope.userPreferences = {genderPreference: '', isInvisible: ''};
    $scope.profileDetails = null;
    $scope.gendersOptions = Globals.genderOptions;
    $scope.yesNoOptions = Globals.yesNoOptions;

    $scope.initializePage = function()
    {
        $scope.profileDetails = $window.Globals.userProfile;

        $scope.userPreferences.genderPreference = $window.Globals.userProfile.genderPreferenceOptions;
        $scope.userPreferences.isInvisible = $window.Globals.userProfile.isInvisible;
    };

    $scope.submitChanges = function()
    {
        var alertPopup = $ionicPopup.confirm({
            title: 'Submit changes',
            template: 'Are you sure?',
            okType: 'button-' + Config.appMood
        });

        alertPopup.then(function(res)
        {
            if (res)
            {
                $scope.sendPreferencesToServer();
            }
            else
            {
                console.log("submitChanges: Cancel. Don't submit Changes. Do nothing.");
            }
        });
    };

    $scope.discardChanges = function()
    {
        console.log('discardChanges: OK. discarding Changes');
        $state.go('app.findPeoplePage');
    };

    $scope.sendPreferencesToServer = function()
    {
        // Show loading animation while we are doing logic in the background
        $ionicLoading.show({
            template : '<h4>Editing...</h4><i class="icon ion-loading-c icon_test"></i>'
        });
        console.log('submitChanges: OK. submit Changes');
        $users.editProfile(
            $scope.changePreferencesCallback,
            $scope.profileDetails.age,
            $scope.profileDetails.gender,
            $scope.userPreferences.genderPreference,
            $scope.profileDetails.faculty,
            $scope.userPreferences.isInvisible);
    };

    $scope.changePreferencesCallback = function(isSuccess)
    {
        $ionicLoading.hide();
        if (isSuccess)
        {
            console.log('preferencesCtrl.changePreferencesCallback(): server changed user preferences successfully.');
            $window.Globals.userProfile.genderPreferenceOptions = $scope.userPreferences.genderPreference;
            $window.Globals.userProfile.isInvisible = $scope.userPreferences.isInvisible;
            $studentChoicesSession.resetAvailableProfiles();
            $state.go('app.findPeoplePage');
        }
        else
        {
            console.log('preferencesCtrl.changePreferencesCallback(): server failed. Try again.');
            $scope.preferencesFailurePopup();
        }
    };

    //On server's failure user will be given with the option to try again or go back to main screen
    $scope.preferencesFailurePopup = function()
    {
        var alertPopup = $ionicPopup.confirm({
            title: 'Preferences edit failure',
            template: '',
            cancelText: 'Cancel',
            cancelType: 'button-' + Config.appMood,
            okText: 'Try again',
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(res)
        {
            if (res)
            {
                console.log('preferencesFailurePopup: OK');
                $scope.sendPreferencesToServer();
            }
            else
            {
                console.log('preferencesFailurePopup: Cancel');
            }
        });
    };

});
