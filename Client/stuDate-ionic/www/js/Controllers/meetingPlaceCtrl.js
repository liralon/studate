
angular.module('starter.controllers').controller('meetingPlaceCtrl', function($scope, $state, $stateParams, $window,
                                                                              $datesManager, $pushNotifications, $ionicPopup,
                                                                              $ionicViewService, $cordovaToast, 
                                                                              $studateActionSheets, $ionicLoading) {

    $scope.undefinedMeetingPlace = $window.undefinedMeetingPlace;

    $scope.date = {name: '', gender: '', faculty: '', profilePicture: ''};
    $scope.places = $window.Globals.meetingPlaces;

    // Logic variables
    $scope.selectValues = {     // Note - This must be in an object because of AngularJS ng-model variables requirement
        chosenLocation: $window.undefinedMeetingPlace,
        otherMeetingPlace: ""
    };
    $scope.flags = {
        suggestToken : false,
        locationSuggested : $window.undefinedMeetingPlace
    };

    $scope.showOtherLocationInput = false;

    $scope.initPage = function()
    {
        console.log('meetingPlaceCtrl.initPage');

        $ionicViewService.nextViewOptions({
            disableAnimate: true,
            disableBack: true
        });

        // Initialize date properties
        $scope.date.name = $window.Globals.currentDate.datePeerProfile.name;
        $scope.date.gender = $window.Globals.currentDate.datePeerProfile.gender;
        $scope.date.faculty = $window.Globals.currentDate.datePeerProfile.faculty;
        $scope.date.profilePicture = "data:image/jpeg;base64," + $window.Globals.currentDate.datePeerProfile.profilePicture;
        // Note - This is useful for debugging page as standalone
        /*$scope.date.name = 'Given Last';
        $scope.date.gender = 'MALE';
        $scope.date.faculty = 'Exact Science';
        $scope.date.profilePicture = "data:image/jpeg;base64," + '$window.Globals.currentDate.datePeerProfile.profilePicture;';*/

        // Initialize parameters
        console.log('$stateParams.parameters: ' + $stateParams.parameters);
        var parameters = JSON.parse($stateParams.parameters);
        console.log('parameters: ' + JSON.stringify(parameters));
        $scope.flags.suggestToken = parameters.suggestToken;
        $scope.flags.locationSuggested = parameters.locationSuggested;

        $scope.selectValues.chosenLocation = $scope.flags.locationSuggested;
        // Note - This is useful for debugging page as standalone
        //$scope.flags.suggestToken = true;
    };

    $scope.showOtherLocationInputIfNeeded = function(location)
    {
        console.log('chosen location in combo-box: ' + location);
        $scope.showOtherLocationInput = (location == 'Other');
    };

    /////////////////////////////////
    //	Push notifications logic
    /////////////////////////////////

    $window.Globals.meetingPlaceSuggestedHandler = function(message)
    {
        var dateId = JSON.parse(message.dateId);
        if (($window.Globals.currentDate) && ($window.Globals.currentDate.dateId == dateId))
        {
            console.log('meetingPlaceSuggestedHandler: ' + message.meetingPlace);
            var toastText = ($scope.flags.suggestToken) ?
                ('Your date has declined your suggested meeting place. They suggested a different one.') :
                ('Your date has suggested a meeting place.');
            $cordovaToast.show(toastText, 'long', 'bottom');

            $scope.flags.suggestToken = false;
            $scope.flags.locationSuggested = message.meetingPlace;
            $scope.$apply();
        }
    };

    $window.Globals.meetingPlaceConfirmedHandler = function(message)
    {
        var dateId = JSON.parse(message.dateId);
        if (($window.Globals.currentDate) && ($window.Globals.currentDate.dateId == dateId))
        {
            $window.Globals.currentDate.meetingPlace = $scope.flags.locationSuggested;
            $cordovaToast.show('Your date has confirmed the meeting place.', 'long', 'bottom');
            $state.go('actualDatePage');
        }
    };

    /////////////////////////////////
    //	Location selection logic
    /////////////////////////////////

    $scope.suggestLocation = function()
    {
        console.log('locationSuggested: ' + $scope.selectValues.chosenLocation);

        // Check a location was chosen
        if ($scope.selectValues.chosenLocation == $window.undefinedMeetingPlace)
        {
            $scope.alertNoLocationSet();
            return;
        }

        if ($scope.selectValues.chosenLocation == 'Other')
        {
            if (!$scope.isValidMeetingPlace($scope.selectValues.otherMeetingPlace))
            {
                return;
            }

            console.log('suggestLocation; sending custom location: ' + $scope.selectValues.otherMeetingPlace);
            $scope.selectValues.chosenLocation = $scope.selectValues.otherMeetingPlace;
            $scope.showOtherLocationInput = false;
        }
        
        $scope.flags.locationSuggested = $scope.selectValues.chosenLocation;

        $window.Globals.currentDate.suggestMeetingPlace($scope.selectValues.chosenLocation, function(isSuccess)
        {
            console.log('suggestMeetingPlace returned: ' + isSuccess);
            if (!isSuccess)
            {
                $cordovaToast.show("Failed to communicate with server. Please try again.", "long", "bottom");
                $scope.flags.locationSuggested = $window.undefinedMeetingPlace;
                $scope.$apply();
            }
        });
    };

    $scope.respondLocationSuggestion = function(isConfirmed)
    {
        console.log('respondLocationSuggestion: ' + isConfirmed);

        if (!isConfirmed)
        {
            console.log('respondLocationSuggestion: Switching parts...');

            // Let the user decide a new place
            $scope.flags.suggestToken = true;
            $scope.flags.locationSuggested = $window.undefinedMeetingPlace;
        }
        else
        {
            console.log('$scope.flags.locationSuggested = ' + $scope.flags.locationSuggested);

            $scope.hideResponseButtons = true;
            $ionicLoading.show({
                template : '<h4>Sending response...</h4><i class="icon ion-loading-c icon_test"></i>'
            });
            $window.Globals.currentDate.confirmMeetingPlace(function(isSuccess)
            {
                $ionicLoading.hide();

                console.log('meetingPlaceCtrl.respondLocationSuggestion: ' + isSuccess);

                if (isSuccess)
                {
                    $window.Globals.currentDate.meetingPlace = $scope.flags.locationSuggested;

                    console.log("respondLocationSuggestion.callback: returned true; Nothing left to do.");
                    console.log('Going to actualDatePage...');
                    $state.go('actualDatePage');
                }
                else
                {
                    //$cordovaToast.show("Failed to send meeting place confirm/decline to server." +
                    //    "Please check your connection and try again","long","bottom");
                    $cordovaToast.show('Failed to communicate with server. Please try again.', 'long','bottom')
                    $scope.hideResponseButtons = false;
                    $scope.$apply();
                }
            });
        }   
    };

    $scope.alertNoLocationSet = function()
    {
        var alertPopup = $ionicPopup.alert({
            title: 'No location was selected',
            template: 'Please select location',
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(res) {});
    };


    $scope.alertInvalidTextInput = function(msg)
    {
        var alertPopup = $ionicPopup.alert({
            title: 'Invalid Location',
            template: msg,
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(res) {});
    };

    $scope.isValidMeetingPlace = function(message)
    {
        console.log('isValidMeetingPlace');
        if (message.length == 0)
        {
            $scope.alertInvalidTextInput('Input is required!');
            return false;
        }
        else if (!message.match(/^[0-9a-zA-Zא-ת ?,.]+$/))   // Here ^ means beginning of string and $ means end of string, and [0-9a-z]+ means one or more of character from 0 to 9 OR from a/A to z/Z.
        {
            $scope.alertInvalidTextInput('Only alphanumeric characters are allowed.');
            return false;
        }

        return true;
    };

    $scope.getOutOfDate = function()
    { 
        var getOutHandler = function(shouldDeleteMatch)
        {
            $window.Globals.currentDate.cancelDate(shouldDeleteMatch,
                function(isSuccess)
                {
                    // Nothing to do here
                    // (We don't have any smart thing to do here in case we fail...)
                });
            $window.setStudentNotInDate();
            console.log('Going to findPeoplePage...');
            $state.go('app.findPeoplePage');
        };
        var cancelHandler = function () {};
        $studateActionSheets.showGetOutOfDateSheet(getOutHandler, cancelHandler);
    };
});
