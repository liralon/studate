angular.module('starter.controllers').controller('FindPeopleIntroCtrl', function($scope, $window, $state, $ionicPopup)
{

	$scope.introCards = [];

    var cardTypesIndexes = {
        SWIPE_DOWN_CARD: 0,
        AVAILABILITY_TOGGLE_CARD: 1
    };

	var cardTypes = [{
			index: cardTypesIndexes.SWIPE_DOWN_CARD,
		    title: 'stuDate',
		    subtitle: 'Welcome!',
		    mainMsg: 'You can view your peers here. Like/Dislike or swipe down to decide later',
		    footerMsg: '(swipe down to continue)'
		  }, {
		  	index: cardTypesIndexes.AVAILABILITY_TOGGLE_CARD,
		    title: 'stuDate',
		    subtitle: 'Welcome',
		    mainMsg: 'When you are available on campus, use the availability toggle on the upper right corner',
		    footerMsg: '(toggle your availability to continue)'
		  }];

  	$scope.introCards.push(angular.extend({}, cardTypes[0]));
	
	$scope.introCardSwiped = function(index) 
	{
        if (index != cardTypesIndexes.SWIPE_DOWN_CARD)
        {
            var alertPopup = $ionicPopup.alert({
                title: 'Tutorial instructions',
                template: "Please make sure to read the instructions on the card! :)",
                okType: 'button-' + Config.appMood
            });
            alertPopup.then(function(res) {
                // Nothing to do here
            });
        }

		$scope.introCards.push(angular.extend({}, cardTypes[1]));
	};

    $scope.introCardResponse = function()
    {
        var alertPopup = $ionicPopup.alert({
            title: 'Tutorial instructions',
            template: "Please make sure to read the instructions on the card! :)",
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(res) {
            // Nothing to do here
        });
    };

  	$scope.cardDestroyed = function(index) {
    	$scope.introCards.splice(index, 1);
  	};

	$scope.availabilityIntroStatusChange = function() 
	{
        $scope.flags.availabilityToggle = false;

		if ($scope.introCards[0].index == cardTypesIndexes.AVAILABILITY_TOGGLE_CARD)
        {
			$window.localStorage.setItem('didSwipeDownTut', true);
        	$state.go('app.findPeoplePage');
		}
        else
        {
            var alertPopup = $ionicPopup.alert({
                title: 'Tutorial instructions',
                template: "Please make sure to read the instructions on the card! :)",
                okType: 'button-' + Config.appMood
            });
            alertPopup.then(function(res) {
                // Nothing to do here
            });
        }
	};

});
