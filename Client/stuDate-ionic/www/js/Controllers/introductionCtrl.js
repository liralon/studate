angular.module('starter.controllers')

.controller('introductionCtrl', function($window, $scope, $state, $ionicSlideBoxDelegate, $ionicViewService) {
    
	$ionicViewService.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });
    
    $scope.finishIntroduction = function (){
        $window.localStorage.setItem('didTutorial', true);
        $state.go('init');
    };

});