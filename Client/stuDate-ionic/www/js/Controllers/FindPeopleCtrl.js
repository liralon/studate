
angular.module('starter.controllers').controller('FindPeopleCtrl', function($scope, $window, $state,
                                                                         $ionicSwipeCardDelegate,
                                                                         $studentChoicesSession, $cordovaToast,
                                                                         $ionicLoading) {

    $scope.cards = [];
    $scope.availableProfiles = [];

    var shownCardIndex = 0;
    var maxNumProfilesToShow = 0;

    $scope.invisibiltyStatus = {
        isInvisible : false
    };
    $scope.profileFlags = {
        noProfiles : false
    };

    $scope.refreshFlags = {
        showRefreshText : true,
        showFailureText : false,
        showNoProfilesText: false
    };

    $scope.initFindPeopleCtrl = function()
    {
        console.log('$window.localStorage.didSwipeDownTut = ' + $window.localStorage.didSwipeDownTut);
        
        if ($window.localStorage.didSwipeDownTut == 'true') 
        {
            $scope.invisibiltyStatus.isInvisible = $window.Globals.userProfile.isInvisible;
            maxNumProfilesToShow = $studentChoicesSession.getMaxNumProfiles();

            // don't want to retrieve new cards if just got back to init from edit profile or something similar
            if ($scope.cards.length == 0 && !$scope.invisibiltyStatus.isInvisible)
            {
                $scope.fillProfilesToMax();
            } 
        }
        else 
        {
            $state.go('app.findPeopleIntroPage');
        }
        
    };

    $scope.fillProfilesToMax = function() {
        // If can't fetch more new profiles - still call the callback to update the shown cards
        if (maxNumProfilesToShow == $studentChoicesSession.getNumberOfAvailableProfiles())
        {
            updateProfiles();
            $scope.showCurrentCard();
        }
        else
        {
            $ionicLoading.show({
                template : '<h4>Retrieving profiles...</h4><i class="icon ion-loading-c icon_test"></i>'
            });
            $studentChoicesSession.getNextProfiles(maxNumProfilesToShow, function(isSuccess)
                {

                    $ionicLoading.hide();
                    updateProfiles();
                    if ($scope.cards.length == 0)
                    {
                        $scope.showCurrentCard();
                    }

                    // Note - no special handling for success
                    if (!isSuccess)
                    {
                        if ($scope.availableProfiles.length > 0)
                        {
                            $cordovaToast.show("Error. Failed to fetch more profiles from server. " +
                                "Please check your connection","short","bottom");
                        }
                        else
                        {
                             $scope.refreshFlags.showFailureText = true;
                        }
                    }
                });
        }
    };



    $scope.cardSwiped = function() {
        $scope.cards.splice($scope.cards.length - 1, 1);
        ++shownCardIndex;
        console.log("swipe - shown index after swipe: " + shownCardIndex);
    };

    $scope.showCurrentCard = function() {
        if ($scope.availableProfiles.length > 0)
        {
            shownCardIndex %= $scope.availableProfiles.length;
            $scope.cards.push(angular.extend({}, $scope.availableProfiles[shownCardIndex]));
        }
    };

    $scope.cardDestroyed = function() {
        // If finished all current cards
        if (shownCardIndex >= $scope.availableProfiles.length)
        {
            shownCardIndex = 0;
            console.log('finished loop. shown card index: ' + shownCardIndex);
            // add new profiles if there is place for more
            if ($scope.availableProfiles.length < maxNumProfilesToShow)
            {
                $scope.fillProfilesToMax();
            }
            else
            {
               $cordovaToast.show("Maximum number of unanswered profiles reached. " +
                   "Like\\Dislike to receive new profiles.","long","bottom");   
                $scope.showCurrentCard();
            }
        }
        else
        {
            $scope.showCurrentCard();
        }
    };

    // TODO - This currently send every response to the server immediately
    // TODO - Don't use properly the strong Server APIs that offer sending a bounch of likes together
    // TODO - I think the real logic should be:
    // TODO - (*) Accumalate likes/dislikes while they are input.
    // TODO - (*) Add a timeout so that if a certain of time passed from the last like/dislike, the send all acuumlated choices
    // TODO - (Note - This is similar to the behaviour that we do with the "Availability toggle"
    // TODO - If changing the logic to accumulate cards - look at current logic carefuly
    // TODO - it currently assumes only one answer
    $scope.currCardResponse = function(isLikeResponse) {
        console.log("resp - shown index: " + shownCardIndex);
        var currCard = $scope.cards[$scope.cards.length - 1];
        console.log('currCard: ' + currCard.emailAddress);
        var currCardResponse = {'email' : currCard.emailAddress, 'result' : isLikeResponse};
        
        // delete card immediately because it was answered
        var emails = [currCard.emailAddress];
        $scope.cards.splice($scope.cards.length - 1, 1);
        $scope.availableProfiles.splice(shownCardIndex,1);
        $studentChoicesSession.deleteProfilesByEmail(emails);

        var newResponses = [currCardResponse];

        var pendingResponses = $studentChoicesSession.getPendingResponses();
        var responses = (pendingResponses.length > 0) ? (newResponses.concat(pendingResponses)) : (newResponses);        

        $studentChoicesSession.setProfileAnswers(responses, maxNumProfilesToShow, false, function(isSuccess) {
            if (isSuccess) 
            {
                $studentChoicesSession.clearPendingResponses();
            }
            else
            {
                $studentChoicesSession.addPendingResponses(newResponses);
            }
        });
    };

    $scope.showRefreshTextInDelay = function() {
        setTimeout(function() {
            $scope.refreshFlags.showRefreshText = true;
            $scope.$apply();
        }, 2000);
    };

    $scope.refreshPull = function() {
        $scope.refreshFlags.showRefreshText = false;
        $scope.refreshFlags.showFailureText = false;
        $scope.refreshFlags.showNoProfilesText = false;
        $scope.$apply();

        $scope.showRefreshTextInDelay();
    };

    $scope.refreshCards = function() {
        $scope.fillProfilesToMax();
        $scope.$broadcast('scroll.refreshComplete');

        $scope.showRefreshTextInDelay();
     };

    // should be called when deck is over
    var updateProfiles = function()
    {
        $scope.availableProfiles = $studentChoicesSession.getAvailableProfiles();
        $scope.profileFlags.noProfiles = (0 == $scope.availableProfiles.length);
        $scope.refreshFlags.showNoProfilesText = (0 == $scope.availableProfiles.length);
    };

    $scope.invisCards = [];

    var invisCard = {
            index: 0,
            title: 'stuDate',
            subtitle: 'Invisible Mode',
            mainMsg: 'You are currently invisible, others can\'t see you',
            footerMsg: 'You can change this in Preferences'
    };

    $scope.invisCards.push(angular.extend({}, invisCard));
    
    $scope.invisCardSwiped = function(index) 
    {
        $scope.invisCards.push(angular.extend({}, invisCard));
    };

    $scope.invisCardDestroyed = function(index) {
        $scope.invisCards.splice(index, 1);
    };
})



.controller('CardCtrl', function($scope, $ionicSwipeCardDelegate, $pushNotifications,$ionicPopup, $window) {
    $scope.cardResponse = function(boolResponse)
    {
        if ($pushNotifications.isPushRegistrationSuccessful()){
            var card = $ionicSwipeCardDelegate.getSwipebleCard($scope);
            card.swipe(false);
            $scope.currCardResponse(boolResponse); // Note - it is important to call this here
        }
        else { 
            console.log('device not registered for push notifications');
            var alertPopup = $ionicPopup.alert({
                title: 'Notifications Disabled',
                template: 'Please enable notifications to enjoy the full studate experience.',
                okType: 'button-' + Config.appMood
            });
            alertPopup.then(function(res) {});
        }
    };
});
