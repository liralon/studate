
angular.module('starter.controllers').controller('signUpCtrl', function($scope, $window, $state, $ionicPopup,
                                                                        $users, $cameraUtils, $pushNotifications,
                                                                        $ionicLoading, $studateActionSheets, $ionicViewService,
                                                                        $connectionUtils) {

    var notSetOptionName = 'Not Set';

    $ionicViewService.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });

    $scope.registrationCompleted = false;
    $scope.userDetails = {
        gender: notSetOptionName,
        faculty: notSetOptionName,
        age: 25,
        genderPreferences: notSetOptionName,
        profilePictureBase64: ''
    };
    $scope.gender = $window.Globals.genders;
    $scope.preferredGender =  $window.Globals.genderOptions;
    $scope.faculties = $window.Globals.faculties;

    $scope.takePicture = function()
    {
        console.log('In takePicture function');
        $cameraUtils.takePicture(function(imageData)
        {
            if (imageData)
            {
                $scope.imgDataURL = imageData;
                $scope.userDetails.profilePictureBase64 = imageData;
                $scope.imgURI = "data:image/jpeg;base64," + imageData;
            }
        });
    };

    $scope.getPictureFromGallery = function()
    {
        $cameraUtils.takeFromGallery(function(imageData)
        {
            if (imageData)
            {
                $scope.imgDataURL = imageData;
                $scope.userDetails.profilePictureBase64 = imageData;
                $scope.imgURI = "data:image/jpeg;base64," + imageData;
            }
        });
    };

    $scope.allFieldsFilledIn = function()
    {
        if($scope.userDetails.gender == notSetOptionName)
        {
            $scope.alertEmptyField('gender');
            return false;
        }
        else if ($scope.userDetails.faculty == notSetOptionName)
        {
            $scope.alertEmptyField('faculty');
            return false;
        }
        else if ($scope.userDetails.genderPreferences == notSetOptionName)
        {
            $scope.alertEmptyField("preferred partner's gender");
            return false;
        }
        else if ($scope.imgURI === undefined)
        {
            $scope.alertNoProfilePicture();
            return false;
        }

        return true;
    };

    $scope.signUpUser = function()
    {
        if ($scope.allFieldsFilledIn())
        {
            // Show loading animation while we are doing logic in the background
            $ionicLoading.show({
                template : '<h4>Setting up your profile...</h4><i class="icon ion-loading-c icon_test"></i>'
            });
            $users.getUserInfo(function(userInfo)
            {
                if (userInfo)
                {
                    $users.register(
                        userInfo.name,
                        $scope.userDetails.age,
                        $scope.userDetails.faculty,
                        $scope.userDetails.gender,
                        $scope.userDetails.genderPreferences,
                        $scope.userDetails.profilePictureBase64,
                        $scope.serverSignUpCallback);
                }
                else
                {
                    $ionicLoading.hide();
                    console.log('Failed to get user info - Registration failed!');
                    $scope.registrationFailedPopup();
                }
            });
        }
    };

    $scope.serverSignUpCallback = function(result)
    {
        if (result)
        {
            console.log('signUpCtrl.serverSignUpCallback(): server registered user.');

            // This hides the original display and sets a button which uses initCtrl and signIn to continue
            $scope.registrationCompleted = true;

            $users.signIn(function(signInResult)
            {
                console.log('Value of hideTauAuthenticaton is: ' + $scope.hideTauAuthenticaton);
                $ionicLoading.hide();

                if ((signInResult == $users.COMMUNICATION_FAILURE) || (!$connectionUtils.isConnectedToInternet()))
                {
                    console.log('Communication failure on signIn!');
                    $scope.communicationFailurePopup();
                }
                else if (signInResult == $users.OAUTH_LOGIN_FAILURE)
                {
                    // User isn't logged-in to TAU redirect to "TAU authentication" screen
                    $scope.hideTauAuthenticaton = false;
                    console.log('Changed hideTauAuthenticaton to false');
                    console.log('authenticate to TAU');
                    $state.go('tauAuthenticaionPage');
                }
                else 
                {
                    $window.Globals.registerPushNotificationHandlers();
                    $pushNotifications.setUserSignedIn();

                    $window.Globals.userProfile = signInResult;
                    $window.Globals.userAvailable = false;
                    $scope.loadedProfile = signInResult;

                    console.log('Going to findPeoplePage...');
                    $state.go('app.findPeoplePage');
                }
            }, true);
        }
        else
        {
            $ionicLoading.hide();
            console.log('signUpCtrl.serverSignUpCallback(): registration failed. Try again.');
            $scope.registrationFailedPopup();
        }
    };

    // On server's failure user will be given with the option to try again or go back to welcome screen
    $scope.registrationFailedPopup = function()
    {
        var alertPopup = $ionicPopup.confirm({
            title: 'Registration failed',
            template: '',
            cancelText: 'Cancel',
            cancelType: 'button-' + Config.appMood,
            okText: 'Try again',
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(shouldTryAgain) {
            if (shouldTryAgain)
            {
                console.log('User chose to try register again');
                $scope.signUpUser();
            }
            else
            {
                console.log('User chose to not try register again.');
                // Just return to registration screen and if he wants to try again, he can just press the register button
            }
        });
    };

    $scope.alertEmptyField = function(field)
    {
        $scope.alertUser('Empty field', 'Please fill in your ' + field);
    };

    $scope.alertNoProfilePicture = function()
    {
        $scope.alertUser('Empty field', 'Please set a profile picture');
    };

    $scope.alertUser = function(title, templateString)
    {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: templateString,
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(res) {
        });
    };

    // Triggered on tapping the picture zone
    $scope.onTapProfilePicture = function() {
        $studateActionSheets.showEditProfilePicSheet($scope.takePicture, $scope.getPictureFromGallery);
    };

});