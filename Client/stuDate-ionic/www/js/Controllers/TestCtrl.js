
angular.module('starter.controllers', ['ionic', 'ionic.contrib.ui.cards', 'studateAppEngine', 'ngCordova'])

.controller('TestCtrl', function($scope, $window, $users, $pushNotifications) {
    $scope.signIn = function()
    {
        alert('Called signin!');

        $users.signIn(function(signInResult)
        {
            $window.alert('SignIn result: ' + JSON.stringify(signInResult));
            $window.Globals.userProfile = signInResult;
        });
    };

    $scope.initPush = function()
    {
        alert('called initPush');
        $pushNotifications.registerWithServer(function(isSuccess) { alert(isSuccess); });

    };

    $scope.register = function()
    {
        $users.getUserInfo(function(userInfo)
        {
            debugger;
            $users.register(userInfo.name, 'CS', 'MALE', 'FEMALE', btoa('SomePictureData\x12\x34\x56\x78'), function(registerResult)
            {
                $window.alert('Register result: ' + registerResult);
            });
        });
    };

    $scope.deleteLocalStorage = function()
    {
        $window.localStorage.removeItem('didTutorial');
        $window.localStorage.removeItem('nextProfilesCursor');
        alert('Deleted all!');
    };

    $scope.unregister = function()
    {
        $users.unregister(function(unregisterResult)
        {
            $window.alert('Unregister result: ' + unregisterResult);
        });
    };

    $scope.doSomething = function()
    {
        console.log("CONFIRM");
    };
});
