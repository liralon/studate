
angular.module('starter.controllers').controller('AppCtrl', function($scope, $window, $state, $ionicModal, $studate, $users,
                                                                     $availableDecleration, $datesManager, $ionicPopup,
                                                                     $pushNotifications, $ionicViewService, $cordovaToast,
                                                                     $ionicLoading, $studateActionSheets, $studentChoicesSession) {

    $scope.debug = Config.debug;

    $window.Globals.dateAlertHandler = function(message, isColdStart)
    {
        // Note - We need to handle only the case of regular push notification (not coldstart)
        // Note - because when going back to the app from a coldstart notification, the signIn in initCtrl
        // Note - logic is activated which will do the relevant stuff anyway
        if (!isColdStart)
        {
            if (!$window.Globals.currentDate)
            {
                $window.Globals.currentDate = new StudateDate($datesManager, JSON.parse(message.dateId));
                // Note - This is done in order to overcome a bug of getting a push notification with a dateId of a date
                // Note - that doesn't exists
                $window.Globals.currentDate.getDateInfo(function(dateInfo)
                {
                    if (dateInfo)
                    {
                        $window.Globals.userAvailable = false;
                        $scope.flags.availabilityToggle = false;
                        $scope.$apply();

                        $scope.openDateConfirmationModal();
                    }
                    else
                    {
                        $window.setStudentNotInDate();
                    }
                });
            }
            else
            {
                console.log('Probably got duplicate push notification: ' + JSON.stringify(message));
                console.log('I think currentDate is: '  + JSON.stringify($window.Globals.currentDate));
            }
        }
    };

    $window.Globals.dateResultHandler = function(message)
    {
        var dateId = JSON.parse(message.dateId);
        if (($window.Globals.currentDate) && ($window.Globals.currentDate.dateId == dateId))
        {
            $window.Globals.currentDate = new StudateDate($datesManager, dateId);
            $scope.handleDateConfirmationResult(
                JSON.parse(message.isConfirmed),
                (message.isSetter) ? (JSON.parse(message.isSetter)) : (false));
        }
        else
        {
            console.log('Probably got duplicate push notification: ' + JSON.stringify(message));
            console.log('I think currentDate is: '  + JSON.stringify($window.Globals.currentDate));
        }
    };

    $scope.initMenu = function()
    {
        console.log('initializing main screen');

        $ionicModal.fromTemplateUrl('templates/confirmationView.html', {
            scope: $scope,
            animation: 'slide-in-up',
            backdropClickToClose: false,
            hardwareBackButtonClose: false
        }).then(function(modal) {
            $scope.dateConfrimationModal = modal;
        });

        $scope.loadedProfile = $window.Globals.userProfile;

        $scope.flags = {
            availabilityToggle: $window.Globals.userAvailable,
            isDateSuggestionConfirmed: $window.Globals.userProfile.dateState == $window.dateStates.DATE_SUGGESTION_CONFIRMED
        };

        if ($window.Globals.userProfile.dateState == $window.dateStates.DATE_CONFIRMATION)
        {
            var message = { dateId: $window.Globals.userProfile.pendingDateId };
            // Note - This must be done in a callback because internally "dateAlertHandler" assumes it runs from a callback
            // Note - (For example, it calls "$scope.$apply();")
            setTimeout(function() { $window.Globals.dateAlertHandler(message); }, 0);
        }
        else if ($window.Globals.userProfile.dateState == $window.dateStates.DATE_SUGGESTION_CONFIRMED)
        {
            $window.Globals.currentDate = new StudateDate($datesManager, $window.Globals.userProfile.pendingDateId);
        }
    };

    $scope.shareStudate = function()
    {
        window.plugins.socialsharing.share('Download stuDate: ', 'I invite you to try stuDate!', null, Config.studateURL);
    };

    $scope.logout = function()
    {
        console.log('Logging out...');
        $ionicLoading.show({
            template : '<h4>Logging out...</h4><i class="icon ion-loading-c icon_test"></i>'
        });

        // Note - We must declare to be not available because we don't want it to be possible to schedule a date while we
        // Note - are logged out...
        console.log('Declaring not available...');
        $availableDecleration.declareNotAvailable(function(isSuccess)
        {
            console.log('declareNotAvailable result: ' + isSuccess);
            if (isSuccess)
            {
                $window.Globals.userAvailable = false;
                $scope.$apply();
            }
            else
            {
                $scope.availabilityCallbackFailure();
            }

            console.log('Delete available profiles from local storage...');
            $studentChoicesSession.resetAvailableProfiles();

            console.log('Unregister push notification handlers...');
            $pushNotifications.unregisterAllListeners();

            console.log('Signout from oauth2...');
            $users.signOut(function()
            {
                $ionicLoading.hide();
                $state.go('init');
            });
        });
    };

    ///////////////////////////
    // Match confirmation dateConfrimationModal
    ///////////////////////////

    $scope.openDateConfirmationModal = function() {
        console.log("MainCtrl: open dateConfrimationModal!");
        $ionicLoading.hide();
        $scope.dateConfrimationModal.show();
    };

    $scope.closeDateConfirmationModal = function() {
        $scope.dateConfrimationModal.hide();
    };

    //Cleanup the dateConfrimationModal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.dateConfrimationModal.remove();
    });

    $scope.availableStatusMap = {"true":"Available on campus!", "false":"I am currently unavailable"};

    var timeToWaitToSendAvailibiltyChange = 500;
    var lastTimeAvailabilityChanged = null;

    var updateAvailabilityStatusOnServer = function()
    {
        var currentTime = new Date();
        var timePassedFromLastAvailabilityChange = currentTime - lastTimeAvailabilityChanged;
        if (timePassedFromLastAvailabilityChange >= timeToWaitToSendAvailibiltyChange)
        {
            if ($scope.flags.availabilityToggle)
            {
                $window.Globals.userAvailable = true;
                console.log('Declaring available: calling ($availableDecleration.declareAvailable)');
                $availableDecleration.declareAvailable($scope.callbackClickAvailable);
            }
            else
            {
                $window.Globals.userAvailable = false;
                console.log('Declaring unavailable: calling ($availableDecleration.declareNotAvailable)');
                $availableDecleration.declareNotAvailable($scope.callbackClickNotAvailable);
            }
        }
    };

    $scope.availabilityCallbackFailure = function(correctAvailabilityStatus) {
        $cordovaToast.show("Error. Failed to change availability status." +
                                "Please check your connection and try again","long","bottom");
        $scope.flags.availabilityToggle = correctAvailabilityStatus;
        $scope.$apply();
    };

    $scope.callbackClickAvailable = function(isSuccess)
    {
        console.log("callbackClickAvailable: Returned " + isSuccess);

        if (isSuccess)
        {
            $scope.$apply();
        }
        else
        {
           $scope.availabilityCallbackFailure(false);
        }
    };

    $scope.callbackClickNotAvailable = function(isSuccess)
    {
        console.log("callbackNotAvailable: clickNotAvilablable returned " + isSuccess);
       
        if (isSuccess)       
        {
            $scope.$apply();
        }
        else
        {
            $scope.availabilityCallbackFailure(true);
        }
    };

    $scope.availabilityStatusChange = function()
    {
        console.log('availabilityStatusChange: Availability status change was detected!');
        console.log('availabilityStatusChange: toggle was set to ' + $scope.flags.availabilityToggle);

        lastTimeAvailabilityChanged = new Date();
        setTimeout(updateAvailabilityStatusOnServer, timeToWaitToSendAvailibiltyChange);
    };

    $scope.confirmDateButtonHandler = function()
    {
        console.log("User chose to confirm [from Modal].");
        $scope.closeDateConfirmationModal();
        $scope.flags.isDateSuggestionConfirmed = true;

        $window.Globals.currentDate.confirm(function(isSuccess)
        {
            console.log('confirmation result: ' + isSuccess);

            if (!isSuccess)
            {
                $scope.flags.isDateSuggestionConfirmed = false;
                $scope.$apply();
                $cordovaToast.show(
                    'Failed to communicate with server. Please try again.',
                    'long', 'bottom');
                $scope.openDateConfirmationModal();
            }
        });
    };

    $scope.declineDateButtonHandler = function() {
        console.log("User chose to decline [from Modal].");
        $scope.closeDateConfirmationModal();
        $window.Globals.currentDate.decline(function(result)
        {
            // Note - In case decline failed, we don't have anything to do with it
            // Note - The date's peer can always cancel the date if it wants to
            console.log('$window.Globals.currentDate.decline returned ' + result);
            $window.setStudentNotInDate();
        });
    };

    $scope.handleDateConfirmationResult = function(didConfirmed, isMeetingPlaceSetter)
    {
        if (didConfirmed)
        {
            console.log("Both sides confirmed the date!");
            $window.Globals.currentDate.getDateInfo(function(result)
            {
                if (result)
                {
                    --$scope.loadedProfile.pendingMatches;

                    $ionicViewService.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true
                    });

                    var parameters = {
                        suggestToken: isMeetingPlaceSetter,
                        locationSuggested: $window.Globals.currentDate.meetingPlace
                    };
                    $state.go('meetingPlacePage', { 'parameters' : JSON.stringify(parameters) });
                }
                else
                {
                    var alertPopup = $ionicPopup.confirm({
                        title: 'Communication Failure',
                        template: "Failed to get date's peer information <br> Try again or cancel date?",
                        cancelText: 'Cancel Date',
                        cancelType: 'button-' + Config.appMood,
                        okText: 'Try again',
                        okType: 'button-' + Config.appMood
                    });
                    alertPopup.then(function(res)
                    {
                        if (res)
                        {
                            // Try again
                            setTimeout(function() { $scope.handleDateConfirmationResult(didConfirmed, isMeetingPlaceSetter); }, 0);
                        }
                        else
                        {
                            $window.Globals.currentDate.cancelDate(false, function(isSuccess)
                            {
                                // Nothing to do here
                                // (We don't have any smart thing to do here in case we fail...)
                            });
                            $window.setStudentNotInDate();
                            $state.go('app.findPeoplePage');
                        }
                    });
                }
            });
        }
        else
        {
            console.log("Other side declined the date!");
            $window.setStudentNotInDate();

            var alertPopup = $ionicPopup.alert({
                title: 'Date Declined',
                template: "We are sorry, but other side declined the date... :(",
                okType: 'button-' + Config.appMood
            });
            alertPopup.then(function(res) {
                // TODO - This is probably useless but it doesn't hurt so I leave it in the code just in case...
                $scope.closeDateConfirmationModal();
            });
        }

        $scope.closeDateConfirmationModal();
        $scope.$apply();
    };

    // TODO - Get rid of duplicate code that does the same thing (Note that there may be small differences)
    $scope.regretDateConfirmation = function()
    { 
        var getOutHandler = function(shouldDeleteMatch)
        {
            $window.Globals.currentDate.cancelDate(shouldDeleteMatch,
                function(isSuccess)
                {
                    // (We don't have any smart thing to do here in case we fail...)
                });
            $window.setStudentNotInDate();
            $scope.flags.isDateSuggestionConfirmed = false;
            console.log('Going to findPeoplePage...');
            $state.go('app.findPeoplePage');
        };
        var cancelHandler = function() {};
        $studateActionSheets.showGetOutOfDateSheet(getOutHandler, cancelHandler);
    };
});