
angular.module('starter.controllers').controller('initCtrl', function($ionicPlatform, $window, $scope, $state, $studate, $ionicLoading,
                                                                      $users, $oauthLogin, $pushNotifications,
                                                                      $availableDecleration, $datesManager, $ionicPopup,
                                                                      $ionicViewService, $connectionUtils, $stateParams) {

    $scope.initializePage = function()
    {
        var parameters = JSON.parse($stateParams.parameters);
        if (parameters && parameters.skipToPageByDateState)
        {
            $scope.goToPageByDateState();
        }
        else
        {
            // Show loading animation while we are doing logic in the background
            $ionicLoading.show({
                template : '<h4>Connecting...</h4><i class="icon ion-loading-c icon_test"></i>'
            });

            $ionicViewService.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });

            console.log('Waiting for ionicPlatform to be ready...');
            $ionicPlatform.ready($scope.realInitializePage);

            $window.Globals.registerPushNotificationHandlers = registerPushNotificationHandlers;
        }
    };

    $scope.realInitializePage = function()
    {
        console.log('Ionic platform ready!');

        $connectionUtils.init();
        $pushNotifications.init();

        if ($connectionUtils.isConnectedToInternet())
        {
            // Note - This is called with setTimeout in purpose because it uses $scope.$apply()
            setTimeout($scope.initApp, 0);
        }
        else
        {
            $ionicLoading.hide();
            $scope.communicationFailurePopup();
        }
    };

    // On server's success - alert user
    $scope.communicationFailurePopup = function() {
        // TODO - (ophir) leaving this in case we ever decide to do it, but I think current mechanism
        // TODO - (ophir) is o.k since we do not have defined behavior when user is offline.
        // TODO - THIS IS A BAD MECHANISM TO REPORT AN ERROR
        // TODO - I THINK WE SHOULD SHOW IN THE MAIN VIEW OF THE APP THAT WE DETECTED NO INTERNET CONNECTION
        // TODO - AND TELL THE USER TO CONNECT.
        // TODO - IN ADDITION, WE CAN USE THE "online"/"offline" HANDLERS OF $connectionUtils TO KNOW WHEN THE
        // TODO - USER WILL BE CONNECTED WITHOUT ASKING FOR HIS PERMISSIONS TO CHECK AGAIN...
        var alertPopup = $ionicPopup.alert({
            title: 'Network failure',
            template: "Please verify you're connected to network and try again.",
            okType: 'button-' + Config.appMood
        });
        alertPopup.then(function(res) {
            console.log('Exited communicationFailurePopup');
            if ($connectionUtils.isConnectedToInternet())
            {
                console.log('Connection is now good!');
                console.log('Trying to signin to studate again');
                $scope.initApp();
            }
            else
            {
                console.log('Connection is still bad...');
                $scope.communicationFailurePopup();
            }
        });
    };

    $scope.initApp = function()
    {
        // Show loading animation while we are doing logic in the background
        $ionicLoading.show({
            template : '<h4>Connecting...</h4><i class="icon ion-loading-c icon_test"></i>'
        });

        $ionicViewService.nextViewOptions({
            disableAnimate: true,
            disableBack: true
        });

        $scope.hideTauAuthenticaton = false;
        console.log('Changed hideTauAuthenticaton to false');
        $scope.$apply();

        console.log('Waiting for AppEngine API to load...');
        $studate.waitForAPIToLoad(function(isSuccess)
        {
            console.log('Finished waiting for AppEngine API to load!');
            $ionicLoading.hide();

            if (!isSuccess)
            {
                // The only way we can fail is if internet connection was disconnected while loading the API
                $scope.communicationFailurePopup();
                return;
            }

            if ($window.localStorage.didTutorial == 'true')
            {
                if ($oauthLogin.didLoginInPast())
                {
                    $scope.loginToStudate();
                }
                else
                {
                    // User isn't logged-in to TAU redirect to "TAU authentication" screen
                    console.log('authenticate to TAU');
                    $state.go('tauAuthenticaionPage');
                }
            }
            else
            {
                $state.go('introduction');
            }
        });
    };

    $scope.loginToStudateFromButton = function()
    {
        // Note - We call the function with timeout because it is expected to be called outside of $scope (It does $scope.$apply())
        setTimeout($scope.loginToStudate, 0);
    };

    $scope.loginToStudate = function()
    {
        console.log('Signing in... (Called loginToStudate)');

        // Show loading animation while we are doing logic in the background
        $ionicLoading.show({
            template : '<h4>Signing in...</h4><i class="icon ion-loading-c icon_test"></i>'
        });

        $scope.hideTauAuthenticaton = true;
        console.log('Changed hideTauAuthenticaton to true');
        $scope.$apply();

        $ionicViewService.nextViewOptions({
            disableAnimate: true,
            disableBack: true
        });

        $users.signIn(function(signInResult)
        {
            console.log('Value of hideTauAuthenticaton is: ' + $scope.hideTauAuthenticaton);
            $ionicLoading.hide();

            if ((signInResult == $users.COMMUNICATION_FAILURE) || (!$connectionUtils.isConnectedToInternet()))
            {
                console.log('Communication failure on signIn!');
                $scope.communicationFailurePopup();
            }
            else if (signInResult == $users.OAUTH_LOGIN_FAILURE)
            {
                // User isn't logged-in to TAU redirect to "TAU authentication" screen
                $scope.hideTauAuthenticaton = false;
                console.log('Changed hideTauAuthenticaton to false');
                console.log('authenticate to TAU');
                $state.go('tauAuthenticaionPage');
            }
            else if (signInResult == $users.AUTHENTICATION_FAILURE)
            {
                console.log('User logged in with an invalid google account (Not mail.tau.ac.il / staff account)');
                // TODO - Rephrase template text...
                var alertPopup = $ionicPopup.alert({
                    title: 'Authentication failure',
                    template: "You have mistakenly logged-in with an invalid stuDate account. Account must be mail.tau.ac.il / staff account",
                    okType: 'button-' + Config.appMood
                });
                alertPopup.then(function(res) {
                    // Note - We must signOut the user from the account it authenticated with and force him to login again
                    $ionicLoading.show({
                        template : '<h4>Logging out...</h4><i class="icon ion-loading-c icon_test"></i>'
                    });
                    $users.signOut(function()
                    {
                        $ionicLoading.hide();
                        $scope.loginToStudate();
                    });
                });
            }
            else if (signInResult == $users.NO_STUDATE_USER_FAILURE)
            {
                // User is not registered to stuDate - Redirect to sign-up screen
                console.log('Going to signUp page...');
                $state.go('signUp');
            }
            else
            {
                $scope.onStudateSignInSuccess(signInResult);
            }
        });
    };

    var dateAlertHandler = function(message, isColdStart)
    {
        console.log('Got DATE_ALERT notification!');
        $window.Globals.dateAlertHandler(message, isColdStart);
    };

    var dateResultHandler = function(message, isColdStart)
    {
        console.log('Got DATE_RESULT notification! message:' + JSON.stringify(message));
        $window.Globals.dateResultHandler(message, isColdStart);
    };

    var meetingPlaceSuggestedHandler = function(message, isColdStart)
    {
        console.log('Got DATE_MEETING_PLACE notification!');

        // Note - We need to handle only the case of regular push notification (not coldstart)
        // Note - because when going back to the app from a coldstart notification, the signIn in initCtrl
        // Note - logic is activated which will do the relevant stuff anyway
        if (!isColdStart)
        {
            $window.Globals.meetingPlaceSuggestedHandler(message);
        }
    };

    var meetingPlaceConfirmedHandler = function(message, isColdStart)
    {
        console.log('Got LOCATION_RESPONSE notification!');

        // Note - We need to handle only the case of regular push notification (not coldstart)
        // Note - because when going back to the app from a coldstart notification, the signIn in initCtrl
        // Note - logic is activated which will do the relevant stuff anyway
        if (!isColdStart)
        {
            console.log('Calling $window.Globals.meetingPlaceConfirmedHandler');
            $window.Globals.meetingPlaceConfirmedHandler(message);
        }
    };

    var dateCanceledHandler = function(message, isColdStart)
    {
        console.log('Got DATE_CANCELED notification!');

        var dateId = JSON.parse(message.dateId);
        if (($window.Globals.currentDate) && ($window.Globals.currentDate.dateId == dateId))
        {
            $window.setStudentNotInDate();

            var dateCanceledAlert = $ionicPopup.alert({
                title: 'Date Canceled',
                template: 'We are sorry but your date peer has canceled the date... :(',
                okText: 'Go back to main screen',
                okType: 'button-' + Config.appMood
            });
            dateCanceledAlert.then(function(res)
            {
                var parameters = {
                    skipToPageByDateState: true
                };
                $state.go('init', { 'parameters' : JSON.stringify(parameters) });
            });
        }
        else
        {
            console.log('Probably got duplicate push notification: ' + JSON.stringify(message));
            console.log('I think currentDate is: '  + JSON.stringify($window.Globals.currentDate));
        }
    };

    var dateChatMesssageHandler = function(message, isColdStart)
    {
        console.log('Got DATE_CHAT_MSG notification!');
        $window.Globals.dateChatMessageHandler(message);
    };

    var broadcastMessageHandler = function(message, isColdStart)
    {
        console.log('Got BROADCAST_MESSAGE notification!');

        var broadcastMessageAlert = $ionicPopup.alert({
            title: 'Message from stuDate',
            template: message.messageText,
            okText: 'Ok',
            okType: 'button-' + Config.appMood
        });
        broadcastMessageAlert.then(function(res)
        {
            // Nothing to do here
        });
    };

    var registerPushNotificationHandlers = function()
    {
        $pushNotifications.registerListener($window.PushMessageTypes.DATE_ALERT, dateAlertHandler);
        $pushNotifications.registerListener($window.PushMessageTypes.DATE_RESULT, dateResultHandler);
        $pushNotifications.registerListener($window.PushMessageTypes.MEETING_PLACE_SUGGESTED, meetingPlaceSuggestedHandler);
        $pushNotifications.registerListener($window.PushMessageTypes.MEETING_PLACE_CONFIRMED, meetingPlaceConfirmedHandler);
        $pushNotifications.registerListener($window.PushMessageTypes.DATE_CANCELED, dateCanceledHandler);
        $pushNotifications.registerListener($window.PushMessageTypes.DATE_CHAT_MSG, dateChatMesssageHandler);
        $pushNotifications.registerListener($window.PushMessageTypes.BROADCAST_MESSAGE, broadcastMessageHandler);

        $pushNotifications.setReadyToReceiveNotifications();
    };

    $scope.onStudateSignInSuccess = function(userProfile)
    {
        // User is already registered to stuDate - Redirect to main screen
        $window.Globals.userProfile = userProfile;
        console.log('userProfile.dateState: ' + $window.Globals.userProfile.dateState);

        registerPushNotificationHandlers();
        $pushNotifications.setUserSignedIn();

        $availableDecleration.getAvailableStatus(function(result)
        {
            $window.Globals.userAvailable = result;
            $scope.goToPageByDateState();
        });
    };

    $scope.goToPageByDateState = function()
    {
        switch ($window.Globals.userProfile.dateState)
        {
            case $window.dateStates.NOT_IN_DATE:
            case $window.dateStates.DATE_CONFIRMATION:
            case $window.dateStates.DATE_SUGGESTION_CONFIRMED:
                console.log('Going to findPeoplePage...');
                $state.go('app.findPeoplePage');
                break;

            case $window.dateStates.MEETING_PLACE_SETTER:
            case $window.dateStates.MEETING_PLACE_WAITER:
                $scope.goToMeetingPlaceScreen();
                break;

            case $window.dateStates.MEETING_PLACE_CONFIRMED:
                $scope.goToActualDateScreen();
                break;
        }
    };

    $scope.goToMeetingPlaceScreen = function()
    {
        console.log('User is in meeting place selection');

        $window.Globals.currentDate = new StudateDate($datesManager, $window.Globals.userProfile.pendingDateId);
        $window.Globals.currentDate.getDateInfo(function(result)
        {
            if (result)
            {
                var isSetter = ($window.Globals.userProfile.dateState == $window.dateStates.MEETING_PLACE_SETTER);
                var parameters = {
                    suggestToken: isSetter,
                    locationSuggested: $window.Globals.currentDate.meetingPlace
                };
                $state.go('meetingPlacePage', { 'parameters' : JSON.stringify(parameters) });
            }
            else
            {
                console.log('getDateInfo failed when opening from state! (Going to findPeople...)');
                $state.go('app.findPeoplePage');
            }
        });
    };

    $scope.goToActualDateScreen = function()
    {
        console.log('User just needs to press "We Met" button!');

        $window.Globals.currentDate = new StudateDate($datesManager, $window.Globals.userProfile.pendingDateId);
        $window.Globals.currentDate.getDateInfo(function(result)
        {
            if (result)
            {
                $state.go('actualDatePage');
            }
            else
            {
                console.log('getDateInfo failed when opening from state! (Going to findPeople...)');
                $state.go('app.findPeoplePage');
            }
        });
    };

});
