
var Config = {};

// Note - VERY IMPORTANT TO UPDATE THIS WHEN CREATING NEW VERSION FOR CLIENTS
Config.studateAppVersion = 0.7;

Config.debug = false;                 // Indicate we are currently activating debug functionality
Config.runningFromBrowser = false;   // Indicate we are running the client through the browser (not native app)
Config.localServerPort = 8888;

Config.staffAccount = false;

//Config.studateURL = 'http://localhost:' + Config.localServerPort + '/';
//Config.studateURL = 'https://studate-tau.appspot.com/';

Config.studateAppEngineVersion = 2;
Config.studateURL = 'https://' + Config.studateAppEngineVersion + '-dot-studate-tau.appspot.com/';

// Neutralize console.log when not compiling in debug
if (!Config.debug) { console.log = function() {}; }

Config.appMood = 'energized';   // This will set the color theme for nav bar and popup buttons
